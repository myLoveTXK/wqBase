package shinyway.request.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by W~Q on 2017/4/1.
 * 基础JSON数据解析
 */
public class ProjectJsonBaseBean {

    private static final String statusKey = "status";
    private static final String dataKey = "data";
    private static final String detailsKey = "details";
    private static final String msgKey = "msg";

    public static String getStatusKey() {
        return statusKey;
    }

    public static String getDataKey() {
        return dataKey;
    }

    public static String getDetailsKey() {
        return detailsKey;
    }

    public static String getMsgKey() {
        return msgKey;
    }

    @SerializedName(statusKey)
    private String status;
    @SerializedName(detailsKey)
    private String details;
    @SerializedName(msgKey)
    private String msg;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "SwJsonBaseBean{" +
                "status='" + status + '\'' +
                ", details='" + details + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
