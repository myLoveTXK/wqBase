package shinyway.request.interfaces;

import com.wq.baseRequest.bean.HttpResponseInfoBean;

/**
 * Created by W~Q on 2017/4/1.
 */
public interface ProjectRequestCallback {
    void swSuccess(HttpResponseInfoBean result);

    void swFail(HttpResponseInfoBean result);
}
