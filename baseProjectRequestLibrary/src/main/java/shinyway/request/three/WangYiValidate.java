package shinyway.request.three;

import android.content.Context;
import android.os.AsyncTask;

import com.lidroid.xutils.util.LogUtils;
import com.netease.nis.captcha.Captcha;
import com.netease.nis.captcha.CaptchaListener;

/**
 * Created by W~Q on 2017/5/14.
 * 网易验证
 */

public class WangYiValidate {
//    public String wangyiCaptchaID = "20732753451d4b77a1badebfad637220";
    public String wangyiCaptchaID = "0489d7c00eff49089c56dfcd4b67f250";
    Captcha captcha;
    private String wangyiValidate;

    public void wangyiVerify(Context context, final ValidateCallback validateCallback) {
        wangyiVerify(context, wangyiCaptchaID, validateCallback);
    }

    public void wangyiVerify(Context context, String wangyiCaptchaId, final ValidateCallback validateCallback) {
        if (captcha == null) {
            captcha = new Captcha(context);
        }
        captcha.setCaptchaId(wangyiCaptchaId);
        captcha.setCaListener(new CaptchaListener() {

            @Override
            public void onValidate(String result, String validate, String message) {
                //验证结果，valiadte，可以根据返回的三个值进行用户自定义二次验证
                if (validate.length() > 0) {
                    wangyiValidate = validate;
                    if (validateCallback != null) {
                        validateCallback.success();
                    }
//                    superRequest(callback);
//                    ShowToast.shortTime("验证成功，validate = " + validate);
                } else {
//                    ShowToast.show("验证失败");
                    if (validateCallback != null) {
                        validateCallback.fail("验证失败");
                    }
                }
            }

            @Override
            public void closeWindow() {
                //请求关闭页面
//                ShowToast.show("取消验证");
                if (validateCallback != null) {
                    validateCallback.fail("取消验证");
                }
            }

            @Override
            public void onError(String errormsg) {
                //出错
                if (validateCallback != null) {
                    validateCallback.fail("验证错误：" + errormsg);
                }
//                ShowToast.show("验证错误：" + errormsg);
//                ShowToast.shortTime("错误信息：" + errormsg);
            }

            @Override
            public void onCancel() {
//                ShowToast.shortTime("取消线程");
//                ShowToast.show("取消验证");
                if (validateCallback != null) {
                    validateCallback.fail("取消验证");
                }
                //用户取消加载或者用户取消验证，关闭异步任务，也可根据情况在其他地方添加关闭异步任务接口
                if (wangYiVerifyTask != null) {
                    if (wangYiVerifyTask.getStatus() == AsyncTask.Status.RUNNING) {
                        LogUtils.i("stop wangYiVerifyTask");
                        wangYiVerifyTask.cancel(true);
                    }
                }
            }

            @Override
            public void onReady(boolean ret) {
                LogUtils.i("wq 0514 ret:" + ret);
                //该为调试接口，ret为true表示加载Sdk完成
//                if (ret) {
//                    ShowToast.shortTime("验证码sdk加载成功");
//                }
            }

        });
        //可选：开启debug
        captcha.setDebug(false);
        //可选：设置超时时间
        captcha.setTimeout(10000);

        if (wangYiVerifyTask != null) {
            if (wangYiVerifyTask.getStatus() == AsyncTask.Status.RUNNING) {
                LogUtils.i("stop wangYiVerifyTask");
                wangYiVerifyTask.cancel(true);
            }
        }
        wangYiVerifyTask = new WangYiVerifyTask();
        //关闭mLoginTask任务可以放在mytestCaListener的onCancel接口中处理
        wangYiVerifyTask.execute();
        //必填：初始化 captcha框架
        captcha.start();
        //可直接调用验证函数Validate()，本demo采取在异步任务中调用（见UserLoginTask类中）
        //captcha.Validate();
    }

    private WangYiVerifyTask wangYiVerifyTask = null;

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class WangYiVerifyTask extends AsyncTask<Void, Void, Boolean> {

        WangYiVerifyTask() {
        }


        @Override
        protected Boolean doInBackground(Void... params) {
            //可选：简单验证DeviceId、CaptchaId、Listener值
            return captcha.checkParams();
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                //必填：开始验证
                captcha.Validate();
            } else {
//                ShowToast.show("验证码SDK参数设置错误,请检查配置");
            }
        }

        @Override
        protected void onCancelled() {
            wangYiVerifyTask = null;
        }
    }

    public interface ValidateCallback {
        void success();

        void fail(String msg);
    }
}
