package shinyway.request;

import com.wq.baseRequest.utils.request.okhttp.WqOkHttp;
import com.wq.baseRequest.utils.request.okhttp.interfaces.UpdateDomainInterface;

import shinyway.request.utils.Config;

/**
 * Created by W~Q on 2017/3/31.
 * 请求配置项
 */

public class ProjectRequestConfig {
    public final static boolean isNeedValidata = false;//是否需要验证  大概率不用

    public static boolean SERVICE_API_IS_NEED_ENCRYPT = true;//是否需要加密

    public static String SERVICE_API_URL = Config.SERVICE_API_URL;
    public static String SERVICE_UPLOAD_URL = Config.SERVICE_PIC_UPLOAD_URL;

    public static boolean isNeedParseOuterLayerStatus = false;//是否需要解析外层状态

    static {
        WqOkHttp.setUpdateDomainInterface(new UpdateDomainInterface() {
            @Override
            public void updateDomain(String domain) {
                SERVICE_API_URL = domain;
            }
        });

    }

}
