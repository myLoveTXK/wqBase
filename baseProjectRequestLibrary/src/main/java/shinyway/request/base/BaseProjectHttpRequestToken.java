package shinyway.request.base;

import android.content.Context;

import com.wq.baseRequest.bean.HttpResponseInfoBean;

import shinyway.request.ProjectResponseStatus;
import shinyway.request.bean.ProjectJsonBaseBean;
import shinyway.request.interfaces.ProjectRequestCallback;

/**
 * Created by xt on 2017/8/24.
 * 封装token的处理
 */

public abstract class BaseProjectHttpRequestToken<DataBean> extends BaseProjectHttpRequestParseData<DataBean> {
    ProjectRequestCallback tokenCallback;

    private int getTokenLoopMaxCount = 2;

    public void setGetTokenLoopMaxCount(int getTokenLoopMaxCount) {
        this.getTokenLoopMaxCount = getTokenLoopMaxCount;
    }

    public void setTokenCallback(ProjectRequestCallback tokenCallback) {
        this.tokenCallback = tokenCallback;
    }

    public ProjectRequestCallback getTokenCallback() {
        return tokenCallback;
    }

    protected abstract boolean isNeedEncrypt();


    public BaseProjectHttpRequestToken(Context context) {
        super(context);
    }

    @Override
    public void request(final ProjectRequestCallback callback) {//封装获取token
        requestAddTokenFailProcess(callback, getTokenLoopMaxCount);
//        requestBeforeCheckToken(callback, getTokenLoopMaxCount);
    }

    /**
     * 请求之前检查token，并执行没有token的情况
     */
//    private void requestBeforeCheckToken(final ProjectRequestCallback callback, final int remainCount) {
//        if (getTokenApi() != null) {
//            requestGetToken(callback, remainCount);
//        } else {
//            requestAddTokenFailProcess(callback, remainCount);
//        }
//    }
    protected BaseProjectHttpRequestCommonParam getTokenApi() {
        return null;
    }

    /**
     * 获取最新token
     *
     * @param callback
     * @param remainCount
     */
    private void requestGetToken(final ProjectRequestCallback callback, final int remainCount) {
        final BaseProjectHttpRequestCommonParam apiGetToken = getTokenApi();
        apiGetToken.isNeedLoading(isNeedLoading());
        apiGetToken.request(new ProjectRequestCallback() {
            @Override
            public void swSuccess(HttpResponseInfoBean result) {
                if (apiGetToken.getTokenCallback() != null) {
                    apiGetToken.getTokenCallback().swSuccess(result);
                }
                requestAddTokenFailProcess(callback, remainCount - 1);
//                if (TextUtils.isEmpty(apiGetToken.getDataBean())) {
//                    requestCallbackFail(callback, result);
//                } else {
//                    setToken(apiGetToken.getDataBean());
//                    requestAddTokenFailProcess(callback, remainCount - 1);
//                }
            }

            @Override
            public void swFail(HttpResponseInfoBean result) {
                if (remainCount <= 0) {
                    requestCallbackFail(callback, result);
                } else {
                    requestGetToken(callback, remainCount - 1);
                }

            }
//            @Override
//            public void swSuccess(String result) {

//            }
//            @Override
//            public void swFail(String result) {
//                requestCallbackFail(callback, result);
//            }

        });
    }

    /**
     * 返回失败
     *
     * @param callback
     * @param result
     */
    private void requestCallbackFail(ProjectRequestCallback callback, HttpResponseInfoBean result) {
        if (callback != null) {
            callback.swFail(result);
        }
    }

    /**
     * 返回成功
     *
     * @param callback
     * @param result
     */
    private void requestCallbackSuccess(ProjectRequestCallback callback, HttpResponseInfoBean result) {
        if (callback != null) {
            callback.swSuccess(result);
        }
    }

    /**
     * 1.增加token失效处理
     */
    private void requestAddTokenFailProcess(final ProjectRequestCallback callback, final int remainCount) {
        super.request(new ProjectRequestCallback() {
            @Override
            public void swSuccess(HttpResponseInfoBean result) {
                requestCallbackSuccess(callback, result);
            }

            @Override
            public void swFail(HttpResponseInfoBean result) {
                ProjectJsonBaseBean swJsonBaseBean = getProjectJsonBaseBean();
                if (getTokenApi() != null &&
                        ((swJsonBaseBean != null && ProjectResponseStatus.STATUS_ERROR_TOKEN_TIMEOUT.equals(swJsonBaseBean.getStatus()))
                                || ProjectResponseStatus.STATUS_ERROR_TOKEN_TIMEOUT.equals(result.getHttpStatusCode() + ""))) {
                    if (remainCount <= 0) {
                        requestCallbackFail(callback, result);
                    } else {
                        requestGetToken(callback, remainCount);
                    }
                } else {
                    requestCallbackFail(callback, result);
                }
            }
        });
    }

}
