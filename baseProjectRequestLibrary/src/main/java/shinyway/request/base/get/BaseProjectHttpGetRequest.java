package shinyway.request.base.get;

import android.content.Context;

import com.wq.baseRequest.bean.enums.HttpType;

import shinyway.request.base.BaseProjectHttpRequestCommonParam;


/**
 * Created by W~Q on 2016/9/27.
 * 封装公参/加密参数
 */
public abstract class BaseProjectHttpGetRequest<DataBean> extends BaseProjectHttpRequestCommonParam<DataBean> {

    public BaseProjectHttpGetRequest(Context context) {
        super(context);
    }

    @Override
    protected HttpType getHttpType() {
        return HttpType.GET;
    }
}
