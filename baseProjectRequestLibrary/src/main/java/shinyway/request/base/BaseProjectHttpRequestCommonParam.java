package shinyway.request.base;

import android.content.Context;
import android.text.TextUtils;

import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.utils.request.interfaces.IRequestBeforeStartListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Request;
import shinyway.request.ProjectRequestConfig;
import shinyway.request.interfaces.ProjectRequestCallback;
import shinyway.request.utils.JsonBeanUtil;


/**
 * Created by W~Q on 2016/9/27.
 * 1.封装公参/加密参数
 * 2.封装加密
 */
public abstract class BaseProjectHttpRequestCommonParam<DataBean> extends BaseProjectHttpRequestToken<DataBean> {

    protected int apiVersion = 1;
    private final String SEND_PARM_KEY = "send_parm";
    private Map<String, Object> cacheLogMap = new HashMap<>();//缓存日志Map
    protected String url;

    protected Map<String, String> getMapParam() {
        Map<String, String> map = new HashMap<>();
//        map.put("wqTestKey", "wqTestValue");
        return map;
    }

    @Override
    protected String getStringBodyParam() throws Exception {
//        return JsonBeanUtil.getObjectJson(getStringJsonBodyParam(new HashMap<String, Object>()));
        return JsonBeanUtil.getObjectJson(getStringJsonBodyParam(new HashMap()));
    }

    protected Map getStringJsonBodyParam(Map objectMap) {
//        objectMap.put("wqTestJsonKey", "wqTestJsonValue");
        return objectMap;
    }

    /**
     * 是否需要加密
     */
    @Override
    protected boolean isNeedEncrypt() {
        return ProjectRequestConfig.SERVICE_API_IS_NEED_ENCRYPT;
    }

    public BaseProjectHttpRequestCommonParam(Context context) {
        super(context);
        url = ProjectRequestConfig.SERVICE_API_URL;
    }

    @Override
    protected Map<String, String> getParam() throws Exception {
        Map<String, String> paramMap = getMapParam();

        if (isNeedEncrypt()) {
            paramMap = getPublicParam(paramMap);
            setSign(paramMap);
        }
        removeEmpty(paramMap);

        if (isStringBodyParam()) {
            cacheLogMap.put(SEND_PARM_KEY, getStringBodyParam());
        } else {
            cacheLogMap.put(SEND_PARM_KEY, paramMap);
        }

        return paramMap;
    }

    @Override
    public void request(ProjectRequestCallback callback) {
        setHeadParam();
        super.request(callback);
    }

    /**
     * 设置头部公参
     */
    private void setHeadParam() {
        getWqOkHttp().setiRequestBeforeStartListener(new IRequestBeforeStartListener() {
            @Override
            public void beforeStar(Request.Builder requestBuilder) {
                //head 增加公参
                Map headMap = getHeadMap();
                if (headMap != null) {
                    for (Object key : headMap.keySet()) {
                        if (key != null) {
                            Object value = headMap.get(key);
                            if (value != null) {
                                requestBuilder.addHeader(key.toString(), value.toString()).build();
                            }
                        }
                    }
                }
                requestBuilder.addHeader("test_head_wq", "haha").build();
            }
        });
    }

    /**
     * 舍弃空的参数
     *
     * @param paramMap
     */
    private void removeEmpty(Map<String, String> paramMap) {
        ArrayList<String> emptyList = new ArrayList();
        for (String key : paramMap.keySet()) {
            if (TextUtils.isEmpty(key) || TextUtils.isEmpty(paramMap.get(key))) {
                emptyList.add(key);
            }
        }
        for (String key : emptyList) {
            try {
                LogUtils.i("wq 请求参数舍弃空参数 key：" + key);
                LogUtils.i("wq 请求参数舍弃空参数 value：" + paramMap.get(key));
                paramMap.remove(key + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected Map<String, String> getParmLog() throws Exception {
        Map<String, String> returnParmLog = new HashMap<>();
        Object sendParm = cacheLogMap.get(SEND_PARM_KEY);
        if (sendParm == null) {
            returnParmLog.put(SEND_PARM_KEY, null);
        } else {
            returnParmLog.put(SEND_PARM_KEY, sendParm.toString());
        }
        return returnParmLog;
    }


    /**
     * 获取加密之前的JSON
     *
     * @param paramMap
     * @return
     * @throws JSONException
     */
    private JSONObject getBeforeJsonObject(Map<String, String> paramMap) throws JSONException {
        JSONObject jsonObject = new JSONObject();//封装D 的JSON
        for (String key : paramMap.keySet()) {
            jsonObject.put(key, paramMap.get(key));
        }
        return jsonObject;
    }

    /**
     * 根据参数设置公参
     *
     * @return
     */
    private Map<String, String> getPublicParam(Map<String, String> paramMap) {
        if (paramMap == null) {
            paramMap = new HashMap<>();
        }
        paramMap.put("公参key", "公参value");
        return paramMap;
    }

    /**
     * 设置sign
     */
    public void setSign(Map<String, String> paramMap) {
        if (paramMap == null) {
            paramMap = new HashMap<>();
        }
        try {
            //增加加密信息
            paramMap.put("sign", "signValue");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
