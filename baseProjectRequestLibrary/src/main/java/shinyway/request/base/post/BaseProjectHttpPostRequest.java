package shinyway.request.base.post;

import android.content.Context;

import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.bean.HttpResponseInfoBean;
import com.wq.baseRequest.bean.enums.HttpType;

import shinyway.request.ProjectRequestConfig;
import shinyway.request.base.BaseProjectHttpRequestCommonParam;
import shinyway.request.interfaces.ProjectRequestCallback;
import shinyway.request.three.WangYiValidate;


/**
 * Created by W~Q on 2016/9/27.
 * 1.封装公参/加密参数
 * 2.封装业务
 */
public abstract class BaseProjectHttpPostRequest<DataBean> extends BaseProjectHttpRequestCommonParam<DataBean> {

    public BaseProjectHttpPostRequest(Context context) {
        super(context);
    }

    @Override
    protected HttpType getHttpType() {
        return HttpType.POST;
    }

    /**
     * 是否需要验证app
     *
     * @return
     */
    public boolean isNeedValidateRequest() {
        return false;
    }




    @Override
    public void request(final ProjectRequestCallback callback) {

        //网易验证码
        if (ProjectRequestConfig.isNeedValidata && isNeedValidateRequest()) {
            new WangYiValidate().wangyiVerify(context, new WangYiValidate.ValidateCallback() {
                @Override
                public void success() {
                    superRequest(callback);
                }

                @Override
                public void fail(String msg) {
                    LogUtils.i("wq 0514 msg:" + msg);
                    HttpResponseInfoBean httpResponseInfoBean = HttpResponseInfoBean.createBean(getUrl(), -9001, null, msg, msg);
                    callback.swFail(httpResponseInfoBean);
                }
            });
        } else {
            superRequest(callback);
        }
    }

    public void superRequest(ProjectRequestCallback callback) {
        super.request(callback);
    }
}
