package shinyway.request.base;

import android.content.Context;
import android.text.TextUtils;

import com.lidroid.xutils.util.LogUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


/**
 * Created by W~Q on 2016/9/27.
 * 封装解析业务逻辑数据
 */
public abstract class BaseProjectHttpRequestParseData<DataBean> extends BaseProjectHttpRequestParseStatus {

    private DataBean dataBean;

    public void setDataBean(DataBean dataBean) {
        this.dataBean = dataBean;
    }

    /**
     * 这里数据有可能为null ，
     * 如果有业务逻辑不返回data ，那么肯定是null
     *
     * @return
     */
    public DataBean getDataBean() {
        return dataBean;
    }

    public BaseProjectHttpRequestParseData(Context context) {
        super(context);
    }

    @Override
    protected void parseProjectData(String data) throws Exception {
        if (!TextUtils.isEmpty(data)) {
            Type[] types;
            try {
                types = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();
            } catch (Exception e) {
//                e.printStackTrace();
                return;
            }
            if (types == null || types.length == 0) {
                return;
            }
            if (types[0] != null) {
                LogUtils.i("wq 1215 data:" + data);
                LogUtils.i("wq 1215 types[0]:" + types[0].toString());
                if ("class java.lang.String".equals(types[0].toString())) {
                    try {
                        LogUtils.i("wq 1215 String类型");
                        setDataBean((DataBean) data);
                    } catch (Exception e) {
                        LogUtils.i("wq 1215 String类型 异常");
                        e.printStackTrace();
                    }
                } else {
                    dataBean = mGson.fromJson(data, types[0]);
                }
            }
        }
    }

}
