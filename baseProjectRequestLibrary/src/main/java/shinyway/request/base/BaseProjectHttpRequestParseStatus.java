package shinyway.request.base;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.wq.baseRequest.base.http.HttpRequestBase;
import com.wq.baseRequest.bean.HttpResponseInfoBean;
import com.wq.baseRequest.interfaces.HttpRequestCallback;

import org.json.JSONObject;

import shinyway.request.ProjectRequestConfig;
import shinyway.request.ProjectResponseStatus;
import shinyway.request.bean.ProjectJsonBaseBean;
import shinyway.request.interfaces.ProjectRequestCallback;


/**
 * Created by W~Q on 2016/9/27.
 * 封装callback ,解析基础数据
 */
public abstract class BaseProjectHttpRequestParseStatus extends HttpRequestBase {
    private final boolean isNeedParseOuterLayerStatus = ProjectRequestConfig.isNeedParseOuterLayerStatus;

    private String SUCCESS_STATUS = ProjectResponseStatus.STATUS_SUCCESS;
    private ProjectJsonBaseBean projectJsonBaseBean;

    protected Gson mGson = new Gson();


    public BaseProjectHttpRequestParseStatus(Context context) {
        super(context);
    }


    /**
     * 顶层数据对象
     *
     * @return
     */
    public ProjectJsonBaseBean getProjectJsonBaseBean() {
        return projectJsonBaseBean;
    }

    /**
     * 是否是网络错误
     *
     * @return
     */
    public boolean isNetworkError() {
        if (projectJsonBaseBean == null) {
            return true;
        }
        if (TextUtils.isEmpty(projectJsonBaseBean.getStatus())) {
            return true;
        }
        return false;
    }


    /**
     * 解析数据
     *
     * @param data
     * @throws Exception
     */
    protected abstract void parseProjectData(String data) throws Exception;

    /**
     * 是否需要解析外层状态码
     * 多项目，设置可配置项
     */
    protected boolean isNeedParseOuterLayerStatus() {
        return isNeedParseOuterLayerStatus;
    }

    @Override
    protected void parseDataBackground(String result) throws Exception {
        if (isNeedParseOuterLayerStatus()) {
            projectJsonBaseBean = mGson.fromJson(result, ProjectJsonBaseBean.class);
            if (projectJsonBaseBean != null) {
                JSONObject dataObject = new JSONObject(result);
                String data = dataObject.getString(ProjectJsonBaseBean.getDataKey());
                if (data != null && !data.equals("null") && !data.equals("{}") && !data.equals("[]")) {
                    parseProjectData(data);
                } else {
                    parseProjectData("");
                }
            } else {
                parseProjectData("");
            }
        } else {
            parseProjectData(result);
        }
    }

    public void request(final ProjectRequestCallback callback) {
        baseRequest(new HttpRequestCallback() {
            @Override
            public void httpSuccess(HttpResponseInfoBean httpResponseInfoBean) {
                if (callback != null) {
                    if (isNeedParseOuterLayerStatus()) {//需要解析外层状态码
                        if (projectJsonBaseBean == null) {
                            callback.swFail(httpResponseInfoBean);
                        } else {
                            passResult(httpResponseInfoBean);
                        }
                    } else {//不需要解析外层状态码
                        callback.swSuccess(httpResponseInfoBean);
                    }
                }
            }

            private void passResult(HttpResponseInfoBean httpResponseInfoBean) {
//                LogUtils.i("wq 1225 passResult:" + reslut);
                JSONObject dataObject = null;
                String status = "";
                String details = "";
                String msg = "";
                try {
                    dataObject = new JSONObject(httpResponseInfoBean.getHttpBody());
                    status = dataObject.getString(ProjectJsonBaseBean.getStatusKey());
                    details = dataObject.getString(ProjectJsonBaseBean.getDetailsKey());
                    msg = dataObject.getString(ProjectJsonBaseBean.getMsgKey());
                } catch (Exception e) {
                    e.printStackTrace();
                    msg = httpResponseInfoBean.getHttpBody();
                }
                if (SUCCESS_STATUS.equals(status)) {
                    callback.swSuccess(httpResponseInfoBean);
                } else {
                    callback.swFail(httpResponseInfoBean);
                }
            }

            @Override
            public void httpFail(HttpResponseInfoBean httpResponseInfoBean) {
                if (callback != null) {
//                    passResult(msg);

                    callback.swFail(httpResponseInfoBean);
                }
            }
        });
    }

}
