package shinyway.request;

/**
 * Created by xt on 2017/8/24.
 */

public class ProjectResponseStatus {
    /**
     * 成功状态码标识
     */
    public static String STATUS_SUCCESS = "1";
    public static String STATUS_ERROR_TOKEN_TIMEOUT = "401";
}
