package shinyway.request.api;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import shinyway.request.base.post.BaseProjectHttpPostRequest;

/**
 * Created by W~Q on 2017/3/31.
 */
public class ApiGetToken extends BaseProjectHttpPostRequest<String> {

    private static final String mobileClientKey = "ghuijo78iuonklmsd";
    private static final String mobileClientSecret = "hggfgyhjikl7889ioijh";

    public ApiGetToken(Context context) {
        super(context);
    }

    @Override
    protected boolean isNeedParseOuterLayerStatus() {//需要解析最外层
        return true;
    }

    @Override
    protected Map<String, String> getMapParam(){
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("clientKey", mobileClientKey);
        mapParam.put("clientSecret", mobileClientSecret);
        return mapParam;
    }


    @Override
    protected String getUrl() {
        return url + "/remote/sysLogin";
    }

    @Override
    protected String apiName() {
        return "getToken";
    }
}
