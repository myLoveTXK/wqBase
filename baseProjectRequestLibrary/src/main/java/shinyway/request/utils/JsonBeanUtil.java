package shinyway.request.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lidroid.xutils.util.LogUtils;

/**
 * Created by W~Q on 2016/8/19.
 * 网络获取的JSON的处理工具类
 */
public class JsonBeanUtil {

    private static Gson gson = new GsonBuilder().serializeNulls().create();

    public static String getObjectJson(Object object) {
        if (object == null) {
            return null;
        }
        String json = gson.toJson(object);
        LogUtils.i("wq 1224 getObjectJson:" + json);
        return json;
    }

    /**
     * 获取整形
     *
     * @param value
     * @param defaultInt
     * @return
     */
    public static int getInteger(String value, int defaultInt) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
//            e.printStackTrace();
            return defaultInt;
        }
    }

    public static long getLong(String value, long defaultInt) {
        try {
            return Long.valueOf(value);
        } catch (Exception e) {
//            e.printStackTrace();
            return defaultInt;
        }
    }

    public static boolean isTrue(String value) {
        return "1".equals(value);

    }

    public static double getDouble(String doubleStr, double defaultDouble) {
        try {
            return Double.valueOf(doubleStr);
        } catch (Exception e) {
//            e.printStackTrace();
            return defaultDouble;
        }
    }
}
