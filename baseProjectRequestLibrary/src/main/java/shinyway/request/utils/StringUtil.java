package shinyway.request.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xt on 2017/8/22.
 */
public class StringUtil {

    public static String replaceBlank(String str, String replace) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\r|\n");
//            \s*|	|
            Matcher m = p.matcher(str);
            dest = m.replaceAll(replace == null ? "" : replace);
        }
        return dest;
    }
}