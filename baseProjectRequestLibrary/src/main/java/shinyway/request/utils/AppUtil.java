package shinyway.request.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/10.
 */
public class AppUtil {
    /**
     * 获取状态栏高度
     */
    public static int getStatusHeight(Activity activity) {
        int statusHeight = 0;
        Rect localRect = new Rect();
        activity.getWindow().getDecorView()
                .getWindowVisibleDisplayFrame(localRect);
        statusHeight = localRect.top;
        if (0 == statusHeight) {
            Class<?> localClass;
            try {
                localClass = Class.forName("com.android.internal.R$dimen");
                Object localObject = localClass.newInstance();
                int i5 = Integer.parseInt(localClass
                        .getField("status_bar_height").get(localObject)
                        .toString());
                statusHeight = activity.getResources()
                        .getDimensionPixelSize(i5);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        return statusHeight;
    }

    @TargetApi(19)
    public static void setTranslucentStatus(Activity activity, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    /**
     * 判断是否安装app
     *
     * @param context
     * @param packageName
     * @return
     */
    public static boolean isAppInstalled(Context context, String packageName) {
        final PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        List<String> pName = new ArrayList<String>();
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                pName.add(pn);
            }
        }
        return pName.contains(packageName);
    }

    /**
     * 是否有微信
     *
     * @param context
     * @return
     */
//    public static boolean isHasWxApp(Context context) {
//        return WxUtil.wxPayApi.isWXAppInstalled();
////        return AppUtil.isAppInstalled(context, "com.tencent.mm");
//    }


    /**
     * Created by xaio bailong on 2016/3/2.
     */
//    public class VitualKey extends RelativeLayout {
//        private onLayoutKeyChange mLayoutKeyChange;
//
//        public VitualKey(Context context) {
//            super(context);
//        }
//
//        public VitualKey(Context context, AttributeSet attrs) {
//            super(context, attrs);
//        }
//
//        public VitualKey(Context context, AttributeSet attrs, int defStyleAttr) {
//            super(context, attrs, defStyleAttr);
//        }
//
//        /**
//         * @param changed 布局发生改变 ture 没有改变False * @param l * @param t * @param r * @param b
//         */
//        @Override
//        protected void onLayout(boolean changed, int l, int t, int r, int b) {
//            super.onLayout(changed, l, t, r, b); //Log.e("onLayout", "onLayout: b "+b);
//            if (changed) mLayoutKeyChange.onLayoutKeyChange(b);
//        }
//
//        public void setonLayoutKeyChange(onLayoutKeyChange layoutKeyChange) {
//            mLayoutKeyChange = layoutKeyChange;
//        }
//
//        public interface onLayoutKeyChange {
//            /**
//             * 虚拟键盘状态监听 * * @param b 布局距离底部的布局
//             */
//            void onLayoutKeyChange(int b);
//        }
//    }

    //获取屏幕原始尺寸高度，包括虚拟功能键高度
    public static int getDpi(Context context) {
        int dpi = 0;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        @SuppressWarnings("rawtypes")
        Class c;
        try {
            c = Class.forName("android.view.Display");
            @SuppressWarnings("unchecked")
            Method method = c.getMethod("getRealMetrics", DisplayMetrics.class);
            method.invoke(display, displayMetrics);
            dpi = displayMetrics.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dpi;
    }

    /**
     * 获取 虚拟按键的高度
     *
     * @param context
     * @return
     */
    public static int getBottomStatusHeight(Context context) {
        int totalHeight = getDpi(context);

        int contentHeight = getScreenHeight(context);

        return totalHeight - contentHeight;
    }

    /**
     * 获得屏幕高度
     *
     * @param context
     * @return
     */
    public static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    //版本名
    public static String getVersionName(Context context) {
        return getPackageInfo(context).versionName;
    }

    //版本号
    public static int getVersionCode(Context context) {
        return getPackageInfo(context).versionCode;
    }

    //包名
    public static String getPageName(Context context) {
        return getPackageInfo(context).packageName;
    }

    private static PackageInfo getPackageInfo(Context context) {
        PackageInfo pi = null;

        try {
            PackageManager pm = context.getPackageManager();
            pi = pm.getPackageInfo(context.getPackageName(),
                    PackageManager.GET_CONFIGURATIONS);

            return pi;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pi;
    }

    /**
     * 获取手机厂商 型号信息
     */
    public static String getDeviceBrandInfo() {
//        LogUtils.i("wq 0606 查看手机属性Build.BOARD："+ Build.BOARD);
//        LogUtils.i("wq 0606 查看手机属性Build.BOOTLOADER："+ Build.BOOTLOADER);
//        LogUtils.i("wq 0606 查看手机属性Build.BRAND："+ Build.BRAND);
//        LogUtils.i("wq 0606 查看手机属性Build.DEVICE："+ Build.DEVICE);
//        LogUtils.i("wq 0606 查看手机属性Build.DISPLAY："+ Build.DISPLAY);
//        LogUtils.i("wq 0606 查看手机属性Build.FINGERPRINT："+ Build.FINGERPRINT);
//        LogUtils.i("wq 0606 查看手机属性Build.HARDWARE："+ Build.HARDWARE);
//        LogUtils.i("wq 0606 查看手机属性Build.HOST："+ Build.HOST);
//        LogUtils.i("wq 0606 查看手机属性Build.ID："+ Build.ID);
//        LogUtils.i("wq 0606 查看手机属性Build.MANUFACTURER："+ Build.MANUFACTURER);
//        LogUtils.i("wq 0606 查看手机属性Build.MODEL："+ Build.MODEL);
//        LogUtils.i("wq 0606 查看手机属性Build.PRODUCT："+ Build.PRODUCT);
//        LogUtils.i("wq 0606 查看手机属性Build.SERIAL："+ Build.SERIAL);
//        LogUtils.i("wq 0606 查看手机属性Build.TAGS："+ Build.TAGS);
//        LogUtils.i("wq 0606 查看手机属性Build.TYPE："+ Build.TYPE);

        return Build.BRAND + " " + Build.MODEL + " " + Build.DISPLAY;
    }



}
