
package shinyway.request.utils;

import com.wq.baseRequest.utils.MD5Util;

/**
 * 报文处理
 * 
 * @author BrookLyn
 * @version $Id: MobileMessageUtil.java, v 1.6 2017-8-14 下午6:26:25 xt Exp $
 */
public class MobileMessageUtil {
//    private static Logger log = LoggerFactory.getLogger(MobileMessageUtil.class);
    private static final String DES_KEY     = "cfgubijn";
    // private static final String DES_KEY     = "starfl3kmysyhl9day";
    private static final String MD5_KEY     = "hksdgfj;sgijlkeg^#*5824jk";
    private static final String TOKEN_SEED  = "dtfygubikjlm";
    // private static final String TOKEN_SEED  = "starwolflvecw";
//    public static String        token;                                                         //Token
    public static long          lastAccessTime;                                                //客户端最后访问时间
    public static final long    expiredTime = 30 * 60 * 1000;                                  //客户端超时时间，30分钟

    /**
     * 报文解密
     * 
     * @param ciphertext
     * @return
     */
    public static String decryptMsg(String ciphertext) {
        // DES解密，获得明文
        String decryptData = new DESUtil(DES_KEY).decryptStr(ciphertext);
//        log.debug("============解密获得明文：【" + decryptData + "】============");
        return decryptData;
    }
    
    /*  *//**
           * 解析请求报文：转换成对应bean
           * 
           * @param decryptData 明文
           * @return 对应报文bean
           */
    /**
     * 对报文进行DES加密
     *
     * @param message 消息明文
     * @return 加密的响应报文
     */
    public static String encryptMsg(String message) {
        String ciphertext = null;
        try {
            // 对json进行DES加密
            ciphertext = new DESUtil(DES_KEY).encryptStr(message);
            //   log.debug("============消息主体转换密文：【" + ciphertext + "】============");
        } catch (Exception e) {
            e.printStackTrace();
//            log.error("encrypt message error! cause by : ", e);
        }
        return ciphertext;

    }

    /**
     * 获得报文摘要
     * @param message 消息明文
     * @return 返回报文摘要
     */
    public static String getMsgHash(String message) {
        String hash = null;
        try {
            hash = MD5Util.stringToMD5(message + MD5_KEY);
            //    log.debug("============消息摘要密文：【" + hash + "】============");
        } catch (Exception e) {
            e.printStackTrace();
            //log.error("encrypt hash error! cause by : ", e);
        }
        return hash;

    }
}
