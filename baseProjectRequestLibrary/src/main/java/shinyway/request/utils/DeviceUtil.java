package shinyway.request.utils;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.lidroid.xutils.util.LogUtils;

import java.util.UUID;

/**
 * Created by Administrator on 2016/10/19.
 * 设备
 */
public class DeviceUtil {

    /**
     * 获取设备号
     *
     * @return
     */
    public static String getDeviceID(Context context) {

//        String deviceId = getUniquePsuedoID();
        String deviceId = getImei(context);
//        LogUtils.i("wq 1202 deviceId:" + deviceId);
        return deviceId;
    }

    public static String getImei(Context context) {
        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = TelephonyMgr.getDeviceId();
        return imei;
    }

    //获得独一无二的Psuedo ID
    public static String getUniquePsuedoID() {
        String serial = null;

        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +
                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +
                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +
                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +
                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +
                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +
                Build.USER.length() % 10; //13 位
        LogUtils.i("wq 1223 Build.BOARD:" + Build.BOARD);
        LogUtils.i("wq 1223 Build.BRAND:" + Build.BRAND);
        LogUtils.i("wq 1223 Build.CPU_ABI:" + Build.CPU_ABI);
        LogUtils.i("wq 1223 Build.DEVICE:" + Build.DEVICE);
        LogUtils.i("wq 1223 Build.DISPLAY:" + Build.DISPLAY);
        LogUtils.i("wq 1223 Build.HOST:" + Build.HOST);
        LogUtils.i("wq 1223 Build.ID:" + Build.ID);
        LogUtils.i("wq 1223 Build.MANUFACTURER:" + Build.MANUFACTURER);
        LogUtils.i("wq 1223 Build.MODEL:" + Build.MODEL);
        LogUtils.i("wq 1223 Build.PRODUCT:" + Build.PRODUCT);
        LogUtils.i("wq 1223 Build.TAGS:" + Build.TAGS);
        LogUtils.i("wq 1223 Build.TYPE:" + Build.TYPE);
        LogUtils.i("wq 1223 Build.USER:" + Build.USER);
        LogUtils.i("wq 1223 Build------------------------------------------------------------------------------------------------------------------:");
        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
            //API>=9 使用serial号
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            //serial需要一个初始化
            serial = "serial"; // 随便一个初始化
        }
        //使用硬件信息拼凑出来的15位号码
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }
}
