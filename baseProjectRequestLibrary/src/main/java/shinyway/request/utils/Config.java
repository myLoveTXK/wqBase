package shinyway.request.utils;

import com.wq.baseRequest.utils.request.okhttp.DomainManager;
import com.wq.baseRequest.utils.request.okhttp.WqOkHttp;

import shinyway.request.ProjectRequestConfig;

/**
 * Created by W~Q on 2017/4/14.
 */

public class Config {

    public static final String PATH = "/YMShinyway";
    public static boolean isDebug = false;//debug模式
    public static boolean isFangZhen = false;//仿真环境
    public static boolean isNeedLoadImageLog = true;


    public static String SERVICE_API_URL;//API接口URL
    public static String SERVICE_PIC_SHOW_URL;//图片显示URL
    public static final String SERVICE_PIC_SHOW_SMALL_PATH = "thumb/";//图片显示URL-缩略图

    public static String SERVICE_PIC_UPLOAD_URL;//图片上传URL
    public static boolean SERVICE_API_IS_NEED_ENCRYPT = true;//API接口是否需要加密

    public static String CUSTOMER_SERVICE_NUMBER = "400-826-2186";

    public static double UI_SCREEN_WIDTH = 750.0;


    public static boolean isNeedValidata = false;//验证码验证,联通验证使用

    static {
        if (isDebug) {
            if (SERVICE_API_IS_NEED_ENCRYPT) {
                SERVICE_API_URL = "http://10.10.11.29:8189";
                //                SERVICE_API_URL = "http://10.10.11.36:8188";
            } else {
                SERVICE_API_URL = "http://10.10.11.29:8089";
            }
            //            SERVICE_PIC_SHOW_URL = "http://img.qct-cg.shinyway.org/";
            //            SERVICE_PIC_UPLOAD_URL = "http://qct-cg.shinyway.org";

            SERVICE_PIC_SHOW_URL = "http://img.qct-cg.shinyway.org/";
            //            SERVICE_PIC_UPLOAD_URL = "http://10.10.11.29:8085/qct/";
            SERVICE_PIC_UPLOAD_URL = "http://qct-cg.shinyway.org";
        } else {
            SERVICE_API_URL = DomainManager.domainList.get(0);
            //            SERVICE_API_URL = "http://api2ym.shinyway.org/v1";

            SERVICE_PIC_SHOW_URL = "http://img.qct-cg.shinyway.org/";
            SERVICE_PIC_UPLOAD_URL = "http://qct-cg.shinyway.org";
            if (isFangZhen) {
                //                SERVICE_API_URL = "http://api_cs.shinyway.org/v1";
                //                SERVICE_API_URL = "http://api-cs.shinyway.org/v1";
                SERVICE_API_URL = "http://api-cs.shinyway.org/ymv1";
            }
        }
        WqOkHttp.isDebug = isDebug;
        setApiUrl(SERVICE_API_URL, SERVICE_PIC_UPLOAD_URL, SERVICE_API_IS_NEED_ENCRYPT);
    }


    /**
     * 设置基类api路径
     *
     * @param SERVICE_API_URL
     * @param SERVICE_PIC_UPLOAD_URL
     */
    static void setApiUrl(String SERVICE_API_URL, String SERVICE_PIC_UPLOAD_URL, boolean SERVICE_API_IS_NEED_ENCRYPT) {
        ProjectRequestConfig.SERVICE_API_URL = SERVICE_API_URL;
        ProjectRequestConfig.SERVICE_UPLOAD_URL = SERVICE_PIC_UPLOAD_URL;
        ProjectRequestConfig.SERVICE_API_IS_NEED_ENCRYPT = SERVICE_API_IS_NEED_ENCRYPT;
    }
}
