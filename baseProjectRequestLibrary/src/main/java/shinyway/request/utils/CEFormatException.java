package shinyway.request.utils;

import java.io.IOException;

public class CEFormatException extends IOException
{
  public CEFormatException(String paramString)
  {
    super(paramString);
  }
}