package wq.share.shareUtil;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by W~Q on 2017-03-20.
 * TODO 这里先使用系统吐司， 后面针对适配机型再做新通全局吐司
 */
public abstract class YouMengShowToast {
    private static Toast toast;

    public static void show(Context context, Object text) {
        shortTime(context, text, Gravity.CENTER);
    }

//    public static void shortTime(Object text) {
//        shortTime(text, Gravity.CENTER);
//    }
//
//    public static void shortTimeTop(Object text) {
//        shortTime(text, Gravity.TOP);
//    }
//
//    public static void shortTimeBottom(Object text) {
//        shortTime(text, Gravity.BOTTOM);
//    }

    private static void shortTime(Context context, Object text, int gravity) {

        try {
            if (toast == null) {
                toast = Toast.makeText(context, text + "", Toast.LENGTH_SHORT);
                toast.setGravity(gravity, 0, 0);
            } else {
                toast.setText(text + "");
            }
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//
//    /**
//     * 显示Toast
//     *
//     * @param title
//     * @param content
//     */
//    public static void showCustom(Context context, String title, String content) {
//        View layout = LayoutInflater.from(context).inflate(R.layout.toast_custom_layout, null);
//
//        TextView titleTv = (TextView) layout.findViewById(R.id.toast_title);
//        TextView contentTv = (TextView) layout.findViewById(R.id.toast_content);
//        titleTv.setText("" + title);
//        contentTv.setText("" + content);
//
//        Toast toast = new Toast(context);
//        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(Toast.LENGTH_SHORT);
//        toast.setView(layout);
//        toast.show();
//    }
//
//
//    public static void showCustom(Context context, int imgRes, String title, String content) {
//        View layout = LayoutInflater.from(context).inflate(R.layout.toast_custom_layout, null);
//
//        ImageView img = (ImageView) layout.findViewById(R.id.img);
//        TextView titleTv = (TextView) layout.findViewById(R.id.toast_title);
//        TextView contentTv = (TextView) layout.findViewById(R.id.toast_content);
//        img.setImageResource(imgRes);
//        titleTv.setText("" + title);
//        contentTv.setText("" + content);
//
//        Toast toast = new Toast(context);
//        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(Toast.LENGTH_SHORT);
//        toast.setView(layout);
//        toast.show();
//    }

}
