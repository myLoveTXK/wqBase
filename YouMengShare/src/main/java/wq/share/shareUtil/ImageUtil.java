package wq.share.shareUtil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by xt on 2017/12/22.
 */

public class ImageUtil {

    public static File saveBitmap(Bitmap bitmap, String name) {
//        File PHOTO_DIR = new File(Environment.getExternalStorageDirectory()+"image");//设置保存路径
        File file = new File(Files.getPhotoFilePath() + name);//设置保存路径
//        File avaterFile = new File(PHOTO_DIR, "avater.jpg");//设置文件名称
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getBitmap(String name) {
        Bitmap bitmap = null;
        try {
//            File avaterFile = new File(PHOTO_DIR, "avater.jpg");

            File file = new File(Files.getPhotoFilePath() + name);
            if (file.exists()) {
                bitmap = BitmapFactory.decodeFile(file.getPath());
            }
        } catch (Exception e) {

        }
        return bitmap;
    }

    public static Bitmap getBitmap(View scrollView) {
        int width = scrollView.getWidth();
        int height = scrollView.getHeight();
        scrollView.setBackgroundColor(Color.WHITE);
        if (0 == width || 0 == height) {
            DisplayMetrics metric = scrollView.getContext().getApplicationContext().getResources().getDisplayMetrics();
            int screenWidth = metric.widthPixels;
            scrollView.measure(View.MeasureSpec.makeMeasureSpec(screenWidth, View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            scrollView.layout(0, 0, scrollView.getMeasuredWidth(), scrollView.getMeasuredHeight());
            width = scrollView.getWidth();
            height = scrollView.getHeight();
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        scrollView.draw(canvas);
        return bitmap;
    }

    public static Bitmap getBitmapByView(ViewGroup scrollView) {
        int h = 0;
        Bitmap bitmap = null;

        for (int i = 0; i < scrollView.getChildCount(); i++) {
            h += scrollView.getChildAt(i).getHeight();
            scrollView.getChildAt(i).setBackgroundColor(
                    Color.parseColor("#ffffff"));
        }

        bitmap = Bitmap.createBitmap(scrollView.getWidth(), h,
                Bitmap.Config.RGB_565);
        final Canvas canvas = new Canvas(bitmap);
        scrollView.draw(canvas);
        return bitmap;
    }
}
