package wq.share.shareUtil;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareConfig;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import java.io.File;

import wq.share.R;

/**
 * Created by W~Q on 2017/5/18.
 */

public class YouMengShareUtil {
    //com.shinyway.app
    //7e46cc6b786c0fafa180ccc31cdc5905

    public static final String WX_APP_ID = "wx1e10bf520cb3e44f";//微信APP_ID
    public static final String WX_APP_SECRET = "142655658d90e2acd2e6dd0d033165fa";//微信APP_SECRET


    public static final String QQ_APP_ID = "1106091097";//QQ 正式
    public static final String QQ_APP_SECRET = "jsXql1p7tPUBIEpZ";//QQ 正式


    public static final String WEIBO_APP_ID = "3967760660";//微博 APP_ID
    public static final String WEIBO_APP_SECRET = "512c8ad954ab1da9402f131d6a75b164";//微博 APP_SECRET


    /**
     * 初始化
     */
    public static void initShareConfig() {
        Config.DEBUG = true;

//        PlatformConfig.setWeixin("todo_share_weixin_appid", "todo_share_weixin_appSecret");
//        PlatformConfig.setQQZone("todo_share_qq_appid", "todo_share_qq_appSecret");
//        PlatformConfig.setSinaWeibo("todo_share_xinlang_appid", "todo_share_xinlang_appSecret", "todo_share_weixin_callback");

        PlatformConfig.setWeixin(WX_APP_ID, WX_APP_SECRET);
        PlatformConfig.setQQZone(QQ_APP_ID, QQ_APP_SECRET);
        PlatformConfig.setSinaWeibo(WEIBO_APP_ID, WEIBO_APP_SECRET, "");

        UMShareConfig config = new UMShareConfig();
        config.isNeedAuthOnGetUserInfo(false);
        config.isOpenShareEditActivity(true);
        config.setSinaAuthType(UMShareConfig.AUTH_TYPE_SSO);
        config.setFacebookAuthType(UMShareConfig.AUTH_TYPE_SSO);
        config.setShareToLinkedInFriendScope(UMShareConfig.LINKED_IN_FRIEND_SCOPE_ANYONE);
    }

    /**
     * @param activity
     * @param url
     * @param imgRes
     * @param title
     * @param description
     * @param share_media
     * @param umShareListener
     */
    private static void shareWeb(Activity activity, String url, int imgRes, String title, String description, SHARE_MEDIA share_media, UMShareListener umShareListener) {
        Log.i("wq", "wq 0523 启动分享 url：" + url);
        Log.i("wq", "wq 0523 启动分享 title：" + title);
        Log.i("wq", "wq 0523 启动分享 description：" + description);
        Log.i("wq", "wq 0523 启动分享 share_media：" + share_media.toString());
        UMImage thumb = new UMImage(activity, imgRes);
        UMWeb web = new UMWeb(url);
        web.setThumb(thumb);
        web.setDescription(description);
        web.setTitle(title);

        if (share_media == SHARE_MEDIA.SINA) {
            web.setDescription(title + url);
        }
        ShareAction shareAction = new ShareAction(activity).withMedia(web).setPlatform(share_media).setCallback(new ShareListener(activity, umShareListener));
//        shareAction.open();
        shareAction.share();
    }

    private static PopupWindow popupWindow;

    public static PopupWindow shareWeb(Activity activity, String url, int imgRes, String title, String description, UMShareListener umShareListener) {
        dismissPop();
        View view = LayoutInflater.from(activity).inflate(R.layout.share_pop, null);
        popupWindow = PopWindowUtil.getPopupWindow(activity, view);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.llShareWeibo.setOnClickListener(new shareOnClick(viewHolder, activity, url, imgRes, title, description, umShareListener));
        viewHolder.llShareWechat.setOnClickListener(new shareOnClick(viewHolder, activity, url, imgRes, title, description, umShareListener));
        viewHolder.llShareComments.setOnClickListener(new shareOnClick(viewHolder, activity, url, imgRes, title, description, umShareListener));
        viewHolder.llShareQq.setOnClickListener(new shareOnClick(viewHolder, activity, url, imgRes, title, description, umShareListener));
        viewHolder.llShareQzone.setOnClickListener(new shareOnClick(viewHolder, activity, url, imgRes, title, description, umShareListener));
        viewHolder.llShareCopy.setOnClickListener(new shareOnClick(viewHolder, activity, url, imgRes, title, description, umShareListener));
        viewHolder.tvSharePopupCancel.setOnClickListener(new shareOnClick(viewHolder, activity, url, imgRes, title, description, umShareListener));
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
//        popupWindow.update();
        return popupWindow;
    }

    public static void dismissPop() {
        if (popupWindow != null) {
            if (popupWindow.isShowing()) {
                try {
                    popupWindow.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class shareOnClick implements View.OnClickListener {
        Activity activity;
        ViewHolder viewHolder;
        String url;
        int imgRes;
        String title;
        String description;
        //        SHARE_MEDIA share_media;
        UMShareListener umShareListener;

        public shareOnClick(ViewHolder viewHolder, Activity activity, String url, int imgRes, String title, String description, UMShareListener umShareListener) {
            this.viewHolder = viewHolder;
            this.activity = activity;
            this.url = url;
            this.imgRes = imgRes;
            this.title = title;
            this.description = description;
            this.umShareListener = umShareListener;
        }

        @Override
        public void onClick(View v) {
            if (viewHolder == null) {
                return;
            }
            if (v == viewHolder.llShareWeibo) {//微博
                shareWeb(activity, url, imgRes, title, description, SHARE_MEDIA.SINA, umShareListener);
            } else if (v == viewHolder.llShareWechat) {//微信
                shareWeb(activity, url, imgRes, title, description, SHARE_MEDIA.WEIXIN, umShareListener);
            } else if (v == viewHolder.llShareComments) {//微信朋友圈
                shareWeb(activity, url, imgRes, title, description, SHARE_MEDIA.WEIXIN_CIRCLE, umShareListener);
            } else if (v == viewHolder.llShareQq) {//qq
                shareWeb(activity, url, imgRes, title, description, SHARE_MEDIA.QQ, umShareListener);
            } else if (v == viewHolder.llShareQzone) {//QQ空间
                shareWeb(activity, url, imgRes, title, description, SHARE_MEDIA.QZONE, umShareListener);
            } else if (v == viewHolder.llShareCopy) {//复制
                ClipboardManager clipboardManager = (ClipboardManager) activity.getSystemService(Activity.CLIPBOARD_SERVICE);
                clipboardManager.setText(url);
                Toast.makeText(activity, "复制成功", Toast.LENGTH_SHORT).show();
            } else if (v == viewHolder.tvSharePopupCancel) {//取消

            }
            dismissPop();
        }

    }

    private static class ShareListener implements UMShareListener {
        UMShareListener umShareListener;
        YouMentShowProcessDialog mProcessDialog;
        Activity activity;

        private void dismissDialog() {
            if (mProcessDialog != null) {
                if (mProcessDialog.isShowing()) {
                    try {
                        mProcessDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public ShareListener(Activity activity, UMShareListener umShareListener) {
            this.umShareListener = umShareListener;
            this.activity = activity;
            if (activity != null) {
                mProcessDialog = new YouMentShowProcessDialog(activity);
            }
        }

        @Override
        public void onStart(SHARE_MEDIA share_media) {
//            if (mProcessDialog != null) {
//                mProcessDialog.show();
//            }
            if (umShareListener != null) {
                umShareListener.onStart(share_media);
            }

        }

        @Override
        public void onResult(SHARE_MEDIA share_media) {
            YouMengShowToast.show(activity, "分享成功！");
            if (umShareListener != null) {
                umShareListener.onResult(share_media);
            }
            dismissDialog();
        }

        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {
            if (throwable != null) {
                YouMengShowToast.show(activity, "分享失败！" + throwable.getMessage());
                throwable.printStackTrace();
            } else {
                YouMengShowToast.show(activity, "分享失败！");
            }
            Log.i("wq", "wq 0518 share_media:" + share_media.toString());
            Log.i("wq", "wq 0518 throwable.getMessage():" + throwable.getMessage());

            if (umShareListener != null) {
                umShareListener.onError(share_media, throwable);
            }
            dismissDialog();
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media) {

//            LogUtils.i("wq 0119 SwTabActivity.swTabActivity:" + SwTabActivity.swTabActivity);
//            if (SwTabActivity.swTabActivity != null) {//放置栈顶
            ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
            activityManager.moveTaskToFront(activity.getTaskId(), ActivityManager.MOVE_TASK_WITH_HOME);
//            }
            if (umShareListener != null) {
                umShareListener.onCancel(share_media);
            }
            dismissDialog();
            YouMengShowToast.show(activity, "分享取消");
        }
    }

    static class ViewHolder {
        TextView loginTip;
        LinearLayout login;
        LinearLayout llShareWeibo;
        LinearLayout llShareWechat;
        LinearLayout llShareComments;
        LinearLayout llShareQq;
        LinearLayout llShareQzone;
        LinearLayout llShareCopy;
        TextView tvSharePopupCancel;

        ViewHolder(View view) {
            loginTip = (TextView) view.findViewById(R.id.login_tip);
            login = (LinearLayout) view.findViewById(R.id.login);
            llShareWeibo = (LinearLayout) view.findViewById(R.id.ll_share_weibo);
            llShareWechat = (LinearLayout) view.findViewById(R.id.ll_share_wechat);
            llShareComments = (LinearLayout) view.findViewById(R.id.ll_share_comments);
            llShareQq = (LinearLayout) view.findViewById(R.id.ll_share_qq);
            llShareQzone = (LinearLayout) view.findViewById(R.id.ll_share_qzone);
            llShareCopy = (LinearLayout) view.findViewById(R.id.ll_share_copy);
            tvSharePopupCancel = (TextView) view.findViewById(R.id.tv_share_popup_cancel);

        }
    }

    /**
     * 分享图片 - view生成的图片
     *
     * @param activity
     * @param share_media
     * @param view
     */
    public static void shareViewImage(Activity activity, SHARE_MEDIA share_media, View view) {
        shareViewImage(activity, share_media, null, view);
    }

    /**
     * 分享图片 - view生成的图片
     *
     * @param activity
     * @param share_media
     * @param view
     */
    public static void shareViewImage(Activity activity, SHARE_MEDIA share_media, UMShareListener umShareListener, View view) {
        final String tmp = "view_img_tmp.png";
        File file = ImageUtil.saveBitmap(ImageUtil.getBitmap(view), tmp);
        shareImage(activity, share_media, new ShareListener(activity, umShareListener), file);
    }

    /**
     * 分享图片
     *
     * @param activity
     * @param shareMedia
     * @param file
     */
    public static void shareImage(Activity activity, SHARE_MEDIA shareMedia, File file) {
        UMImage pic = new UMImage(activity, file);
//        pic.setThumb(new UMImage(activity, "http://pic4.nipic.com/20091121/3764872_215617048242_2.jpg"));
        new ShareAction(activity).withMedia(pic).setPlatform(shareMedia).setCallback(new ShareListener(activity, null)).withText("").share();
    }

    /**
     * 分享图片
     *
     * @param activity
     * @param shareMedia
     * @param umShareListener
     * @param file
     */
    public static void shareImage(Activity activity, SHARE_MEDIA shareMedia, UMShareListener umShareListener, File file) {
        UMImage pic = new UMImage(activity, file);
//        pic.setThumb(new UMImage(activity, "http://pic4.nipic.com/20091121/3764872_215617048242_2.jpg"));
        new ShareAction(activity).withMedia(pic).setPlatform(shareMedia).setCallback(umShareListener).withText("").share();
    }

}
