package wq.share.shareUtil;

import android.app.AlertDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import wq.share.R;


/**
 * Created by victor on 15-9-17.
 */
public class YouMentShowProcessDialog extends AlertDialog.Builder {
    private TextView tvTitle;
    private String _title = "";
    private AlertDialog alertDialog;
    private View view;
    private Context context;

//    DialogInterface.OnDismissListener onDismissListener;

    public YouMentShowProcessDialog(Context context) {
        super(context, R.style.youmeng_process_dialog);
        this.context = context;
    }

    public boolean isShowing() {
        if (alertDialog == null) {
            return false;
        } else {
            return alertDialog.isShowing();
        }
    }

    public void dismiss() {
        try {
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void setShowOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
//        this.onDismissListener = onDismissListener;
//    }

    @Override
    public AlertDialog show() {

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
//        alertDialog.setOnDismissListener(onDismissListener);
        alertDialog = super.show();
        view = LayoutInflater.from(context).inflate(R.layout.umeng_show_process_dialog, null);
        tvTitle = (TextView) view.findViewById(R.id.tv_process_title);
        alertDialog.getWindow().setContentView(view);
        alertDialog.setCancelable(true);

        if (TextUtils.isEmpty(_title)) {
            tvTitle.setVisibility(View.GONE);
        } else {
            tvTitle.setText(_title);
            tvTitle.setVisibility(View.VISIBLE);
        }
        return null;
    }

    public YouMentShowProcessDialog setTitle(String text) {
        _title = text;
        return this;
    }
}
