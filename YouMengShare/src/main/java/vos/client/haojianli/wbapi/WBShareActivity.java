package vos.client.haojianli.wbapi;


//import com.umeng.weixin.callback.WXCallbackActivity;

import android.content.Intent;
import android.util.Log;

import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.media.WBShareCallBackActivity;

public class WBShareActivity extends WBShareCallBackActivity {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode,resultCode,data);//完成回调
        Log.i("wq","wq 0523 分享回调");
    }

}
