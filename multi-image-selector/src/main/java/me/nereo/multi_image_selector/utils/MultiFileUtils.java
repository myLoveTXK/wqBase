package me.nereo.multi_image_selector.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * 文件操作类
 * Created by Nereo on 2015/4/8.
 */
public class MultiFileUtils {

    public static File createTmpFile(Context context){

        String state = Environment.getExternalStorageState();
        if(state.equals(Environment.MEDIA_MOUNTED)){
            // 已挂载
            File pic = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());
//            String fileName = "multi_image_"+timeStamp+"";
            File tmpFile = new File(pic, "tmp.jpg");
            return tmpFile;
        }else{
            File cacheDir = context.getCacheDir();
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA).format(new Date());
//            String fileName = "multi_image_"+timeStamp+"";
            File tmpFile = new File(cacheDir, "tmp.jpg");
            return tmpFile;
        }

    }

}
