package cn.wq.baseActivity.bean;

import java.io.Serializable;

/**
 * Created by xt on 2018/5/7.
 */
public class ShareBean implements Serializable {
    private String url;
    private String title;
    private String content;
    private int shareIcon;

    public int getShareIcon() {
        return shareIcon;
    }

    public void setShareIcon(int shareIcon) {
        this.shareIcon = shareIcon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}