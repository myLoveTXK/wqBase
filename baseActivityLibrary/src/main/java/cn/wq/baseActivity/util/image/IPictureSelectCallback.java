package cn.wq.baseActivity.util.image;

/**
 * Created by Administrator on 2016/11/4.
 */
public interface IPictureSelectCallback {
    void success(String path);
    void fail();
}
