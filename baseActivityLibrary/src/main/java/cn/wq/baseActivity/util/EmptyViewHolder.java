package cn.wq.baseActivity.util;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import cn.wq.baseActivity.R;

/**
 * Created by W~Q on 2017/5/17.
 */

public class EmptyViewHolder {


    public ImageView emptyImg;
    public TextView emptyTitle;
    public TextView emptyContent;
    public FrameLayout emptyLayout;

    public EmptyViewHolder(View view) {
        emptyImg = (ImageView) view.findViewById(R.id.empty_img);
        emptyTitle = (TextView) view.findViewById(R.id.empty_title);
        emptyContent = (TextView) view.findViewById(R.id.empty_content);
        emptyLayout = (FrameLayout) view.findViewById(R.id.empty_layout);
    }
}
