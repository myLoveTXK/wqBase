package cn.wq.baseActivity.util.anim;

import android.app.Activity;

import cn.wq.baseActivity.R;


/**
 * Created by W~Q on 2017/5/16.
 * activity动画
 */

public class ActivityAnimUtil {

    /**
     * 启动由下到上
     *
     * @param activity
     */
    public static void startActivityBottomToTop(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_bottom_in, R.anim.activity_top_out);
    }

    /**
     * 关闭由上到下
     *
     * @param activity
     */
    public static void finishActivityTopToBottom(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_top_in, R.anim.activity_bottom_out);
    }

    /**
     * 关闭由大到小
     *
     * @param activity
     */
    public static void finishActivityBigToSmall(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_top_in, R.anim.activity_big_out);
    }


}
