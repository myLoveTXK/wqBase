package cn.wq.baseActivity.util;

/**
 * Created by xt on 2018/7/31.
 */

public class Config {

    public static String emptyListDataTitle = "暂无数据";
    public static String emptyListDataContent = "点击刷新";

    public static void setEmptyListDataTitle(String emptyListDataTitle) {
        Config.emptyListDataTitle = emptyListDataTitle;
    }

    public static void setEmptyListDataContent(String emptyListDataContent) {
        Config.emptyListDataContent = emptyListDataContent;
    }
}
