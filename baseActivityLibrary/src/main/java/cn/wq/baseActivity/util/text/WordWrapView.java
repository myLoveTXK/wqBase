package cn.wq.baseActivity.util.text;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.util.LogUtils;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.view.image.util.DisplayUtil;


/**
 * Created by Administrator on 2016/9/13.
 * 水平文字换行view
 */
public class WordWrapView extends LinearLayout {
    boolean isRegularWH = true;
    private int regularW = DisplayUtil.getScreenRealLength(200);
    private int regularH = DisplayUtil.getScreenRealLength(70);

    private int textColor = Color.parseColor("#333333");

    protected int PADDING_HOR = 8;//水平方向padding
    protected int PADDING_VERTICAL = 0;//垂直方向padding
    protected int SIDE_MARGIN = DisplayUtil.getScreenRealLength(30);//左右间距
    protected int textHorMargin = DisplayUtil.getScreenRealLength(30);
    protected int textVerticalMargin = DisplayUtil.getScreenRealLength(30);
    protected Context context;
    private OnWordItemListener onWordItemListener;
    private int textBackgroundResource = R.drawable.bg_word_bg;

    public void setTextHorMargin(int textHorMargin) {
        this.textHorMargin = textHorMargin;
    }

    public void setTextVerticalMargin(int textVerticalMargin) {
        this.textVerticalMargin = textVerticalMargin;
    }

    /**
     * @param context
     */
    public WordWrapView(Context context) {
        super(context);
        init(context);
    }

    public void setOnWordItemListener(OnWordItemListener onWordItemListener) {
        this.onWordItemListener = onWordItemListener;
    }

    private void init(Context context) {
        this.context = context;
    }

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    public WordWrapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }


    /**
     * @param context
     * @param attrs
     */
    public WordWrapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public int getTextBackgroundResource() {
        return textBackgroundResource;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        int autualWidth = r - l;
        int x = SIDE_MARGIN;// 横坐标开始
        int y = 0;//纵坐标开始
        int rows = 1;
        for (int i = 0; i < childCount; i++) {
            final int position = i;
            final TextView view = (TextView) getChildAt(i);
            view.setGravity(Gravity.CENTER);
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onWordItemListener != null) {
                        onWordItemListener.onWordItemClick(position, view.getText().toString());
                    }
                }
            });

            int width = view.getMeasuredWidth();
            int height = view.getMeasuredHeight();

            if (y == 0) {
                y = height;
            }
            if (x == SIDE_MARGIN) {
                x = x + width;
            } else {
                x = x + textHorMargin + width;
            }

            boolean isNextRows = false;
            if (x > autualWidth) {
//                if (x - textHorMargin > autualWidth) {
                x = width + SIDE_MARGIN;
                rows++;
                isNextRows = true;
//                } else {
//                    x = x - textHorMargin;
//                }

            }
            if (isNextRows) {
                y = y + textVerticalMargin + height;
            }
//            if (i == 0) {
//                view.layout(0, y - height, x - textHorMargin, y);
//            } else if (isNextRows) {
//                view.layout(0, y - height, x - textHorMargin, y);
//            } else {
            view.layout(x - width, y - height, x, y);
            view.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setForegroundGravity(Gravity.CENTER);
            }
//            }

//            if (i == 0) {
//                view.layout(x - width - TEXT_MARGIN, y - height, x - TEXT_MARGIN, y);
//            } else {
//                view.layout(x - width, y - height, x, y);
//            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int x = 0;//横坐标
        int y = 0;//纵坐标
        int rows = 1;//总行数
        int specWidth = MeasureSpec.getSize(widthMeasureSpec);

        int actualWidth = specWidth/* - SIDE_MARGIN * 2*/;//实际宽度
        int childCount = getChildCount();
        for (int index = 0; index < childCount; index++) {
            View child = getChildAt(index);

            if (rows == 0) {
                //zheli
                child.setPadding(PADDING_HOR, PADDING_VERTICAL, PADDING_HOR, PADDING_VERTICAL);
            } else {
                child.setPadding(PADDING_HOR, PADDING_VERTICAL, PADDING_HOR, PADDING_VERTICAL);
            }

            int width = child.getMeasuredWidth();
            int height = child.getMeasuredHeight();
            if (isRegularWH) {
                child.measure(width, height);
            } else {
                child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            }

            LogUtils.i("wq 0118 width:" + width);
            LogUtils.i("wq 0118 height:" + height);
            if (y == 0) {
                y = height;
            }
            if (x == 0) {
                x = width;
            } else {
                x += width + textHorMargin;
            }
            if (x > actualWidth) {//换行
                x = width;
                rows++;
                y = y + textVerticalMargin + height;
            }
            child.setForegroundGravity(Gravity.CENTER);
        }

        setMeasuredDimension(actualWidth, y);
    }

    public void addText(String tag) {
        TextView textView = new TextView(context);
        textView.setText(tag);
//        textView.setTextColor(context.getResources().getColor(R.color.sw_color_666666));
        textView.setBackgroundResource(getTextBackgroundResource());
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(textColor);
        textView.setTextSize(13);
        if (isRegularWH) {
            textView.setWidth(regularW);
            textView.setHeight(regularH);
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//            layoutParams.gravity = Gravity.CENTER;
//            textView.setLayoutParams(layoutParams);
        }

        addView(textView);
        textView.setGravity(Gravity.CENTER);
//        LayoutParams layoutParams= textView.getLayoutParams();
//        layoutParams.
    }

    public void setTextBackgroundResource(int textBackgroundResource) {
        this.textBackgroundResource = textBackgroundResource;
    }

    public void clear() {
        removeAllViews();
        invalidate();
    }

    public interface OnWordItemListener {
        void onWordItemClick(int position, String text);
    }
}