package cn.wq.baseActivity.util.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Base64;
import android.util.Log;

import com.lidroid.xutils.util.LogUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * @author W~Q 2015-12-25
 */
public class ImageUtil {

    /**
     * 压缩后的固定宽度
     */
//    private static final int afterCompressionWidth = 640;
    private static final int afterCompressionWidth = 320;

    private static final Size ZERO_SIZE = new Size(0, 0);
    private static final Options OPTIONS_GET_SIZE = new Options();
    private static final Options OPTIONS_DECODE = new Options();

    /**
     * 获取压缩的图片的大小，自适应高度
     *
     * @return Size
     */
    private static Size getFixedCompressionSize(Bitmap result) {
        int oldWidth = result.getWidth();
        int oldHeight = result.getHeight();
        int newWidth = afterCompressionWidth;
        int newHeight = (int) ((Float.valueOf(newWidth) / Float.valueOf(oldWidth)) * Float.valueOf(oldHeight));
        return new Size(newWidth, newHeight);
    }

    /**
     * 修改图片尺寸并保存
     *
     * @param filePath
     * @return 返回保存的路径
     */
    public static String saveCompressionBitmap(String filePath) {

        Bitmap bm = BitmapFactory.decodeFile(filePath);
        if (bm == null) {
            return null;
        }
        Size fixedCompressionSize = getFixedCompressionSize(bm);
        String newPath = saveCompressionBitmap(filePath, fixedCompressionSize.width, fixedCompressionSize.height);
        return newPath;
    }

    /**
     * 修改图片尺寸并保存
     *
     * @param bitmap
     * @return 返回保存的路径
     */
    public static String saveCompressionBitmap(Bitmap bitmap) {


        Size fixedCompressionSize = getFixedCompressionSize(bitmap);
        String newPath = saveCompressionBitmap(bitmap, fixedCompressionSize.width, fixedCompressionSize.height);
        return newPath;
    }

    /**
     * 修改图片尺寸并保存
     *
     * @param filePath
     * @param width
     * @param height
     * @return 返回保存的路径
     */
    public static String saveCompressionBitmap(String filePath, int width, int height) {
        File file = new File(filePath);
        if (!file.exists()) {
            Log.v("wq", "当前文件不存在:" + filePath);
            return null;
        }
        Size size = getBitMapSize(file);
        if (size.width <= width && size.height <= height) {
            return filePath;
        }
        Bitmap bitmap = createBitmap(file, width, height);
        String tempPath = ImageFiles.getTempPath() + UUID.randomUUID().toString() + ".png";
        boolean flag = saveCompressionBitmap(bitmap, tempPath);
        return flag ? tempPath : filePath;
    }

    /**
     * 修改图片尺寸并保存
     *
     * @param originalBitmap
     * @param width
     * @param height
     * @return 返回保存的路径
     */
    public static String saveCompressionBitmap(Bitmap originalBitmap, int width, int height) {

        Bitmap bitmap = createBitmap(originalBitmap, width, height);
        String tempPath = ImageFiles.getTempPath() + UUID.randomUUID().toString();
        boolean flag = saveCompressionBitmap(bitmap, tempPath);
        return flag ? tempPath : null;
    }

    /**
     * 保存压缩的图片
     *
     * @param bitmap
     * @param tempPath
     * @return
     */
    private static boolean saveCompressionBitmap(Bitmap bitmap, String tempPath) {
        boolean isSuccess = false;
        try {
            isSuccess = ImageUtil.save(bitmap, tempPath);
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.isRecycled();
                bitmap = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    /**
     * 返回图片
     *
     * @param filePath 图片路径
     * @return 解析不出来返回null
     */
    public static Bitmap getBitmap(String filePath) {
        Bitmap bm = BitmapFactory.decodeFile(filePath);
        if (bm == null) {
            return null;
        }
        return bm;
    }

    /**
     * 保存图像
     *
     * @param bmp
     * @param filename
     * @return
     * @throws Exception
     */
    public static boolean save(final Bitmap bmp, final String filename)
            throws Exception {
        LogUtils.v("wq 1229 createNewFile-filename:" + filename);
        File file = new File(filename);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        final CompressFormat format = CompressFormat.JPEG;
        final int quality = 80;
        OutputStream stream = null;

        stream = new FileOutputStream(filename);
        boolean success = bmp.compress(format, quality, stream);
        stream.close();
        return success;
    }

    /**
     * 获取图片的高宽
     *
     * @param file
     * @return
     */
    public static Size getBitMapSize(File file) {
        OPTIONS_GET_SIZE.inJustDecodeBounds = true;
        if (file.exists()) {
            InputStream in = null;
            try {
                in = new FileInputStream(file);
                BitmapFactory.decodeStream(in, null, OPTIONS_GET_SIZE);
                return new Size(OPTIONS_GET_SIZE.outWidth,
                        OPTIONS_GET_SIZE.outHeight);
            } catch (FileNotFoundException e) {
                return ZERO_SIZE;
            } finally {
                closeInputStream(in);
            }
        }
        return ZERO_SIZE;
    }

    /**
     * 通过图片路径,宽度高度创建一个Bitmap对象
     *
     * @param file
     * @param width
     * @param height
     * @return
     */
    public static Bitmap createBitmap(File file, int width, int height) {
        if (file.exists()) {
            InputStream in = null;
            try {
                in = new FileInputStream(file);
                Size size = getBitMapSize(file);
                if (size.equals(ZERO_SIZE)) {
                    return null;
                }
                int scale = 1;
                int a = size.getWidth() / width;
                int b = size.getHeight() / height;
                scale = Math.max(a, b);
                synchronized (OPTIONS_DECODE) {
                    OPTIONS_DECODE.inSampleSize = scale;
                    Bitmap bitMap = BitmapFactory.decodeStream(in, null,
                            OPTIONS_DECODE);
                    return bitMap;
                }
            } catch (FileNotFoundException e) {
                Log.e("wq", "createBitmap==" + e.toString());
            } finally {
                closeInputStream(in);
            }
        }
        return null;
    }

    /**
     * 通过图片路径,宽度高度创建一个Bitmap对象
     *
     * @param bitmap
     * @param width
     * @param height
     * @return
     */
    public static Bitmap createBitmap(Bitmap bitmap, int width, int height) {
        if (bitmap != null) {
//
            Size size = new Size(bitmap.getWidth(), bitmap.getHeight());
            if (size.equals(ZERO_SIZE)) {
                return null;
            }
            int scale = 1;
            int a = size.getWidth() / width;
            int b = size.getHeight() / height;
            scale = Math.max(a, b);
            OPTIONS_DECODE.inSampleSize = scale;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(CompressFormat.PNG, 100, baos);

            Bitmap bitMap = BitmapFactory.decodeByteArray(baos.toByteArray(), 0, baos.size(), OPTIONS_DECODE);
            return bitMap;

        }
        return null;
    }


    // 关闭输入流
    private static void closeInputStream(InputStream in) {
        if (null != in) {
            try {
                in.close();
            } catch (IOException e) {
                Log.e("wq", "closeInputStream==" + e.toString());
            }
        }
    }


    // 图片大小
    static class Size {
        private int width, height;

        Size(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }

    /**
     * @param imgPath
     * @return
     */
    public static String imgToBase64(String imgPath) {
        Bitmap bitmap = null;
        if (imgPath != null && imgPath.length() > 0) {
            bitmap = getBitmap(imgPath);
        }
        if (bitmap == null) {
            //bitmap not found!!
        }
        ByteArrayOutputStream out = null;
        try {
            out = new ByteArrayOutputStream();
            bitmap.compress(CompressFormat.JPEG, 100, out);

            out.flush();
            out.close();

            byte[] imgBytes = out.toByteArray();
            return Base64.encodeToString(imgBytes, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param bitmap
     * @return
     */
    public static String imgToBase64(Bitmap bitmap) {

        if (bitmap == null) {
            //bitmap not found!!
            return null;
        }
        ByteArrayOutputStream out = null;
        try {
            out = new ByteArrayOutputStream();
            bitmap.compress(CompressFormat.JPEG, 100, out);

            out.flush();
            out.close();

            byte[] imgBytes = out.toByteArray();
            return Base64.encodeToString(imgBytes, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

//    private static Bitmap readBitmap(String imgPath) {
//        try {
//            return BitmapFactory.decodeFile(imgPath);
//        } catch (Exception e) {
//            return null;
//        }
//
//    }

    /**
     * @param base64Data
     * @param imgName
     * @param imgFormat  图片格式
     */
    public static void base64ToBitmap(String base64Data, String imgName, String imgFormat) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        File myCaptureFile = new File("/sdcard/", imgName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myCaptureFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        boolean isTu = bitmap.compress(CompressFormat.JPEG, 100, fos);
        if (isTu) {
            // fos.notifyAll();  
            try {
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 剪切图片
     *
     * @param filePath
     * @param percentageStartX
     * @param percentageStartY
     * @param percentageWidth
     * @param percentageHeight
     * @return
     */
    public static Bitmap tailoringImage(String filePath,
                                        double percentageStartX, double percentageStartY,
                                        double percentageWidth, double percentageHeight) {
        Bitmap bitmap = getBitmap(filePath);
        final Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,
                (int) (bitmap.getWidth() * percentageStartX),
                (int) (bitmap.getHeight() * percentageStartY),
                (int) (bitmap.getWidth() * percentageWidth),
                (int) (bitmap.getWidth() * percentageHeight));
        return croppedBitmap;
    }
//    String tempPath = Files.getTempPath() + UUID.randomUUID().toString();

    /**
     * 设置仿IOS模糊
     *
     * @param context
     * @param sentBitmap
     * @param radius
     * @return
     */
    public static Bitmap blurImg(Context context, Bitmap sentBitmap, int radius) {

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int temp = 256 * divsum;
        int dv[] = new int[temp];
        for (i = 0; i < temp; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16)
                        | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);
        return (bitmap);
    }


    /**
     * 转换图片成圆形
     *
     * @param bitmap 传入Bitmap对象
     * @return
     */
    public Bitmap toRoundBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float roundPx;
        float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;

        //无非就是计算圆形区域
        if (width <= height) {
            roundPx = width / 2;
            float clip = (height - width) / 2;
            top = clip;
            bottom = width + clip;
            left = 0;
            right = width;

            height = width;

            dst_left = 0;
            dst_top = 0;
            dst_right = width;
            dst_bottom = width;
        } else {
            roundPx = height / 2;
            float clip = (width - height) / 2;

            left = clip;
            right = height + clip;
            top = 0;
            bottom = height;

            width = height;

            dst_left = 0;
            dst_top = 0;
            dst_right = height;
            dst_bottom = height;
        }

        Bitmap output = Bitmap.createBitmap(width,
                height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect src = new Rect((int) left, (int) top, (int) right, (int) bottom);
        final Rect dst = new Rect((int) dst_left, (int) dst_top, (int) dst_right, (int) dst_bottom);
        final RectF rectF = new RectF(dst);

        paint.setAntiAlias(true);

        paint.setColor(0xFFFFFFFF);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, src, dst, paint);
        return output;
    }

}
