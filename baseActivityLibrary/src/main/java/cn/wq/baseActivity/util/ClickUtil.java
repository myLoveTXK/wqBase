package cn.wq.baseActivity.util;

/**
 * Created by Administrator on 2016/12/21.
 */
public class ClickUtil {
    private static long lastClickTime ;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }
}
