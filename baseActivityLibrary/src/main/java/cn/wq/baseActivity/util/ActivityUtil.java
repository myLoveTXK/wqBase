package cn.wq.baseActivity.util;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

public class ActivityUtil {
    final static List<Activity> activities = new ArrayList<>();


    public static void addActivity(Activity activity) {
        if (!activities.contains(activity)) {
            activities.add(activity);
        }
    }

    public static void removeActivity(Activity activity) {
        if (activities.contains(activity)) {
            activities.remove(activity);
        }
    }

    public static List<Activity> getActivities() {
        return activities;
    }
}
