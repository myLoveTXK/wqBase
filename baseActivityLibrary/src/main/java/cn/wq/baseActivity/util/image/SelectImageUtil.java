package cn.wq.baseActivity.util.image;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.interfaces.IActivityCallback;
import cn.wq.baseActivity.util.file.Files;
import me.kareluo.imaging.IMGEditActivity;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;


/**
 * Created by xt on 2017/4/14.
 */

public class SelectImageUtil {


    /**
     * 编辑图片
     *
     * @param baseActivity
     * @param path
     * @param iPictureSelectCallback
     */
    public static void editPicture(BaseActivity baseActivity, String path, final IPictureSelectCallback iPictureSelectCallback) {
        Intent intent = new Intent(baseActivity, IMGEditActivity.class);

        File picture = new File(path);
        intent.putExtra(IMGEditActivity.EXTRA_IMAGE_URI, Uri.fromFile(picture));

        final String editPicturePath = Files.getNewTempFileName() + ".png";
        intent.putExtra(IMGEditActivity.EXTRA_IMAGE_SAVE_PATH, editPicturePath);
        baseActivity.startActivityForResult(intent, new IActivityCallback() {
            @Override
            public void callback(int resultCode, Intent data) {
                if (iPictureSelectCallback != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        iPictureSelectCallback.success(editPicturePath);
                    } else {
                        iPictureSelectCallback.fail();
                    }
                }

            }
        });
    }

    /**
     * 选择系统拍照功能
     */
    public static void selectCameraPicture(BaseActivity baseActivity, final IPictureSelectCallback iPictureSelectCallback) {
        if (baseActivity == null) {
            return;
        }
        // 跳转到系统照相机
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(baseActivity.getPackageManager()) != null) {
            // 设置系统相机拍照后的输出路径
            // 创建临时文件
            final File mTmpFile = new File(Files.getTempPath() + "paizhao.png");
            if (mTmpFile.exists()) {
                mTmpFile.delete();
            }
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mTmpFile));
            baseActivity.startActivityForResult(cameraIntent, new IActivityCallback() {
                @Override
                public void callback(int resultCode, Intent data) {
                    if (iPictureSelectCallback != null) {
                        if (resultCode == Activity.RESULT_OK) {
                            iPictureSelectCallback.success(mTmpFile.getPath());
                        } else {
                            iPictureSelectCallback.fail();
                        }
                    }
                }
            });
        } else {
            Toast.makeText(baseActivity, R.string.msg_no_camera, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 选择照片
     *
     * @param baseActivity
     * @param iPictureSelectCallback
     */
    public static void selectPicture(final BaseActivity baseActivity, boolean isShowCamera, final IPictureSelectCallback iPictureSelectCallback) {
//        setiPictureSelectCallback(iPictureSelectCallback);
        boolean showCamera = isShowCamera;
        int maxNum = 1;
        int selectedMode = MultiImageSelectorActivity.MODE_SINGLE;
        Intent intent = new Intent();
        // 是否显示拍摄图片
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, showCamera);
        // 最大可选择图片数量
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, maxNum);
        // 选择模式
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
        // 默认选择
//                if (mSelectPath != null && mSelectPath.size() > 0) {
//                    intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
//                }
        baseActivity.startActivityForResult(MultiImageSelectorActivity.class, intent, new IActivityCallback() {
            @Override
            public void callback(int resultCode, Intent data) {
                if (iPictureSelectCallback != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        ArrayList<String> mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                        String path = "";
                        for (String p : mSelectPath) {
                            if (TextUtils.isEmpty(p)) {
                                continue;
                            }
                            path = p;
                            break;
                        }
                        iPictureSelectCallback.success(path);
                    } else {
                        iPictureSelectCallback.fail();
                    }
                }
            }
        });
    }

    /**
     * 选择照片
     *
     * @param baseActivity
     * @param num
     * @param showCamera
     * @param iPictureManySelectCallback
     */
    public static void selectManyPicture(final BaseActivity baseActivity, int num, boolean showCamera, final IPictureManySelectCallback iPictureManySelectCallback) {
//        setiPictureSelectCallback(iPictureSelectCallback);
//        boolean showCamera = true;
        int maxNum = num;
        if (maxNum <= 0) {
            maxNum = 1;
        }
//        int selectedMode = MultiImageSelectorActivity.MODE_SINGLE;
        int selectedMode = MultiImageSelectorActivity.MODE_MULTI;
        Intent intent = new Intent();
        // 是否显示拍摄图片
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, showCamera);
        // 最大可选择图片数量
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, maxNum);
        // 选择模式
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
        // 默认选择
//                if (mSelectPath != null && mSelectPath.size() > 0) {
//                    intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
//                }
        baseActivity.startActivityForResult(MultiImageSelectorActivity.class, intent, new IActivityCallback() {
            @Override
            public void callback(int resultCode, Intent data) {
                if (iPictureManySelectCallback != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        ArrayList<String> mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                        iPictureManySelectCallback.success(mSelectPath);
                    } else {
                        iPictureManySelectCallback.fail();
                    }
                }
            }
        });
    }


    /**
     * 选择用户头像
     *
     * @param baseActivity
     * @param iPictureSelectCallback
     */
    public static void selectUserHead(final BaseActivity baseActivity, final IPictureSelectCallback iPictureSelectCallback) {
        selectPicture(baseActivity, true, new IPictureSelectCallback() {
            @Override
            public void success(String path) {
                pictureCaijian(baseActivity, path, new OnCaijianCallback() {
                    @Override
                    public void caijianSuccess(String path) {
                        if (iPictureSelectCallback != null) {
                            iPictureSelectCallback.success(path);
                        }
                    }

                    @Override
                    public void caijianFail() {
                        if (iPictureSelectCallback != null) {
                            iPictureSelectCallback.fail();
                        }
                    }
                });
            }

            @Override
            public void fail() {
                if (iPictureSelectCallback != null) {
                    iPictureSelectCallback.fail();
                }
            }
        });
    }


    /**
     * 裁剪图片
     *
     * @param path
     */
    public static void pictureCaijian(BaseActivity baseActivity, String path, final OnCaijianCallback onCaijianCallback) {
        if (baseActivity == null || baseActivity.isFinishing()) {
            return;
        }
        try {
            if (baseActivity.isDestroyedSw()) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String caijianTempFile = ImageFiles.getTempPath() + "caijian-" + UUID.randomUUID() + ".png";
        Intent intent1 = new Intent("com.android.camera.action.CROP");
        intent1.setDataAndType(Uri.fromFile(new File(path)), "image/*");
        intent1.putExtra("crop", true);
        intent1.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(caijianTempFile)));//
        intent1.putExtra("aspectX", 1);
        intent1.putExtra("aspectY", 1);
//                    intent1.putExtra("outputFormat", Bitmap.CompressFormat.JPEG);
        intent1.putExtra("outputX", 500);
        intent1.putExtra("outputY", 500);
        intent1.putExtra("return-data", false);
        baseActivity.startActivityForResult(null, intent1, new IActivityCallback() {
            @Override
            public void callback(int resultCode, Intent data) {

                if (resultCode == Activity.RESULT_OK) {
                    if (onCaijianCallback != null) {
                        onCaijianCallback.caijianSuccess(caijianTempFile);
                    }
//                    iPictureSelectCallback.success(caijianTempFile);
                } else {
                    if (onCaijianCallback != null) {
                        onCaijianCallback.caijianFail();
                    }
                }
            }
        });
//        startActivityForResult(intent1, REQUEST_BASE_PICTURE_CAIJIAN);
    }


    public interface OnCaijianCallback {
        void caijianSuccess(String path);

        void caijianFail();
    }
}
