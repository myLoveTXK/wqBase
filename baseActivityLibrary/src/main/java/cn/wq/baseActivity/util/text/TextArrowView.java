package cn.wq.baseActivity.util.text;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.andview.refreshview.utils.LogUtils;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.view.edit.ClearEditText;
import cn.wq.baseActivity.view.image.WqImageView;


/**
 * Created by W~Q on 2017/04/14.
 */
public class TextArrowView extends FrameLayout {
    Context context;
    ClearEditText edit;

    private TextView tv_left, tv_middle, edit_mark;
    private ImageView img_left;
    private ImageView img_right;
    private ImageView noRead;
    private WqImageView rightWqImg;
    private FrameLayout img_left_layout;

    public TextView getTv_left() {
        return tv_left;
    }

    public FrameLayout getImg_left_layout() {
        return img_left_layout;
    }

    public TextArrowView(Context context) {
        this(context, null);
    }

    public TextArrowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LogUtils.i("wq 0412 TextArrowView 初始化");
        initView(context, attrs);
    }

    public void setImg_left(int res) {
        if (res == 0) {
            img_left.setVisibility(GONE);
            img_left_layout.setVisibility(GONE);
        } else {
            img_left.setVisibility(VISIBLE);
            img_left_layout.setVisibility(VISIBLE);
            img_left.setImageResource(res);
        }
    }

    public void setTv_left(String str) {
        tv_left.setText(str);
    }

    public void setShowNoRead(boolean isShow) {
        if (noRead == null) {
            return;
        }
        noRead.setVisibility(isShow ? VISIBLE : GONE);
    }

    public void setTv_middleBlack() {
        if (tv_middle != null) {
            tv_middle.setTextColor(context.getResources().getColor(R.color.base_sw_text_black_333333));
        }
    }

    private void initView(Context context, AttributeSet attrs) {
        this.context = context;
        View parent = LayoutInflater.from(context).inflate(R.layout.common_view_text_arrow, null);
//        View parent = LayoutInflater.from(context).inflate(R.layout.test_lllll, null, false);
        tv_left = (TextView) parent.findViewById(R.id.tv_left);
        edit_mark = (TextView) parent.findViewById(R.id.edit_mark);
        edit = (ClearEditText) parent.findViewById(R.id.edit);
        tv_middle = (TextView) parent.findViewById(R.id.tv_middle);
        img_left = (ImageView) parent.findViewById(R.id.img_left);
        img_left_layout = (FrameLayout) parent.findViewById(R.id.img_left_layout);
        img_right = (ImageView) parent.findViewById(R.id.img_right);
        noRead = (ImageView) parent.findViewById(R.id.no_read);
        rightWqImg = (WqImageView) parent.findViewById(R.id.right_sw_img);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TextArrowView);
        setAttributes(context, array);
        addView(parent);
    }

    /**
     * 设置属性
     *
     * @param context
     * @param array
     */
    public void setAttributes(Context context, TypedArray array) {
        if (array == null) {
            return;
        }
        for (int i = 0; i < array.getIndexCount(); i++) {
            int attr = array.getIndex(i);
            if (attr == R.styleable.TextArrowView_left_text_size) {
                float mTextSize = array.getDimension(attr, 14);
                tv_left.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
            } else if (attr == R.styleable.TextArrowView_edit_mark_size) {
                float editMarkSize = array.getDimension(attr, 14);
                edit_mark.setTextSize(TypedValue.COMPLEX_UNIT_PX, editMarkSize);
            } else if (attr == R.styleable.TextArrowView_edit_text_size) {
                float editSize = array.getDimension(attr, 14);
                edit.setTextSize(TypedValue.COMPLEX_UNIT_PX, editSize);
            } else if (attr == R.styleable.TextArrowView_middle_text_color) {
                tv_middle.setTextColor(Color.parseColor("#bbbbbb"));
            } else if (attr == R.styleable.TextArrowView_left_text_color) {
                tv_left.setTextColor(context.getResources().getColor(array.getResourceId(attr, R.color.base_sw_text_black_333333)));
            } else if (attr == R.styleable.TextArrowView_left_text) {
                tv_left.setText(array.getText(attr));
            } else if (attr == R.styleable.TextArrowView_middle_edit_hint) {
                edit.setHint(array.getText(attr));
            } else if (attr == R.styleable.TextArrowView_need_middle_edit) {
                edit.setVisibility(array.getBoolean(attr, false) ? VISIBLE : GONE);
            } else if (attr == R.styleable.TextArrowView_need_edit_mark) {
                edit_mark.setVisibility(array.getBoolean(attr, false) ? VISIBLE : GONE);
            } else if (attr == R.styleable.TextArrowView_edit_mark) {
                edit_mark.setText(array.getText(attr));
            } else if (attr == R.styleable.TextArrowView_middle_text_left) {
                tv_middle.setGravity(array.getBoolean(attr, false) ? Gravity.LEFT : Gravity.RIGHT);
            } else if (attr == R.styleable.TextArrowView_middle_text) {
                tv_middle.setText(array.getText(attr));
            } else if (attr == R.styleable.TextArrowView_left_img) {
                Drawable drawable = array.getDrawable(attr);
                img_left.setImageDrawable(drawable);
                if (drawable == null) {
                    img_left.setVisibility(GONE);
                    img_left_layout.setVisibility(GONE);
                } else {
                    img_left.setVisibility(VISIBLE);
                    img_left_layout.setVisibility(VISIBLE);
                }
            } else if (attr == R.styleable.TextArrowView_need_right) {
                if (array.getBoolean(attr, true)) {
                    img_right.setVisibility(View.VISIBLE);
                } else {
                    img_right.setVisibility(View.GONE);
//                        tv_middle.setPadding(tv_middle.getPaddingLeft(),tv_middle.getPaddingTop(),130,tv_middle.getBottom());
                }
            } else if (attr == R.styleable.TextArrowView_right_img) {
                try {
                    Drawable drawableRight = array.getDrawable(attr);
                    img_right.setImageDrawable(drawableRight);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        array.recycle();
    }

    public void setNeedImgRight(boolean isNeed) {
        if (isNeed) {
            img_right.setVisibility(View.VISIBLE);
        } else {
            img_right.setVisibility(View.GONE);
        }
    }

    public void setMiddleText(String txt) {
        tv_middle.setText(txt);
    }

    public String getMiddleText() {
        return tv_middle.getText().toString();
    }

    public WqImageView getRightWqImg() {
        return rightWqImg;
    }

    public ClearEditText getEdit() {
        return edit;
    }
}
