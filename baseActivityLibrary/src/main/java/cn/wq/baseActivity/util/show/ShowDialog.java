package cn.wq.baseActivity.util.show;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.util.ClickUtil;


/**
 * Created by fionera on 15-11-12.
 */
public class ShowDialog {

    /**
     * 显示dialog
     *
     * @param context
     * @param isCancelable
     * @param content
     * @param left
     * @param right
     * @param leftStr
     * @param rightStr
     */
    public static void showSelect(final Context context, boolean isCancelable, String title, String content, final View.OnClickListener left, final View.OnClickListener right, String leftStr, String rightStr) {

        showSelect(context, isCancelable, title, content, left, right, leftStr, rightStr, 0);

    }

    private static int showLevel = -1;

    /**
     * @param context
     * @param isCancelable
     * @param title
     * @param content
     * @param left
     * @param right
     * @param leftStr
     * @param rightStr
     * @param level        显示等级，越高，代表优先
     */
    public static synchronized void showSelect(final Context context, final boolean isCancelable, final String title, final String content, final View.OnClickListener left, final View.OnClickListener right, final String leftStr, final String rightStr, final int level) {
        if (level != 0 && showLevel != -1 && level > showLevel) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showSelect(context, isCancelable, title, content, left, right, leftStr, rightStr, level);
                }
            }, 1000 * 2);
            return;
        }
        showLevel = level;
        try {
//            final AlertDialog myAlertDialog = new AlertDialog.Builder(context, R.style.process_dialog).setCancelable(isCancelable).create();
            final AlertDialog myAlertDialog = new AlertDialog.Builder(context, R.style.dialog).setCancelable(isCancelable).create();
            myAlertDialog.show();
            myAlertDialog.setContentView(R.layout.alterdialog_select);
            TextView titleTv = (TextView) myAlertDialog.getWindow().findViewById(R.id.title);
            TextView contentTv = (TextView) myAlertDialog.getWindow().findViewById(R.id.content);
            TextView leftButton = (TextView) myAlertDialog.getWindow().findViewById(R.id.left_button);
            TextView rightButton = (TextView) myAlertDialog.getWindow().findViewById(R.id.right_button);
            leftButton.setText("" + leftStr);
            rightButton.setText("" + rightStr);
            contentTv.setText("" + content);

            if (TextUtils.isEmpty(title)) {
                titleTv.setVisibility(View.GONE);
            } else {
                titleTv.setText(title + "");
            }
            leftButton.setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (ClickUtil.isFastDoubleClick()) {
                                return;
                            }
                            if (left != null) {
                                left.onClick(v);
                            }
//                            if (isCanClose)
                            myAlertDialog.dismiss();
                            showLevel = -1;
                        }
                    });
            rightButton.setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (ClickUtil.isFastDoubleClick()) {
                                return;
                            }
                            if (right != null) {
                                right.onClick(v);
                            }
                            myAlertDialog.dismiss();
                            showLevel = -1;
                        }
                    });
            myAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    showLevel = -1;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 显示dialog
     */
    public static void showConfrim(final Context context, String title, String content, final View.OnClickListener confrimListener, String confrimStr) {
        try {
            final AlertDialog myAlertDialog = new AlertDialog.Builder(context, R.style.dialog).setCancelable(true).create();
            myAlertDialog.setCancelable(false);
            myAlertDialog.show();
            myAlertDialog.setContentView(R.layout.alterdialog_confrim);
            TextView titleTv = (TextView) myAlertDialog.getWindow().findViewById(R.id.title);
            TextView contentTv = (TextView) myAlertDialog.getWindow().findViewById(R.id.content);
            TextView confirm = (TextView) myAlertDialog.getWindow().findViewById(R.id.confirm);
            confirm.setText("" + confrimStr);
            contentTv.setText("" + content);

            if (TextUtils.isEmpty(title)) {
                titleTv.setVisibility(View.GONE);
            } else {
                titleTv.setText(title + "");
            }

            confirm.setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (ClickUtil.isFastDoubleClick()) {
                                return;
                            }
                            if (confrimListener != null) {
                                confrimListener.onClick(v);
                            }
//                            if (isCanClose)
                            myAlertDialog.dismiss();

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    private static AlertDialog showValidationDialog = null;


    public interface ValidationUserCallback {
        void confirm(AlertDialog myAlertDialog, String validationCode);
    }

}
