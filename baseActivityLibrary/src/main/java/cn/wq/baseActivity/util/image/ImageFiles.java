package cn.wq.baseActivity.util.image;

import android.os.Environment;

import java.io.File;

public final class ImageFiles {

	public static final String PATH = "/wq_base_path";

	private static final String	AUDIODIR	= "/audio";
	private static final String	FACESDIR	= "/faces";
	private static final String	PHOTODIR	= "/photo";
	private static final String	APKDIR		= "/apk";
	private static final String	TEMPDIR		= "/tmp";
	private static final String	APPDIR		= "/app";
	private static final String	DYNAMICDIR	= "/dynamic";

	public static final String getBase() {
		return Environment.getExternalStorageDirectory().getPath() + PATH;
	}

	/**
	 * 临时存放路径
	 * @return
	 */
	public static String getTempPath() {
		String path=getBase() + TEMPDIR + "/";
		File file=new File(path);
		if(!file.exists()){
			file.mkdirs();
		}
		return path;
	}

	/**
	 * 递归删除目录下的所有文件及子目录下所有文件
	 * @param dir 将要删除的文件目录
	 * @return boolean Returns "true" if all deletions were successful.
	 *                 If a deletion fails, the method stops attempting to
	 *                 delete and returns "false".
	 */
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			//递归删除目录中的子目录下
			for (int i=0; i<children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		// 目录此时为空，可以删除
		return dir.delete();
	}

	/**
	 * 递归删除目录下的所有文件及子目录下所有文件
	 * @param path
	 * @return
	 */
	public static boolean deleteDir(String path) {
		return deleteDir(new File(path));
	}

}
