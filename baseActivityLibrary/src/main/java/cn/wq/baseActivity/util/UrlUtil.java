package cn.wq.baseActivity.util;

/**
 * Created by xt on 2017/10/11.
 */

public class UrlUtil {


    /**
     * 获取标准url
     * 1.去掉多余斜杠
     * @param url
     * @return
     */
    public static String getStandardUrl(String url) {
        final String reserveHttp = "WQ_haha_heihei_xixi_no_s";
        final String http = "http://";
        final String https = "https://";

        final String reserveHttps = "WQ_haha_heihei_xixi_has_s";
        if (url != null) {
            if (url.contains(http)) {
                url = url.replace(http, reserveHttp);
            }
            if (url.contains(https)) {
                url = url.replace(https, reserveHttps);
            }
            url = url.replace("//", "/");
            url = url.replace(reserveHttp, http);
            url = url.replace(reserveHttps, https);

            url.replace(" ", "");
        }
        return url;
    }
}
