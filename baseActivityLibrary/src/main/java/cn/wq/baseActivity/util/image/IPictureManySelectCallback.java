package cn.wq.baseActivity.util.image;

import java.util.List;

/**
 * Created by Administrator on 2016/11/4.
 */
public interface IPictureManySelectCallback {
    void success(List<String> path);
    void fail();
}
