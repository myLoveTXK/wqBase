package cn.wq.baseActivity.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import shinyway.request.utils.JsonBeanUtil;

/**
 * Created by xt on 2018/7/20.
 */

public class TimeUtil {

    /**
     * 格式化
     *
     * @param beforeTime       yyyy-MM-dd HH:mm:ss
     * @param formatRegulation
     * @return
     */
    public static String formatTime(String beforeTime, String fromFormatRegulation, String formatRegulation) {
        Date date = stringToDate(beforeTime, fromFormatRegulation);
        if (date == null) {
            return beforeTime;
        } else {
            SimpleDateFormat format = new SimpleDateFormat(formatRegulation);
            return format.format(date);
        }
    }


    public static String formatTimeToDay(String beforeTime, String fromFormatRegulation) {
        return formatTime(beforeTime, fromFormatRegulation, "yyyy-MM-dd");
    }


    /**
     * 字符串转Date方法
     *
     * @param dateStr
     * @param format  如yyyy-MM-dd HH:mm:ss等
     * @return
     * @throws Exception
     */
    public static Date stringToDate(String dateStr, String format) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * Date转字符串方法
     *
     * @param date
     * @param format 如yyyy-MM-dd HH:mm:ss等
     * @return
     * @throws Exception
     */
    public static String dateToString(Date date, String format) {
        String dateStr = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            dateStr = sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateStr;
    }

    /**
     * 时间戳转day
     *
     * @param longTime
     * @return
     */
    public static String longTimeToDay(String longTime) {
        try {
            long time = JsonBeanUtil.getLong(longTime, 0);
            Date date = new Date(time);
            String dayStr = TimeUtil.dateToString(date, "yyyy-MM-dd");
            return dayStr;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}
