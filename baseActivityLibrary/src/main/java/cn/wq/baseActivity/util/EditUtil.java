package cn.wq.baseActivity.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

/**
 * Created by Administrator on 2016/10/20.
 */
public class EditUtil {

    /**
     * 强制隐藏输入法键盘
     */
    public static void hideInput(Context context, View view) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 强制弹出输入法键盘
     */
    public static void openInput(Activity activity, View view) {
//        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * EditText获取焦点并显示软键盘
     */
    public static void showSoftInputFromWindow(Activity activity, EditText editText) {
//        editText.setFocusable(true);
//        editText.setFocusableInTouchMode(true);
//        editText.requestFocus();


        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);


        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        editText.requestFocusFromTouch();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);


//        openInput(activity, editText);
    }

    /**
     * 设置电话号码
     *
     * @param s
     * @param start
     * @param before
     */
    public static void setPhone(EditText editText, CharSequence s, int start, int before) {
        if (s == null || s.length() == 0) return;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (i != 3 && i != 8 && s.charAt(i) == ' ') {
                continue;
            } else {
                sb.append(s.charAt(i));
                if ((sb.length() == 4 || sb.length() == 9) && sb.charAt(sb.length() - 1) != ' ') {
                    sb.insert(sb.length() - 1, ' ');
                }
            }
        }
        if (!sb.toString().equals(s.toString())) {
            int index = start + 1;
            try {
                if (sb.length() > start && sb.charAt(start) == ' ') {
                    if (before == 0) {
                        index++;
                    } else {
                        index--;
                    }
                } else {
                    if (before == 1) {
                        index--;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            editText.setText(sb.toString());
            try {
                editText.setSelection(index);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setMaxLength(EditText editText, int maxLeng) {
        InputFilter[] filters = {new InputFilter.LengthFilter(maxLeng)};
        editText.setFilters(filters);
    }


    public static void updateHeightScrollView(Activity activity, final ScrollView scrollView) {
        final View decorView = activity.getWindow().getDecorView();
        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                decorView.getWindowVisibleDisplayFrame(rect);
                int screenHeight = decorView.getRootView().getHeight();
                int heightDifference = screenHeight - rect.bottom;//计算软键盘占有的高度  = 屏幕高度 - 视图可见高度
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) scrollView.getLayoutParams();
                layoutParams.setMargins(0, 0, 0, heightDifference);//设置ScrollView的marginBottom的值为软键盘占有的高度即可
                scrollView.requestLayout();
            }
        });
    }

    /**
     * 返回正常布局
     * @param root
     */
    private static void controlKeyboardLayout(final View root) {
        root.scrollTo(0, 0);
    }

    /**
     * @param root         最外层布局，需要调整的布局
     * @param scrollToView 被键盘遮挡的scrollToView，滚动root，使scrollToView在root可视区域的底部
     */
    public static void controlKeyboardLayout(final View root, final View scrollToView) {
        if (scrollToView == null) {
            controlKeyboardLayout(root);
            return;
        }
        Rect rect = new Rect();
        //获取root在窗体的可视区域
        root.getWindowVisibleDisplayFrame(rect);
        //获取root在窗体的不可视区域高度(被其他View遮挡的区域高度)
        int rootInvisibleHeight = root.getRootView().getHeight() - rect.bottom;
        //若不可视区域高度大于100，则键盘显示
        if (rootInvisibleHeight > 100) {
            int[] location = new int[2];
            //获取scrollToView在窗体的坐标
            scrollToView.getLocationInWindow(location);
            //计算root滚动高度，使scrollToView在可见区域
            int srollHeight = (location[1] + scrollToView.getHeight()) - rect.bottom;
            root.scrollTo(0, srollHeight);
        } else {
            //键盘隐藏
            root.scrollTo(0, 0);
        }


//        root.getViewTreeObserver().addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                Rect rect = new Rect();
//                //获取root在窗体的可视区域
//                root.getWindowVisibleDisplayFrame(rect);
//                //获取root在窗体的不可视区域高度(被其他View遮挡的区域高度)
//                int rootInvisibleHeight = root.getRootView().getHeight() - rect.bottom;
//                //若不可视区域高度大于100，则键盘显示
//                if (rootInvisibleHeight > 100) {
//                    int[] location = new int[2];
//                    //获取scrollToView在窗体的坐标
//                    scrollToView.getLocationInWindow(location);
//                    //计算root滚动高度，使scrollToView在可见区域
//                    int srollHeight = (location[1] + scrollToView.getHeight()) - rect.bottom;
//                    root.scrollTo(0, srollHeight);
//                } else {
//                    //键盘隐藏
//                    root.scrollTo(0, 0);
//                }
//            }
//        });
    }

}
