package cn.wq.baseActivity.util;

/**
 * Created by Administrator on 2016/11/24.
 */

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

/**
 * Created by win7 on 2016/9/9.
 * View 处理
 */
public class AwuViewTreeUtil {
    // For more information, see https://code.google.com/p/android/issues/detail?id=5497
    // To use this class, simply invoke setInstantOnLayout() on an Activity that already has its content view set.
//    private static boolean isKeyBordVisiable;

    /**
     * 设置 即时onlayout
     * @param content
     * @param statusHeight
     */
    public static void setInstantOnLayout(View content, int statusHeight) {
        new AwuViewTreeUtil(content, statusHeight);
    }

    private View mChildOfContent;
    private int usableHeightPrevious;
    private ViewGroup.LayoutParams frameLayoutParams;
    private int statusHeight;

    private AwuViewTreeUtil(View content, int statusHeight) {
        this.statusHeight = statusHeight;
        mChildOfContent = content;
        mChildOfContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                possiblyResizeChildOfContent();
            }
        });
        frameLayoutParams = mChildOfContent.getLayoutParams();
    }
    private void possiblyResizeChildOfContent() {
        int usableHeightNow = computeUsableHeight();
//        if (usableHeightNow != usableHeightPrevious) {
            //如果两次高度不一致
//            int usableHeightSansKeyboard = mChildOfContent.getRootView().getHeight();
//            int heightDifference = usableHeightSansKeyboard - usableHeightNow;
//            if (heightDifference > (usableHeightSansKeyboard / 4)) {
//                // keyboard probably just became visible
//                isKeyBordVisiable=true;
//            } else {
//                // keyboard probably just became hidden
//                isKeyBordVisiable=false;
//            }
            //将计算的可视高度设置成视图的高度
            frameLayoutParams.height = usableHeightNow;
            mChildOfContent.requestLayout();//请求重新布局
            usableHeightPrevious = usableHeightNow;
//        }
    }

    private int computeUsableHeight() {
        //计算视图可视高度
        Rect r = new Rect();
        mChildOfContent.getWindowVisibleDisplayFrame(r);
        return (r.bottom - r.top) + statusHeight;
    }
}