package cn.wq.baseActivity.util.file;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.MediaColumns;
import android.text.TextUtils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;


public final class Files {


    private static final String PATH = "/wq_base_file";


    private static final String AUDIODIR = "/audio";
    private static final String VIDEODIR = "/video";
    private static final String FACESDIR = "/faces";
    private static final String PHOTODIR = "/photo";
    private static final String APKDIR = "/apk";
    private static final String TEMPDIR = "/tmp";
    private static final String APPDIR = "/app";
    private static final String DYNAMICDIR = "/dynamic";

    public static String getFaceFileByMd5(final String UserIDMd5) {
        return getBase() + FACESDIR + "/" + UserIDMd5 /* + ".png" */;
    }


    public static String getVedioDir() {
        String path = getBase() + VIDEODIR + "/";
        checkFileExists(path);
        return path;
    }

    public static String getVedioDir(String path) {
        String videoPath;
        if (path.startsWith("/")) {
            videoPath = getBase() + VIDEODIR + path;
        } else {
            videoPath = getBase() + VIDEODIR + "/" + path;
        }
        checkFileExists(videoPath);
        return videoPath;
    }


    public static void checkFileExists(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static String getFaceDir() {
        return getBase() + FACESDIR + "/";
    }


    public static String getDynamcDir() {
        return getBase() + DYNAMICDIR + "/";
    }


    public static byte[] getFileBytes(final String filePath) {
        byte[] buffer = null;
        try {
            final File file = new File(filePath);
            final FileInputStream fis = new FileInputStream(file);
            final ByteArrayOutputStream bos = new ByteArrayOutputStream((int) file.length());
            final byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (final FileNotFoundException e) {
            e.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }


    // 创建一个新的音频文件名称
    public static String getNewAudioFileName() {
        return getBase() + AUDIODIR + "/" + java.util.UUID.randomUUID();
    }


    public static String getNewAudioFileName(final String audioFileName) {
        return getBase() + AUDIODIR + "/" + audioFileName;
    }


    /*
     * 传入一个文件名,将其转为本地音频文件
     */
    public static String getLocalAudioFileName(final String filePath) {
        return getBase() + AUDIODIR + "/" + Files.getFileNameWithoutPath(filePath);
    }


    public static Uri getNewPhotoFileName() {
        final String filePath = getBase() + PHOTODIR + "/" + java.util.UUID.randomUUID();

        return Uri.fromFile(new File(filePath));
    }


    public static File getNewPhotoFile() {
        final String filePath = getBase() + PHOTODIR + "/" + java.util.UUID.randomUUID();

        return new File(filePath);
    }


    /**
     * 根据传入的文件名得到新的临时文件名
     *
     * @param fileName
     * @return
     */
    public static String getNewTempFileName(final String fileName) {
        return getBase() + TEMPDIR + "/" + fileName;
    }


    public static boolean renameTo(String oldFilePath, String newFilePath) {
        boolean success = new File(oldFilePath).renameTo(new File(newFilePath));
        return success;
    }


    /**
     * 随机生成一个临时文件名
     *
     * @return
     */
    public static String getNewTempFileName() {
        String tempPath = getTempPath();

        checkFileExists(tempPath);
        String fileName = tempPath + java.util.UUID.randomUUID();
        return fileName;
    }


    public static final boolean hasSdCard() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }


    public static final boolean init() {
        // PATH_SD =
        // Android.OS.Environment.GetExternalStoragePublicDirectory().AbsoluteFile;

        boolean success = false;
        success = isFolderExists(getBase());
        if (!success) {
            return success;
        }

        final String[] dirs = new String[]{FACESDIR, AUDIODIR, PHOTODIR, APKDIR, APPDIR, TEMPDIR, DYNAMICDIR};

        for (final String dir : dirs) {
            success = isFolderExists(getBase() + dir);
            if (!success) {
                return success;
            }
        }
        return success;

    }


    public static final String getBase() {
        return Files.getSdcardPath() + PATH;
    }

    public static String getTempPath() {
        String temp = getBase() + TEMPDIR + "/";
        checkFileExists(temp);
        return temp;
    }

    public static final String getSdcardPath() {
//        String downloadCachePath = Environment.getDownloadCacheDirectory().getPath();
        String dataPath = Environment.getDataDirectory().getPath();
//        String rootPath = Environment.getRootDirectory().getPath();
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED); //判断sd卡是否存在
        if (sdCardExist) {
            return Environment.getExternalStorageDirectory().getPath();
        } else {
            return dataPath;
        }
    }


    public static final boolean isFileExists(final String strFolder) {
        if (TextUtils.isEmpty(strFolder)) return false;
        final File file = new File(strFolder);

        return file.exists();
    }


    public static final boolean isFolderExists(final String strFolder) {
        if (TextUtils.isEmpty(strFolder)) return false;
        final File file = new File(strFolder);

        if (!file.exists()) {
            if (file.mkdir()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }


    public static String saveFaceFile(final String userID, final byte[] bitmap) throws Exception {
        final String fileName = getBase() + FACESDIR + "/" + userID /* + ".png" */;

        Files.saveFile(fileName, bitmap);

        return fileName;

    }


    public static void saveFile(final String filePath, final byte[] fileContent) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;

        try {

            file = new File(filePath);

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(fileContent);

        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (final IOException e1) {
                    e1.printStackTrace();
                }
            }

            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e1) {
                    e1.printStackTrace();
                }

            }
        }

    }


    public static String getApkFilePath(final int vercode) {
        return getBase() + APKDIR + "/awu_" + vercode + ".apk";
    }


    public static String getApkFileTempPath(final int vercode) {
        return getBase() + APKDIR + "/awu_" + vercode + ".temp";
    }


    public static String getApkFilePath(String apkFileName) {
        return getBase() + APKDIR + "/awu_" + apkFileName + ".apk";
    }


    public static String getFileNameWithoutPath(final String filePath) {
        return new File(filePath).getName();
    }


    public static String parseImageUri(final Uri uri, final Context context) {

        final String[] proj = {MediaColumns.DATA};

        final Cursor actualimagecursor = context.getContentResolver().query(uri, proj, null, null, null);

        final int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaColumns.DATA);

        actualimagecursor.moveToFirst();

        final String img_path = actualimagecursor.getString(actual_image_column_index);

        actualimagecursor.close();

        return img_path;
    }


//    /**
//     * 解压缩一个文件
//     *
//     * @param zipFile    压缩文件
//     * @param folderPath 解压缩的目标目录
//     * @throws IOException 当解压缩过程出错时抛出
//     */
//    public static void upZipFile(final File zipFile, final String folderPath) throws ZipException, IOException {
//
//        final File desDir = new File(folderPath);
//        if (!desDir.exists()) {
//            desDir.mkdirs();
//        }
//        final ZipFile zf = new ZipFile(zipFile);
//        for (final Enumeration<?> entries = zf.entries(); entries.hasMoreElements(); ) {
//            final ZipEntry entry = ((ZipEntry) entries.nextElement());
//            final InputStream in = zf.getInputStream(entry);
//            String str = folderPath + File.separator + entry.getName();
//            str = new String(str.getBytes("8859_1"), "GB2312");
//            final File desFile = new File(str);
//            if (!desFile.exists()) {
//                final File fileParentDir = desFile.getParentFile();
//                if (!fileParentDir.exists()) {
//                    fileParentDir.mkdirs();
//                }
//                desFile.createNewFile();
//            }
//
//            final OutputStream out = new FileOutputStream(desFile);
//            final byte buffer[] = new byte[1204];
//            int realLength;
//            while ((realLength = in.read(buffer)) > 0) {
//                out.write(buffer, 0, realLength);
//            }
//            in.close();
//            out.close();
//        }
//    }


    public static Exception deleteFile(final String fileName) {

        try {
            final File f = new File(fileName);
            f.delete();

        } catch (final Exception e) {
            return e;
        }

        return null;
    }


    public static void deleteCacheFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles == null || listFiles.length == 0) {
                    file.delete();
                    return;
                }
                for (File f : listFiles) {
                    deleteCacheFile(f.getPath());
                }
                file.delete();
            } else {
                file.delete();
            }
        } else {
            file.delete();
        }
    }


    public static void appendToFile(String filePath, String msg) throws IOException {
        FileWriter writer = new FileWriter(filePath, true);
        PrintWriter printer = new PrintWriter(writer);
        printer.append(msg);

        printer.close();
        writer.close();
    }


    public static String getAppPath() {
        return getBase() + APPDIR + "/";
    }


    /**
     * 文件拷贝
     *
     * @param ctx
     * @param db_path 数据路径
     * @param db_name 数据名称
     *                从assets中拷�?
     */
    public static void copyDatabase(Context ctx, String db_path, String db_name) {
        String path = db_path + "/" + db_name;
        if (new File(db_path).exists() == false) {
            checkFileExists(db_path);
        }
        InputStream is = null;
        FileOutputStream os = null;
        try {
            is = ctx.getAssets().open(db_name);
            os = new FileOutputStream(path);
            byte[] buffer = new byte[1024];
            int length = 0;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
//			os.close();
//			is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

//
//	public static ArrayList<FileBean> getFileBeans(String path) {
//		if(path!=null && !path.endsWith("/")){
//			path=path+"/";
//		}
//		File file = new File(path);
//		ArrayList<FileBean> fileBeans=new ArrayList<FileBean>();
//		if (!file.exists()) {
////			Toast.makeText(This, file+"路径不存在", duration)
//			System.out.println(path + " not exists");
//
////			return;
//		}else{
//			File[] childs =file.listFiles();
//
//			for (File child : childs) {
//				FileBean fileBean=new FileBean();
//				fileBean.setName(child.getName());
//				fileBean.setPath(path + child.getName());
//				fileBean.setSize(child.length());
//				if(child.isDirectory()){
//					fileBean.setFileType(FileType.FOLDER);
//				}else{
//					fileBean.setFileType(FileType.VIDEO);
//				}
//				fileBeans.add(fileBean);
//			}
//		}
//		return fileBeans;
//	}
//	public static ArrayList<FileBean> getFileBeans(Context context,String path) {
//		if(path!=null && !path.endsWith("/")){
//			path=path+"/";
//		}
//		File file = new File(path);
//		ArrayList<FileBean> fileBeans=new ArrayList<FileBean>();
//		if (!file.exists()) {
////			Toast.makeText(This, file+"路径不存在", duration)
//			System.out.println(path + " not exists");
//
////			return;
//		}else{
//			File[] childs =file.listFiles();
//
//			for (File child : childs) {
//				FileBean fileBean=new FileBean();
//				fileBean.setName(child.getName());
//				fileBean.setPath(path + child.getName());
//				fileBean.setSize(child.length());
//				if(child.isDirectory()){
//					fileBean.setFileType(FileType.FOLDER);
//				}else{
//					fileBean.setFileType(FileType.VIDEO);
//				}
//				fileBeans.add(fileBean);
//			}
//		}
//		return fileBeans;
//	}

}
