package cn.wq.baseActivity.base;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.lidroid.xutils.util.LogUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import cn.wq.baseActivity.BaseApplication;
import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.interfaces.IAppOnForegroundListener;
import cn.wq.baseActivity.base.interfaces.OnKeyboardListener;
import cn.wq.baseActivity.base.ui.toolbar.BaseToolbarActivity;
import cn.wq.baseActivity.util.ActivityUtil;
import cn.wq.baseActivity.util.AppUtil;
import cn.wq.baseActivity.util.AwuViewTreeUtil;
import cn.wq.baseActivity.util.EditUtil;
import cn.wq.baseActivity.util.SoftKeyBroadManager;
import cn.wq.baseActivity.util.StatusUtil;

/**
 * Created by W~Q on 2017/3/16.
 * SW activity通用基类
 */
public abstract class BaseActivity<T extends BaseViewDelegate> extends BaseToolbarActivity<T, BaseFragment> implements IAppOnForegroundListener, View.OnLayoutChangeListener
        , OnKeyboardListener, SoftKeyBroadManager.SoftKeyboardStateListener {
    public BaseActivity This;//当前对象,当前类便捷实用

    //    protected boolean isNeed5497() {
//        return false;
//    }
    SoftKeyBroadManager mManager;
    private boolean isShowKeyboard;

    public boolean isShowKeyboard() {
        return isShowKeyboard;
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        ButterKnife.bind(this);
        ButterKnife.bind(getViewDelegate(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        This = this;
        //设置允许通过ActivityOptions.makeSceneTransitionAnimation发送或者接收Bundle
        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
//        //设置使用TransitionManager进行动画，不设置的话系统会使用一个默认的TransitionManager
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);

//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setFullScreen();
        ActivityUtil.addActivity(This);
//        if (isNeed5497()) {
//            setBottomBar();
//            AndroidBug5497Workaround.assistActivity(this);
//        }

        getViewDelegate().getRootView().setKeepScreenOn(true);
        setStatusFocusable();
        //        getViewDelegate().getRootView().addOnLayoutChangeListener(this);
        mManager = new SoftKeyBroadManager(getView(R.id.base_layout));
        //添加软键盘的监听，然后和上面一样的操作即可.
        mManager.addSoftKeyboardStateListener(this);
        //注意销毁时，得移除监听
//        mManager.removeSoftKeyboardStateListener(this);
        if (isNeedEventbusNotify()) {
            //注册成为订阅者
            try {
                EventBus.getDefault().register(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 是否需要Eventbus通知
     *
     * @return
     */
    public boolean isNeedEventbusNotify() {
        return false;
    }

    @Override
    protected void onDestroy() {
        if (isNeedEventbusNotify()) {
            //注册成为订阅者
            try {
                EventBus.getDefault().register(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
        ActivityUtil.removeActivity(This);
        if (mManager != null) {
            mManager.removeSoftKeyboardStateListener(this);
        }
    }

    public boolean isFullScreen() {
        return false;
    }

    private void setFullScreen() {
        if (isFullScreen()) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    /**
     * 是否系统形式关闭键盘，
     * 否则点击任何区域关闭
     *
     * @return
     */
    public boolean isSysColseKeyboard() {
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (isSysColseKeyboard()) {
            return super.dispatchTouchEvent(ev);
        }
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v == null) {
                return super.dispatchTouchEvent(ev);
            }
            if (isShouldHideInput(v, ev)) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
//                getWindow().getDecorView().setFocusable(true);
//                getWindow().getDecorView().setFocusableInTouchMode(true);
//                getWindow().getDecorView().requestFocus();
//                getWindow().getDecorView().requestFocusFromTouch();

                setStatusFocusable();
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isWebActivity() {
        return false;
    }

    private void setStatusFocusable() {
        if (getView(R.id.base_status_view) != null) {
            getView(R.id.base_status_view).setFocusable(true);
            getView(R.id.base_status_view).setFocusableInTouchMode(true);
            getView(R.id.base_status_view).requestFocus();
            getView(R.id.base_status_view).requestFocusFromTouch();
        }
    }

    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public View getView(int rId) {
        return getViewDelegate().get(rId);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (onBackKeyDown()) {
                return true;
            } else {
                return super.onKeyDown(keyCode, event);
            }
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * 设置有动态计算布局高度 ，主要针对华为手机底部虚拟按键
     */
    protected void setBottomBar() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//
//        }
        if (isNeedChangeLayoutSuit()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && isSetStatusBarInside()) {
                AwuViewTreeUtil.setInstantOnLayout(findViewById(R.id.base_layout), StatusUtil.getStatusHeight(This));
//            AppUtil.getStatusHeight(this)
            } else {
                AwuViewTreeUtil.setInstantOnLayout(findViewById(R.id.base_layout), 0);
            }
        }

    }

    /**
     * 适应动态布局改变
     * 1.查看图片，不需要动态改变
     *
     * @return
     */
    protected boolean isNeedChangeLayoutSuit() {
        return true;
    }

    /**
     * 这个方法特殊，  主要处理沉浸在状态栏 和输入法冲突的情况
     *
     * @return
     */
    protected boolean isSetStatusBarInside() {
        return true;
    }

    /**
     * 重写按下返回键方法
     *
     * @return
     */
    public boolean onBackKeyDown() {
        return false;
    }


    @Override
    public T getViewDelegate() {
        return super.getViewDelegate();
    }


    public boolean isDestroyedSw() {
        if (this == null || this.isFinishing()) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return isDestroyed();
        } else {
            return isFinishing();
        }
    }

    @Override
    public void onResume() {


        if (!TextUtils.isEmpty(statisticsPageName())) {

            MobclickAgent.onPageStart(statisticsPageName()); //统计页面(仅有Activity的应用中SDK自动调用，不需要单独写。"SplashScreen"为页面名称，可自定义)
//        MobclickAgent.onResume(this);          //统计时长

        }
        LogUtils.i("wq 0619 statisticsPageName()统计页面:" + statisticsPageName());
        super.onResume();
    }

    @Override
    public void appOnForeground() {
        LogUtils.i("wq 0823 当前activity监听到app退出到后台：" + getClass().getSimpleName());
    }

    @Override
    public void onPause() {

        if (!TextUtils.isEmpty(statisticsPageName())) {

            MobclickAgent.onPageEnd(statisticsPageName()); // （仅有Activity的应用中SDK自动调用，不需要单独写）保证 onPageEnd 在onPause 之前调用,因为 onPause 中会保存信息。"SplashScreen"为页面名称，可自定义
        }
        LogUtils.i("wq 0619 statisticsPageName()统计页面:" + statisticsPageName());
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isDestroyedSw()) {
            if (AppUtil.isBackground(this)) {
                appOnForeground();
                ((BaseApplication) getApplication()).appOnForeground();
            }
        }
    }

    /**
     * 子类集成当前方法进行页面ID统计
     *
     * @return
     */
    public String statisticsPageName() {
        return getClass().getSimpleName();
    }


    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        //获取屏幕高度
        int screenHeight = this.getWindowManager().getDefaultDisplay().getHeight();
        //阀值设置为屏幕高度的1/3
        int keyHeight = screenHeight / 3;
        //old是改变前的左上右下坐标点值，没有old的是改变后的左上右下坐标点值
        //现在认为只要控件将Activity向上推的高度超过了1/3屏幕高，就认为软键盘弹起
        if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom > keyHeight)) {
            onShowKeyboard();
        } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom > keyHeight)) {
            onCloseKeyboard();
        }
    }


    @Override
    public void onShowKeyboard() {
        LogUtils.i("wq 0409 键盘显示");

    }

    @Override
    public void onCloseKeyboard() {
        LogUtils.i("wq 0409 键盘关闭");

    }

    @Override
    public void onSoftKeyboardOpened(int keyboardHeightInPx) {
        isShowKeyboard = true;
        updateKeyboardLayout();
        onShowKeyboard();
    }

    @Override
    public void onSoftKeyboardClosed() {
        isShowKeyboard = false;
        EditUtil.controlKeyboardLayout(getView(R.id.base_layout), null);
        onCloseKeyboard();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            boolean b = super.dispatchKeyEvent(event);
            updateKeyboardLayout();
            return b;
        }
        return super.dispatchKeyEvent(event);
    }


    /**
     * 键盘弹起布局
     */
    public void updateKeyboardLayout() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(This).onActivityResult(requestCode, resultCode, data);
    }
}
