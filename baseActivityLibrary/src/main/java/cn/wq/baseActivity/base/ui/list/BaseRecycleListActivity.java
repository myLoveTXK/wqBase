package cn.wq.baseActivity.base.ui.list;

import android.os.Bundle;
import android.view.View;

import com.andview.refreshview.XRefreshView;

import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.util.ClickUtil;

/**
 * Created by W~Q on 2017/3/21.
 * shi
 */

public abstract class BaseRecycleListActivity<T extends BaseRecycleListViewDelegate> extends BaseActivity<T> {

    public void setNeedRefresh(boolean isNeedRefresh) {
        getViewDelegate().commonRefresh.setPullRefreshEnable(isNeedRefresh);

    }

    public void setNeedLoadMore(boolean isNeedLoadMore) {
        getViewDelegate().commonRefresh.setPullLoadEnable(isNeedLoadMore);

    }


    public int getAdapterItemType(int position) {
        return 0;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getViewDelegate().getRecycler().setOnRefreshListener(this);
        initRefreshListener();


    }

    private void initRefreshListener() {
        getViewDelegate().commonRefresh.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh(boolean isPullDown) {
                if (isDestroyedSw()) {
                    return;
                }
                getViewDelegate().setShowEmpty(false);
                currentOnRefresh(isPullDown);
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                if (isDestroyedSw()) {
                    return;
                }
                getViewDelegate().setShowEmpty(false);
                currentOnLoadMore(isSilence);
            }
        });
        getViewDelegate().emptyViewHolder.emptyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClickUtil.isFastDoubleClick()) {
                    return;
                }
                if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
                    return;
                }
                getViewDelegate().commonRefresh.post(new Runnable() {
                    @Override
                    public void run() {
                        getViewDelegate().setShowEmpty(false);
                        startRefresh();
                    }
                });

            }
        });

    }

    public void setLoadMoreComplete(boolean isComplete) {
        getViewDelegate().commonRefresh.setLoadComplete(isComplete);
    }

    public void startRefresh() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        setLoadMoreComplete(true);
        getViewDelegate().commonRefresh.startRefresh();
    }

    public void stopRefresh() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        getViewDelegate().commonRefresh.stopRefresh();
    }

    public void stopLoadMore() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        getViewDelegate().commonRefresh.stopLoadMore();
    }

    protected void currentOnRefresh(boolean isPullDown) {
        onRefresh(isPullDown);
    }

    private void currentOnLoadMore(boolean isSilence) {
        onLoadMore(isSilence);
    }

    public abstract void onRefresh(boolean isPullDown);

    public abstract void onLoadMore(boolean isSilence);


}