package cn.wq.baseActivity.base.broadcast;

import android.content.Intent;

import java.util.List;

import cn.wq.baseActivity.base.interfaces.IBroadcastListener;
import cn.wq.baseActivity.base.interfaces.IRegisterFragment;
import cn.igo.themvp.presenter.FragmentPresenter;
import cn.igo.themvp.view.IDelegate;

/**
 * Created by W~Q on 2017/3/16.
 * Fragment 基类
 * 1.实现了基类默认接受广播，回调onReceiveBroadcast函数
 */
public abstract class BaseBroadcastFragment<T extends IDelegate,B extends BaseBroadcastFragment> extends FragmentPresenter<T> implements IBroadcastListener, IRegisterFragment<B> {

    ManagerRegisterFragment managerRegisterFragment = new ManagerRegisterFragment();

    public void baseReceiveBroadcast(String broadcast, Intent intent) {
        onReceiveBroadcast(broadcast, intent);
        onReceiveBroadcastFragmentChilds(broadcast, intent);

    }

    @Override
    public void onReceiveBroadcast(String broadcast, Intent intent) {

    }

    @Override
    public void onReceiveBroadcastFragmentChilds(String broadcastMark, Intent intent) {
        if (getResisterFragment() != null) {
            for (BaseBroadcastFragment fragment : getResisterFragment()) {
                fragment.baseReceiveBroadcast(broadcastMark, intent);
            }
        }
    }

    @Override
    public void resisterFragment(List<B> registerFragment) {
        if (managerRegisterFragment != null) {
            managerRegisterFragment.resisterFragment(registerFragment);
        }
    }

    @Override
    public List<B> getResisterFragment() {
        if (managerRegisterFragment == null) {
            managerRegisterFragment = new ManagerRegisterFragment();
        }
        return managerRegisterFragment.getResisterFragment();
    }
}
