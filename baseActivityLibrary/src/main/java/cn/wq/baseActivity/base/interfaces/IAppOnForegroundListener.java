package cn.wq.baseActivity.base.interfaces;

/**
 * Created by xt on 2017/8/23.
 * app退到后台监听
 */

public interface IAppOnForegroundListener {

    void appOnForeground();

}
