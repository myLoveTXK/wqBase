package cn.wq.baseActivity.base.interfaces;

/**
 * Created by xt on 2019/9/3.
 * 权限申请监听
 */

public interface IPermissionListener {

    void onPermissionGranted(String permission);//授权

    void onPermissionDenied(String permission);//拒绝
}
