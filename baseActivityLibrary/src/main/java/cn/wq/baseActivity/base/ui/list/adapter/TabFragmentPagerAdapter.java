package cn.wq.baseActivity.base.ui.list.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.wq.baseActivity.base.BaseFragment;
import cn.wq.baseActivity.base.ui.interfaces.ITabFragment;


/**
 * Created by Administrator on 2016/6/8.
 */
public class TabFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<BaseFragment> fragments = new ArrayList<>();

    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public List<BaseFragment> getFragments() {
        return fragments;
    }

    public void addFragments(List<? extends BaseFragment> fragments) {
        this.fragments.addAll(fragments);
        notifyDataSetChanged();
    }

    /**
     * 这个方法非常有用，默认返回1.0f，权重比例
     */
//    @Override
//    public float getPageWidth(int position) {
//        return (float) 0.9;
//    }
    @Override
    public BaseFragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public String getPageTitle(int position) {
        if (getItem(position) instanceof ITabFragment) {
            return ((ITabFragment) getItem(position)).getTabTitle();
        }
        return "标题";

//        return "wq";
    }
}
