package cn.wq.baseActivity.base.interfaces;

import android.content.Intent;

/**
 * Created by W~Q on 2017/3/16.
 *
 */
public interface IActivityCallback {
    
    void callback(int resultCode, Intent data);

}
