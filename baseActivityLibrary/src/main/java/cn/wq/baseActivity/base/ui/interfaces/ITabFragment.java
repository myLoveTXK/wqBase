package cn.wq.baseActivity.base.ui.interfaces;

/**
 * Created by xt on 2019/12/12.
 */

public interface ITabFragment {
    String getTabTitle();
}
