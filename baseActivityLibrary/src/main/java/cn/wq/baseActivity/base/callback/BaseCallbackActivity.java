package cn.wq.baseActivity.base.callback;

import android.content.Intent;
import android.os.Build;

import java.util.HashMap;
import java.util.Map;

import cn.igo.themvp.view.IDelegate;
import cn.wq.baseActivity.base.broadcast.BaseBroadcastActivity;
import cn.wq.baseActivity.base.broadcast.BaseBroadcastFragment;
import cn.wq.baseActivity.base.interfaces.IActivityCallback;

/**
 * Created by xt on 2017/3/16.
 * 封装activity跳转回调基类
 * 1.封装startActivityForResult
 */
public abstract class BaseCallbackActivity<T extends IDelegate, B extends BaseBroadcastFragment> extends BaseBroadcastActivity<T, B> {
    //可能存在（startActivityForResult)方法多次调用，设计出循环map形式添加和删除，
    private Map<Integer, IActivityCallback> activityCallbackMap;//callback保存对象
    private final int loopCount = 20;//循环次数
    private int recodeCount = 0;//当前记录的值
//    private BaseCallbackActivity instance;


    public synchronized void startActivity(Class<?> clas) {
        Intent intent = new Intent(this, clas);
        startActivity(intent);
    }

    public synchronized void startActivity(Class<?> clas, Intent intent) {
        if (intent == null) {
            intent = new Intent(this, clas);
        }
        intent.setClass(this, clas);
        startActivity(intent);
    }

    public synchronized void startActivityForResult(Class<?> clas, IActivityCallback activityCallback) {
        startActivityForResult(clas, null, activityCallback);
    }

    /**
     * 启用回调函数接受页面返回值
     *
     * @param clas           指定目标
     * @param intent           传参数用
     * @param activityCallback 回调监听
     */
    public synchronized void startActivityForResult(Class<?> clas, Intent intent, IActivityCallback activityCallback) {
        if (intent == null) {
            if (clas == null) {
                intent = new Intent();
            } else {
                intent = new Intent(this, clas);
            }

        } else {
            if (clas != null) {
                intent.setClass(this, clas);
            }
        }
        startActivityForResult(intent, activityCallback);
    }

    /**
     * 启用回调函数接受页面返回值
     *
     * @param intent           传参数用
     * @param activityCallback 回调监听
     */
    public synchronized void startActivityForResult(Intent intent, IActivityCallback activityCallback) {
        if (intent == null) {
            return;
        }
        if (isFinishing()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {//兼容版本处理
            if (isDestroyed()) {
                return;
            }
        }
        recodeCount = recodeCount % loopCount + 1;
        getActivityCallbackMap().put(recodeCount, activityCallback);
        startActivityForResult(intent, recodeCount);
    }

    /**
     * 返回当前存储的Callback map
     *
     * @return
     */
    private Map<Integer, IActivityCallback> getActivityCallbackMap() {
        if (activityCallbackMap == null) {
            synchronized (this) {
                if (activityCallbackMap == null) {
                    activityCallbackMap = new HashMap<>();
                }
            }
        }
        return activityCallbackMap;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IActivityCallback activityCallback = getActivityCallbackMap().get(requestCode);
        if (activityCallback != null) {
            activityCallback.callback(resultCode, data);
            if (getActivityCallbackMap().containsKey(requestCode)) {
                getActivityCallbackMap().remove(requestCode);
            }
        }
    }

}
