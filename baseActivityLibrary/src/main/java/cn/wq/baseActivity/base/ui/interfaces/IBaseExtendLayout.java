package cn.wq.baseActivity.base.ui.interfaces;

import android.view.ViewGroup;

/**
 * Created by xt on 2018/5/11.
 */

public interface IBaseExtendLayout {
    ViewGroup getBaseExtendContentLayout();

}
