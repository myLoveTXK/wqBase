package cn.wq.baseActivity.base.ui.toolbar;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.igo.themvp.view.AppDelegate;
import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.view.image.util.DisplayUtil;


/**
 * Created by W~Q on 2017/3/20.
 * 默认设置左右按钮，中间title
 */
public abstract class BaseToolbarDelegate extends AppDelegate implements IUiInterface.BaseToolbarInterface {
    private boolean isUseBaseToolbar;//使用默认的头部
    private int otherToolbarLayoutRes;

    @Override
    public void setToolbarDesignHeight(int designHeight) {
        getToolbar().getLayoutParams().height = DisplayUtil.getScreenRealLength(designHeight);
    }

    /**
     * 内容是否相对于toolbar之下（相对布局上下）
     * 返回0 则 全屏
     * 返回1 则 在状态栏之下
     * 返回2 则 在toolbar之下
     */
    public abstract int getContentBaseRelativeLayout();

    @Override
    public void create(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(getBaseRootLayoutID(), container, false);
        ViewGroup contentLayout = (ViewGroup) rootView.findViewById(R.id.sw_content);
        if (getLayoutID() != 0) {
            View contentView = inflater.inflate(getLayoutID(), container, false);
            contentLayout.addView(contentView);
        }
        initData();
    }

    protected int getBaseRootLayoutID() {
        return R.layout.activity_base;
    }

    @Override
    public void initWidget() {
        super.initWidget();

        updateContentBaseRelativeLayout();
        get(R.id.base_layout).invalidate();
        getToolbarTitle().setText("");
        getToolbarChangeTitle().setText("");

        ViewGroup.LayoutParams layoutParams = getToolbarTopLayout().getLayoutParams();
        layoutParams.height = 0;
        getToolbarTopLayout().setLayoutParams(layoutParams);
    }

    public void updateContentBaseRelativeLayout() {
        ViewGroup contentLayout = (ViewGroup) rootView.findViewById(R.id.sw_content);
        if (getContentBaseRelativeLayout() == 0) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) contentLayout.getLayoutParams();
            layoutParams.addRule(RelativeLayout.BELOW, 0);
            contentLayout.setLayoutParams(layoutParams);
        } else if (getContentBaseRelativeLayout() == 1) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) contentLayout.getLayoutParams();
            layoutParams.addRule(RelativeLayout.BELOW, R.id.base_status_bar_layout);
            contentLayout.setLayoutParams(layoutParams);
        } else if (getContentBaseRelativeLayout() == 2) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) contentLayout.getLayoutParams();
            layoutParams.addRule(RelativeLayout.BELOW, R.id.toolbar);
            contentLayout.setLayoutParams(layoutParams);
        }
    }

    @Override
    public View getToolDividerView() {
        return get(R.id.base_toolbar_divider);
    }

    @Override
    public Toolbar getToolbar() {
        return get(R.id.toolbar);
    }


    public ViewGroup getToolbarTopLayout() {
        return getViewGroup(R.id.toolbarTopLayout);
    }

    @Override
    public void setToolbarBackgroundColor(int color) {
        get(R.id.toolbar).setBackgroundColor(color);
        if (color == Color.TRANSPARENT) {
            get(R.id.base_layout).setBackgroundColor(color);
        }
    }

    @Override
    public void setToolbarBackgroundColorRes(int colorRes) {
        get(R.id.toolbar).setBackgroundColor(getActivity().getResources().getColor(colorRes));
    }

    @Override
    public void setToolbarBackgroundImgRes(int imgRes) {
        get(R.id.toolbar).setBackgroundResource(imgRes);
    }

    @Override
    public void initToolbar() {
        setToolbar(true, 0);
    }

    @Override
    public void setShowLeftButton(boolean isShow) {
        get(R.id.base_left_layout).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setShowPlaceholderLeftButton(boolean isShow) {
        get(R.id.base_left_layout_placeholder).setVisibility(isShow ? View.INVISIBLE : View.GONE);
    }

    @Override
    public void setShowRightButton(boolean isShow) {
        get(R.id.base_right_layout).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setShowPlaceholderRightButton(boolean isShow) {
        get(R.id.base_right_layout_placeholder).setVisibility(isShow ? View.INVISIBLE : View.GONE);
    }

    @Override
    public void setShowTitleButton(boolean isShow) {
        get(R.id.base_title).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setToolbarTitle(String title) {
        getTextView(R.id.base_title).setText(title + "");
    }

    @Override
    public void setToolbarTitleColor(int color) {
        getTextView(R.id.base_title).setTextColor(color);
    }

    @Override
    public void setToolbarTitleColorRes(int colorRes) {
        getTextView(R.id.base_title).setTextColor(getActivity().getResources().getColor(colorRes));
    }

    public void setToolbarTitleSize(float size) {
        getTextView(R.id.base_title).setTextSize(size);
    }

    @Override
    public TextView getToolbarTitle() {
        return getTextView(R.id.base_title);
    }

    public TextView getToolbarChangeTitle() {
        return getTextView(R.id.wqChangeTitle);
    }

    @Override
    public void setToolbarLeftButton(int imgResource, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getTextView(R.id.base_left_tv).setCompoundDrawablesRelativeWithIntrinsicBounds(imgResource, 0, 0, 0);
        } else {
            getTextView(R.id.base_left_tv).setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

        }
        getTextView(R.id.base_left_tv).setText((text == null ? "" : text) + "");
    }

    @Override
    public void setOnToolbarLeftButtonClickListener(final OnToolbarButtonClickListener listener) {
        get(R.id.base_left_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });
    }

    @Override
    public void setToolbarRightButton(int imgResource, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getTextView(R.id.base_right_tv).setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, imgResource, 0);
        } else {
            getTextView(R.id.base_right_tv).setCompoundDrawablesWithIntrinsicBounds(0, 0, imgResource, 0);
        }
        getTextView(R.id.base_right_tv).setText(text + "");

    }

    public TextView getToolbarRightButton() {
        return getTextView(R.id.base_right_tv);
    }

    @Override
    public void setOnToolbarRightButtonClickListener(final OnToolbarButtonClickListener listener) {
        get(R.id.base_right_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });
    }

    @Override
    public void setToolbar(boolean isBase, int layoutResource) {
        isUseBaseToolbar = isBase;
        otherToolbarLayoutRes = layoutResource;
        updateToolbarLayout();
    }

    /**
     * 更新toolbar布局
     */
    private void updateToolbarLayout() {
        if (isUseBaseToolbar) {
            get(R.id.base_toolbar_layout).setVisibility(View.VISIBLE);
            get(R.id.other_toolbar_layout).setVisibility(View.GONE);
        } else {
            get(R.id.base_toolbar_layout).setVisibility(View.GONE);
            get(R.id.other_toolbar_layout).setVisibility(View.VISIBLE);
            if (otherToolbarLayoutRes != 0) {
                View otherToolbarView = LayoutInflater.from(this.getActivity()).inflate(otherToolbarLayoutRes, null);
                getViewGroup(R.id.other_toolbar_layout).removeAllViews();
                getViewGroup(R.id.other_toolbar_layout).addView(otherToolbarView);
            }
        }
    }

    @Override
    public void setShowToolbar(boolean isShow) {
        if (get(R.id.toolbar) != null) {
            get(R.id.toolbar).setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void setShowToolbarShadow(boolean isShow) {
        get(R.id.title_shadow).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setShowToolbarDivider(boolean isShow) {
        get(R.id.title_divider).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }
}
