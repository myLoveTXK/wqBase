package cn.wq.baseActivity.base.interfaces.list;

import cn.wq.baseActivity.view.pullRecycleView.RecycleViewDataAdapter;

/**
 * Created by W~Q on 2017/3/27.
 */

public interface IRecycleAdapter {
    RecycleViewDataAdapter getAdapter();
}
