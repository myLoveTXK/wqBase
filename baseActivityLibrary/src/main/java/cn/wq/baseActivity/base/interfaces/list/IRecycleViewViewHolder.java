package cn.wq.baseActivity.base.interfaces.list;

import android.view.ViewGroup;

import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;


/**
 * Created by W~Q on 2017/3/21.
 */

public interface IRecycleViewViewHolder {
    BaseViewHolder getViewHolder(ViewGroup parent, int viewType,IRecycleViewBind iRecycleViewBind);
    
}
