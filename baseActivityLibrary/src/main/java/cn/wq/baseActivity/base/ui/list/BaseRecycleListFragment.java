package cn.wq.baseActivity.base.ui.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.andview.refreshview.XRefreshView;

import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.BaseFragment;
import cn.wq.baseActivity.util.ClickUtil;

/**
 * Created by W~Q on 2017/3/21.
 * 封装列表基类
 */

public abstract class BaseRecycleListFragment<T extends BaseRecycleListDataViewDelegate> extends BaseFragment<T> /*implements PullRecycler.OnRecyclerRefreshListener */ {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        getViewDelegate().getRecycler().setAdapter(getAdapter());
        initRefreshListener();

    }

    public void setNeedRefresh(boolean isNeedRefresh) {
        getViewDelegate().commonRefresh.setPullRefreshEnable(isNeedRefresh);

    }

    public void setNeedLoadMore(boolean isNeedRefresh) {
        getViewDelegate().commonRefresh.setPullLoadEnable(isNeedRefresh);

    }


    public int getAdapterItemType(int position) {
        return 0;
    }


    private void initRefreshListener() {
        getViewDelegate().commonRefresh.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh(boolean isPullDown) {
                if (getActivity() instanceof BaseActivity) {
                    if (((BaseActivity) getActivity()).isDestroyedSw()) {
                        return;
                    }
                }
                getViewDelegate().setShowEmpty(false);
                currentOnRefresh(isPullDown);
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                if (getActivity() instanceof BaseActivity) {
                    if (((BaseActivity) getActivity()).isDestroyedSw()) {
                        return;
                    }
                }
                getViewDelegate().setShowEmpty(false);
                currentOnLoadMore(isSilence);
            }
        });
        getViewDelegate().emptyViewHolder.emptyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClickUtil.isFastDoubleClick()) {
                    return;
                }
                if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
                    return;
                }
                getViewDelegate().commonRefresh.post(new Runnable() {
                    @Override
                    public void run() {
                        getViewDelegate().setShowEmpty(false);
                        startRefresh();
                    }
                });

            }
        });

    }

    public void setLoadMoreComplete(boolean isComplete) {
        getViewDelegate().commonRefresh.setLoadComplete(isComplete);
    }

    public void startRefresh() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        setLoadMoreComplete(true);
        getViewDelegate().commonRefresh.startRefresh();
    }

    public void stopRefresh() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        getViewDelegate().commonRefresh.stopRefresh();
    }

    public void stopLoadMore() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        getViewDelegate().commonRefresh.stopLoadMore();
    }

    protected void currentOnRefresh(boolean isPullDown) {
        onRefresh(isPullDown);
    }

    private void currentOnLoadMore(boolean isSilence) {
        onLoadMore(isSilence);
    }

    public abstract void onRefresh(boolean isPullDown);

    public abstract void onLoadMore(boolean isSilence);
}
