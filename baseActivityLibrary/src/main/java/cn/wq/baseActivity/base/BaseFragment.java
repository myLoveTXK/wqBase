package cn.wq.baseActivity.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.andview.refreshview.utils.LogUtils;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import cn.wq.baseActivity.base.broadcast.BaseBroadcastFragment;

/**
 * Created by W~Q on 2017/3/16.
 * SW Fragment 通用基类
 */
public abstract class BaseFragment<T extends BaseViewDelegate> extends BaseBroadcastFragment<T, BaseFragment> {
    protected BaseFragment This;//当前对象,当前类便捷实用
    private boolean isViewCreated = false;

    private boolean isShowing;

    public boolean isShowing() {
        return isShowing;
    }

    /**
     * 是否需要统计信息
     *
     * @return
     */
    protected boolean isNeedStatisticsPage() {
        return true;
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        ButterKnife.bind(this, getViewDelegate().getRootView());
        ButterKnife.bind(getViewDelegate(), getViewDelegate().getRootView());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        This = this;
        super.onCreate(savedInstanceState);
        if (isNeedEventbusNotify()) {
            //注册成为订阅者
            try {
                EventBus.getDefault().register(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 是否需要Eventbus通知
     *
     * @return
     */
    public boolean isNeedEventbusNotify() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isNeedEventbusNotify()) {
            //注册成为订阅者
            try {
                EventBus.getDefault().register(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDelegate().setShowToolbar(false);
        getViewDelegate().setShowStatus(false);
        isViewCreated = true;
    }

    public boolean isViewCreated() {
        return isViewCreated;
    }

    public View getView(int id) {
        return getViewDelegate().get(id);
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) super.getActivity();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!isHidden) {
            com.lidroid.xutils.util.LogUtils.i("wq 0519 showTab");
            showTab();
        }
        if (!TextUtils.isEmpty(statisticsPageName()) && isNeedStatisticsPage()) {
            LogUtils.i("wq 0619 statisticsPageName()统计页面onResume:" + statisticsPageName());

            MobclickAgent.onPageStart(statisticsPageName()); //统计页面，"MainScreen"为页面名称，可自定义
//            MobclickAgent.onResume(getBaseActivity());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isHidden) {
            hideTab();
        }
        if (!TextUtils.isEmpty(statisticsPageName()) && isNeedStatisticsPage()) {
            LogUtils.i("wq 0619 statisticsPageName()统计页面onPause:" + statisticsPageName());
            MobclickAgent.onPageEnd(statisticsPageName());
//            MobclickAgent.onPause(getBaseActivity());
        }
    }

    /**
     * 子类集成当前方法进行页面ID统计
     *
     * @return
     */
    public String statisticsPageName() {
        return getClass().getSimpleName();
    }


    public void hideTab() {
        isShowing = false;
        com.lidroid.xutils.util.LogUtils.i("wq 0619 statisticsPageName()统计页面onPause:" + statisticsPageName());
        MobclickAgent.onPageEnd(statisticsPageName());
    }

    public void showTab() {
        isShowing = true;
        com.lidroid.xutils.util.LogUtils.i("wq 0519 showTab:" + this.getClass().getName());
        com.lidroid.xutils.util.LogUtils.i("wq 0619 statisticsPageName()统计页面onResume:" + statisticsPageName());
        MobclickAgent.onPageStart(statisticsPageName()); //统计页面，"MainScreen"为页面名称，可自定义
    }

    boolean isHidden;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isHidden = !isVisibleToUser;
        if (!isViewCreated) {
            return;
        }
        if (isVisibleToUser) {
            showTab();
        } else {
            hideTab();
        }

    }

//    @Override
//    public void setMenuVisibility(final boolean visible) {
//        super.setMenuVisibility(visible);
//        isHidden = !visible;
//        if (!isViewCreated) {
//            return;
//        }
//        if (visible) {
//            showTab();
//        } else {
//            hideTab();
//        }
//    }

    @Override
    public void onHiddenChanged(boolean isHidden) {
        super.onHiddenChanged(isHidden);
        this.isHidden = isHidden;
        if (isHidden) {// 不在最前端界面显示
            hideTab();
        } else {// 重新显示到最前端中
            com.lidroid.xutils.util.LogUtils.i("wq 0519 showTab");
            showTab();
        }
    }

}
