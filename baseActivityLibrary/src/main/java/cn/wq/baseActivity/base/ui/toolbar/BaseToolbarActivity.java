package cn.wq.baseActivity.base.ui.toolbar;

import cn.wq.baseActivity.base.broadcast.BaseBroadcastFragment;
import cn.wq.baseActivity.base.callback.BasePermissionCallbackActivity;

/**
 * Created by W~Q on 2017/3/20.
 */

public abstract class BaseToolbarActivity<T extends BaseToolbarDelegate, B extends BaseBroadcastFragment> extends BasePermissionCallbackActivity<T, B> {

    //    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        initStatus();
//    }
//
//
    @Override
    protected void initToolbar() {
        super.initToolbar();
        getViewDelegate().initToolbar();
        getViewDelegate().setShowToolbar(true);
    }
}
