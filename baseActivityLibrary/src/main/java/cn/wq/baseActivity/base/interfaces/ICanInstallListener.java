package cn.wq.baseActivity.base.interfaces;

/**
 * Created by xt on 2019/9/3.
 * 是否能安装APP监听
 */

public interface ICanInstallListener {

    void onCanInstall(boolean isCanInstall);

}
