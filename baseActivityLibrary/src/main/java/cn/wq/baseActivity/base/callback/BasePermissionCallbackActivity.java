package cn.wq.baseActivity.base.callback;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.igo.themvp.view.IDelegate;
import cn.wq.baseActivity.base.broadcast.BaseBroadcastFragment;
import cn.wq.baseActivity.base.interfaces.IActivityCallback;
import cn.wq.baseActivity.base.interfaces.ICanInstallListener;
import cn.wq.baseActivity.base.interfaces.IPermissionListener;

/**
 * Created by xt on 2019/09/03.
 * 封装activity 权限条件
 */
public abstract class BasePermissionCallbackActivity<T extends IDelegate, B extends BaseBroadcastFragment> extends BaseCallbackActivity<T, B> {

    final int swRequestCode = 661;

    Map<String, IPermissionListener> iPermissionListenerMap = new HashMap<>();

    public synchronized void startActivity(Class<?> clas, String permission) {
        Intent intent = new Intent(this, clas);
        startActivityForResult(clas, intent, null, permission);
    }

    public synchronized void startActivity(Class<?> clas, Intent intent, String permission) {
        if (intent == null) {
            intent = new Intent(this, clas);
        }
        intent.setClass(this, clas);
        startActivityForResult(clas, intent, null, permission);
    }

    public synchronized void startActivityForResult(Class<?> clas, IActivityCallback activityCallback, String permission) {
        startActivityForResult(clas, null, activityCallback, permission);
    }

    /**
     * 启用回调函数接受页面返回值
     *
     * @param intent           传参数用
     * @param activityCallback 回调监听
     */
    public synchronized void startActivityForResult(final Class<?> clas, final Intent intent, final IActivityCallback activityCallback, String permission) {

        applyPermission(permission, new IPermissionListener() {
            @Override
            public void onPermissionGranted(String permission) {
                startActivityForResult(clas, intent, activityCallback);
            }

            @Override
            public void onPermissionDenied(String permission) {
            }
        });

    }

    /**
     * 启用回调函数接受页面返回值
     *
     * @param intent           传参数用
     * @param activityCallback 回调监听
     */
    public synchronized void startActivityForResult(final Intent intent, final IActivityCallback activityCallback, String permission) {

        applyPermission(permission, new IPermissionListener() {
            @Override
            public void onPermissionGranted(String permission) {
                startActivityForResult(intent, activityCallback);
            }

            @Override
            public void onPermissionDenied(String permission) {

            }
        });

    }

    public synchronized void applyOrderPermission(IPermissionListener iPermissionListener, String... permissions) {

        if (permissions == null) {

        }
        int position = 0;
        orderApplyPermission(permissions, position, iPermissionListener);
    }

    /**
     * 顺序申请权限
     *
     * @param permissions
     * @param position
     * @param iPermissionListener
     */
    private void orderApplyPermission(final String[] permissions, final int position, final IPermissionListener iPermissionListener) {
        if (permissions == null || position < 0 || position >= permissions.length) {
            iPermissionListener.onPermissionGranted(null);
            return;
        }
        applyPermission(permissions[position], new IPermissionListener() {
            @Override
            public void onPermissionGranted(String permission) {
                if ((position + 1) == permissions.length) {
                    iPermissionListener.onPermissionGranted(permissions[position]);
                } else {
                    orderApplyPermission(permissions, position + 1, iPermissionListener);
                }
            }

            @Override
            public void onPermissionDenied(String permissions) {
                iPermissionListener.onPermissionDenied(permissions);
            }
        });
    }


    public synchronized void applyPermission(String permission, IPermissionListener iPermissionListener) {
//        if (iPermissionListenerMap.get(permission) != null) {
////            int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
////            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
////                iPermissionListenerMap.get(permission).onPermissionDenied(permission);
////            } else {
////                iPermissionListenerMap.get(permission).onPermissionGranted(permission);
////            }
//            iPermissionListenerMap.put(permission, null);
//            return;
//        }

        int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            LogUtils.i("wq 0903 没有权限 发起申请权限 ： " + permission);
            iPermissionListenerMap.put(permission, iPermissionListener);
            ActivityCompat.requestPermissions(this, new String[]{permission}, swRequestCode);
        } else {
            if (iPermissionListener != null) {
                iPermissionListener.onPermissionGranted(permission);
            }

        }
    }

    public synchronized void applyPermission(List<String> permissions) {
        Map<String, IPermissionListener> listenerMap = new HashMap<>();
        for (String permission : permissions) {
            listenerMap.put(permission, null);
        }
        applyPermission(listenerMap);
    }


    public synchronized void applyPermission(Map<String, IPermissionListener> map) {
        if (map == null) {
            return;
        }
        List<String> permissions = new ArrayList<>();
        for (String permission : map.keySet()) {
            if (iPermissionListenerMap.get(permission) != null) {
                int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    iPermissionListenerMap.get(permission).onPermissionDenied(permission);
                } else {
                    iPermissionListenerMap.get(permission).onPermissionGranted(permission);
                }
            }
            iPermissionListenerMap.put(permission, map.get(permission));
            permissions.add(permission);
        }
        String[] array = permissions.toArray(new String[permissions.size()]);
        if (array != null && array.length > 0) {
            ActivityCompat.requestPermissions(this, array, swRequestCode);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        LogUtils.i("wq 0904 isMainThread:" + isMainThread());
        if (requestCode == swRequestCode) {
            if (grantResults != null) {
                for (int i = 0; i < grantResults.length; i++) {
                    int grantResult = grantResults[i];
                    if (isPermissionBoolean(grantResult)) {
                        if (permissions != null && permissions.length > i) {
                            LogUtils.i("wq 0903 获取 " + permissions[i] + " 权限成功");
                            if (iPermissionListenerMap.get(permissions[i]) != null) {
                                iPermissionListenerMap.get(permissions[i]).onPermissionGranted(permissions[i]);
                            }
                        } else {
                            LogUtils.i("wq 0903 获取 未知 权限成功");
                        }
                    } else {
                        if (permissions != null && permissions.length > i) {
                            LogUtils.e("wq 0903 获取 " + permissions[i] + " 权限失败");
                            if (iPermissionListenerMap.get(permissions[i]) != null) {
                                iPermissionListenerMap.get(permissions[i]).onPermissionDenied(permissions[i]);
                            }
                        } else {
                            LogUtils.e("wq 0903 获取 未知 权限失败");
                        }
                    }
                }
            }
        } else {
            LogUtils.i("wq 0903 获取 权限  非 swRequestCode");
        }
    }

    public boolean isMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    /**
     * 权限是否通过
     *
     * @param grantResult
     * @return
     */
    protected boolean isPermissionBoolean(int grantResult) {
        return grantResult == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 检查是否设置过了可安装未知来源
     *
     * @param iCanInstallListener
     */
    public void checkCanAppInstall(final ICanInstallListener iCanInstallListener) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            boolean isCanInstall = getPackageManager().canRequestPackageInstalls();
            LogUtils.i("wq 0905 isCanInstall:" + isCanInstall);
            if (isCanInstall) {
                iCanInstallListener.onCanInstall(isCanInstall);
            } else {
                Uri packageURI = Uri.parse("package:" + getPackageName());
                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, packageURI);
                startActivityForResult(intent, new IActivityCallback() {
                    @Override
                    public void callback(int resultCode, Intent data) {
                        if (resultCode == RESULT_OK) {
                            iCanInstallListener.onCanInstall(true);
                        } else {
                            iCanInstallListener.onCanInstall(false);
                        }

                    }
                });
            }
        } else {
            LogUtils.i("wq 0905 Build.VERSION.SDK_INT:" + Build.VERSION.SDK_INT);
            iCanInstallListener.onCanInstall(true);
        }
    }


}
