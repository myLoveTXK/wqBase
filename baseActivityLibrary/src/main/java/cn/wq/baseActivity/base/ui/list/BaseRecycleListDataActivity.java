package cn.wq.baseActivity.base.ui.list;

import android.os.Bundle;
import android.view.View;

import com.andview.refreshview.XRefreshViewFooter;

import java.util.List;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.interfaces.list.IRecycleAdapter;
import cn.wq.baseActivity.base.interfaces.list.IRecycleViewBind;
import cn.wq.baseActivity.base.ui.interfaces.IItemListenter;
import cn.wq.baseActivity.util.Config;
import cn.wq.baseActivity.view.pullRecycleView.RecycleViewDataAdapter;
import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;

/**
 * Created by W~Q on 2018/5/11.
 * 封装刷新
 * 1.分页
 * 2.adapter
 * 3.成功设置数据
 * 4.失败设置文案（通用情况）
 * <p>
 * 继承此类，只需处理关键数据即可
 */

public abstract class BaseRecycleListDataActivity<T extends BaseRecycleListDataViewDelegate<Data>, Data> extends BaseRecycleListActivity<T> implements IRecycleAdapter, IRecycleViewBind, IItemListenter<Data> {

    protected int page = 1;
    protected int pageSize = 20;
    protected RecycleViewDataAdapter<Data> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getViewDelegate().commonRefresh.setAutoLoadMore(true);

        getViewDelegate().getRecycler().setAdapter(getAdapter());
        getAdapter().setCustomLoadMoreView(new XRefreshViewFooter(this));

        setNeedRefresh(true);
        setNeedLoadMore(false);
        getViewDelegate().commonRefresh.setAutoLoadMore(true);

    }

    @Override
    protected void currentOnRefresh(boolean isPullDown) {
        page = 1;
        getAdapter().clear();
        super.setLoadMoreComplete(false);
        super.setNeedLoadMore(false);
        super.currentOnRefresh(isPullDown);
    }

    public Data getItem(int position) {
        if (adapter == null) {
            return null;
        }
        return adapter.getItem(position);
    }


    @Override
    public RecycleViewDataAdapter<Data> getAdapter() {
        if (adapter == null) {
            adapter = new RecycleViewDataAdapter<Data>(getViewDelegate()) {
                @Override
                public int getItemType(int position) {
                    return getAdapterItemType(position);
                }
            };
            adapter.setiRecycleViewBind(this);
            adapter.setCustomLoadMoreView(new XRefreshViewFooter(This));
        }
        return adapter;
    }


    /**
     * 移民设计为没有两层，那么只用content
     *
     * @return
     */
    public String getEmptyTitle() {
        return Config.emptyListDataTitle;
    }


    public String getEmptyContent() {
        return Config.emptyListDataContent;
    }

    /**
     * 设置请求失败
     *
     * @param result
     * @param isRefresh
     * @param isNetworkError
     */
    protected void setApiError(String result, boolean isRefresh, boolean isNetworkError) {
        if (isRefresh) {
            getViewDelegate().setEmptyNoNetwork(isNetworkError, result);
            getViewDelegate().setShowEmpty(true);
            if (isRefresh) {
                stopRefresh();
            }
        } else {
            stopLoadMore();
        }
    }

    protected void setApiEmptyView(boolean isRefresh, View empty) {
        if (isRefresh) {
            stopRefresh();
        } else {
            stopLoadMore();
        }
        getViewDelegate().setEmptyView(empty);
        getViewDelegate().setShowEmpty(true);
    }

    @Override
    public void setNeedLoadMore(boolean isNeedLoadMore) {
        this.isNeedLoadMore = isNeedLoadMore;
        super.setNeedLoadMore(isNeedLoadMore);
    }

    boolean isNeedLoadMore = true;

    /**
     * 是否需要加载更多
     *
     * @return
     */
    protected boolean isNeedLoadMore() {
        return isNeedLoadMore;
    }

    /**
     * 单纯设置数据
     *
     * @param beans
     * @param isRefresh
     */
    protected void setDataSingleData(List<Data> beans, boolean isRefresh) {
        if (isRefresh) {
            getAdapter().clear();
        }
        getAdapter().addData(beans);
        if (isRefresh && beans != null && beans.size() > 0) {
            getViewDelegate().getRecycler().scrollToPosition(0);
        }
    }


    protected void setDataSingleData(Data bean, boolean isRefresh) {
        if (isRefresh) {
            getAdapter().clear();
        }
        getAdapter().addData(bean);
        if (isRefresh && bean != null) {
            getViewDelegate().getRecycler().scrollToPosition(0);
        }
    }

    public int getEmptyImgRes() {
        return R.mipmap.img_erro_empty;
    }

    /**
     * 设置请求成功数据
     * 封装
     */
    protected void setApiData(List<Data> beans, boolean isRefresh) {
        if (beans == null || beans == null || beans.size() == 0) {
            if (isRefresh) {
                getViewDelegate().setEmpty(getEmptyImgRes(), getEmptyTitle(), getEmptyContent());
                getViewDelegate().setShowEmpty(true);
            } else {
                setLoadMoreComplete(true);
            }
        } else {
            setDataSingleData(beans, isRefresh);
            page++;
            super.setNeedLoadMore(isNeedLoadMore());
            if (pageSize > beans.size()) {
                setLoadMoreComplete(true);
            } else {
                setLoadMoreComplete(false);
            }
        }
        if (isRefresh) {
            stopRefresh();
        } else {
            stopLoadMore();
        }
    }

    @Override
    public void onBindViewHolderData(int viewType, BaseViewHolder baseViewHolder, final int position) {
        int size = ((getAdapter() == null || getAdapter().getDataList() == null) ? 0 : getAdapter().getDataList().size());
        getViewDelegate().onBindData(viewType, baseViewHolder, getItem(position), position, size);
        onViewHolderListener(viewType, baseViewHolder, getItem(position), position);
    }

}
