package cn.wq.baseActivity.base;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.ui.toolbar.BaseToolbarDelegate;
import cn.wq.baseActivity.util.StatusUtil;

/**
 * Created by W~Q on 2017/3/20.
 * 基类V代理
 * 暂时保留当前类，作为未知需求的扩展基类(同BaseActivity)
 */
public abstract class BaseViewDelegate extends BaseToolbarDelegate {

    private BaseActivity activity;


    @Override
    public BaseActivity getActivity() {
        if (activity != null) {
            return activity;
        }
        return super.getActivity();
    }




    public View getContentLayout() {
        return get(R.id.sw_content);
    }

    public void setActivity(BaseActivity activity) {
        this.activity = activity;
    }

    public ViewGroup getBaseContentLayout() {
        return getViewGroup(R.id.sw_content);
    }

    public ViewGroup getBaseLayout() {
        return getViewGroup(R.id.base_layout);
    }

    @Override
    public int getContentBaseRelativeLayout() {//是否全屏内容
        return 2;
    }

    /**
     * 状态栏颜色设置，目前支持5.0
     *
     * @param color
     */
    public void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            get(R.id.base_status_view).setBackgroundColor(color);
        }
    }

    public void setStatusBarColorRes(int colorRes) {
        setStatusBarColor(getActivity().getResources().getColor(colorRes));
    }

    /**
     * 状态栏图片设置，目前支持5.0
     *
     * @param imgRes
     */
    public void setStatusImg(int imgRes) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        get(R.id.base_status_view).setBackgroundResource(imgRes);
//        }
    }

    public void setShowStatus(boolean isShow) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            StatusUtil.initStatusBar_LOLLIPOP(getActivity());

            get(R.id.base_status_view).setVisibility(isShow ? View.VISIBLE : View.GONE);

//            setStatusBarColor(getActivity().getResources().getColor(R.color.baseColorPrimaryDark));
        }
    }

    /**
     * 设置背景颜色
     *
     * @param colorRes
     */
    public void setBaseLayoutColorRes(int colorRes) {
        get(R.id.base_layout).setBackgroundResource(colorRes);
    }

    /**
     * 初始化状态栏，目前支持5.0
     */
    public void initStatus() {
        //设置状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            StatusUtil.initStatusBar_LOLLIPOP(getActivity());
            StatusUtil.setFlymeSetStatusBarTextColorDark(getActivity().getWindow(), true);
            StatusUtil.setMIUISetStatusBarTextColorDark(getActivity().getWindow(), true);

            //设置statusBar高度
            ViewGroup.LayoutParams statusLayoutParams = get(R.id.base_status_view).getLayoutParams();
            statusLayoutParams.height = StatusUtil.getStatusHeight(getActivity());
            get(R.id.base_status_view).setLayoutParams(statusLayoutParams);
            setStatusBarColor(getActivity().getResources().getColor(R.color.baseColorPrimaryDark));
        } else {

        }
    }

    @Override
    public void initWidget() {
        super.initWidget();
        initStatus();

    }


}
