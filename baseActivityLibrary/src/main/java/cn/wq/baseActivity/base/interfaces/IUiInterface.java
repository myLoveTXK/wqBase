package cn.wq.baseActivity.base.interfaces;

import android.view.View;
import android.widget.TextView;

/**
 * Created by W~Q on 2017/3/20.
 * 基础UI包含接口定义
 */
public interface IUiInterface {

    /**
     * toolbar 接口
     */
    interface BaseToolbarInterface {

        //toolbar 分割线
        View getToolDividerView();

        void initToolbar();

        void setShowLeftButton(boolean isShow);

        void setShowPlaceholderLeftButton(boolean isShow);

        void setShowRightButton(boolean isShow);

        void setShowPlaceholderRightButton(boolean isShow);

        void setShowTitleButton(boolean isShow);

        /**
         * 使用其他的toolbar，扩展toolbar
         *
         * @param isBase         是否使用基类的toolbar
         * @param layoutResource 自定义toolbar布局
         */
        void setToolbar(boolean isBase, int layoutResource);

        void setToolbarDesignHeight(int designHeight);

        void setShowToolbar(boolean isShow);//是否显示toolbar


        //标题
        void setToolbarTitle(String title);//设置标题

        TextView getToolbarTitle();//获取标题TextView

        void setToolbarTitleColor(int color);//设置标题颜色

        void setToolbarTitleColorRes(int colorRes);//设置标题颜色


        //设置左边图片文字及监听
        void setToolbarLeftButton(int imgResource, String text);

        void setOnToolbarLeftButtonClickListener(OnToolbarButtonClickListener listener);

        //设置右边图片文字及监听
        void setToolbarRightButton(int imgResource, String text);

        void setOnToolbarRightButtonClickListener(OnToolbarButtonClickListener listener);


        //toolbar背景
        void setToolbarBackgroundColor(int color);

        void setToolbarBackgroundImgRes(int imgRes);

        void setToolbarBackgroundColorRes(int colorRes);

        //设置头部阴影
        void setShowToolbarShadow(boolean isShow);

        //设置头部分割线
        void setShowToolbarDivider(boolean isShow);

        interface OnToolbarButtonClickListener {
            void onClick();
        }
    }


    interface BaseToolbarTwoButtonInterface {
        void setShowLeftButtonTwo(boolean isShow);

        void setShowRightButtonTwo(boolean isShow);

        void setShowPlaceholderButtonTwo(boolean isShow);

        //设置左边图片文字及监听
        void setToolbarLeftButtonTwo(int imgResource, String text);

        void setOnToolbarLeftButtonClickListenerTwo(OnToolbarButtonTwoClickListener listener);

        //设置右边边图片文字及监听
        void setToolbarRightButtonTwo(int imgResource, String text);

        void setOnToolbarRightButtonClickListenerTwo(OnToolbarButtonTwoClickListener listener);


        interface OnToolbarButtonTwoClickListener {
            void onClick();
        }
    }


    interface BaseContentInterface {
        View getContentLayout();

        void setContentLayoutBackgroundColor(int color);

        void setContentLayoutBackgroundColorRes(int colorRes);
    }
}
