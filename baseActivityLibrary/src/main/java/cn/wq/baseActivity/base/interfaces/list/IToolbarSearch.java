package cn.wq.baseActivity.base.interfaces.list;

/**
 * Created by W~Q on 2017/3/21.
 */

public interface IToolbarSearch {
    void setNeedSearchToolbar(boolean isNeed);
}
