package cn.wq.baseActivity.base.ui.list;


import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.andview.refreshview.recyclerview.XSpanSizeLookup;
import com.andview.refreshview.utils.LogUtils;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.BaseViewDelegate;
import cn.wq.baseActivity.base.interfaces.list.IRecycleViewViewHolder;
import cn.wq.baseActivity.base.ui.interfaces.IListExtendLayout;
import cn.wq.baseActivity.util.EmptyViewHolder;
import cn.wq.baseActivity.view.pullRecycleView.PullRecycler;
import cn.wq.baseActivity.view.pullRecycleView.layoutmanager.ILayoutManager;
import cn.wq.baseActivity.view.pullRecycleView.layoutmanager.MyGridLayoutManager;
import cn.wq.baseActivity.view.pullRecycleView.layoutmanager.MyLinearLayoutManager;


/**
 * Created by W~Q on 2017/3/20.
 * 基类V代理
 * 列表页基类
 */
public abstract class BaseRecycleListViewDelegate extends BaseViewDelegate implements IRecycleViewViewHolder, IListExtendLayout {

    protected PullRecycler recycler;
    public XRefreshView commonRefresh;
    protected EmptyViewHolder emptyViewHolder;

    public XRefreshView getCommonRefresh() {
        return commonRefresh;
    }

    public PullRecycler getRecycler() {
        return recycler;
    }

    @Override
    public void initWidget() {
        super.initWidget();
        emptyViewHolder = new EmptyViewHolder(get(R.id.empty_layout));

        View extendEmpty = getExtendEmptyLayout();
        if (extendEmpty != null) {
            getViewGroup(R.id.extend_empty_layout).removeAllViews();
            getViewGroup(R.id.extend_empty_layout).addView(extendEmpty);
        }

        commonRefresh = get(R.id.common_refresh);
        commonRefresh.setPinnedTime(3 * 1000);

        recycler = get(R.id.pullRecycler);
        recycler.setLayoutManager(getLayoutManager());
        recycler.addItemDecoration(getItemDecoration());
    }


    protected View getExtendEmptyLayout() {
        return null;
    }

    protected View getExtendNetworkEmptyLayout() {
        return null;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_base_recycleview_list_new;
    }


    public ILayoutManager getLayoutManager() {
        return new MyLinearLayoutManager(this.getActivity().getApplicationContext());
    }

    public ILayoutManager getGridLayoutManager(int spanCount, final WqSpanSize wqSpanSize) {
        MyGridLayoutManager myGridLayoutManager = new MyGridLayoutManager(this.getActivity().getApplicationContext(), spanCount);
        myGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                final BaseRecyclerAdapter adapter = (BaseRecyclerAdapter) getRecycler().getAdapter();
                if (adapter == null) {
                    return 1;
                }
                boolean isHeaderOrFooter = adapter.isFooter(position) || adapter.isHeader(position);
                if (isHeaderOrFooter) {
                    return myGridLayoutManager.getSpanCount();
                } else {
                    if (wqSpanSize == null) {
                        return 1;
                    } else {
                        if (adapter.isHasHead()) {
                            return wqSpanSize.getWqSpanSize(position - 1);
                        } else {
                            return wqSpanSize.getWqSpanSize(position);
                        }
                    }

                }
            }
        });
        return myGridLayoutManager;
    }

    /**
     * 复杂布局使用
     * 首页比较杂的布局
     */
    public interface WqSpanSize {
        int getWqSpanSize(int position);
    }


    public class WqSpanSizeLookup extends XSpanSizeLookup {

        public WqSpanSizeLookup(BaseRecyclerAdapter adapter, int spanSize) {
            super(adapter, spanSize);
        }

        @Override
        public int getSpanSize(int position) {
            LogUtils.i("wq 1221 XRefresh 中使用getSpanSize：" + position);
            return super.getSpanSize(position);
        }
    }


    protected RecyclerView.ItemDecoration getItemDecoration() {
//        return new DividerItemDecoration(getActivity(), R.drawable.list_divider);
        return null;
    }

    @Override
    public ViewGroup getExtendHeadLayout() {
        return get(R.id.extend_head_layout);
    }

    @Override
    public ViewGroup getExtendContentLayout() {
        return get(R.id.extend_content_layout);
    }

    @Override
    public ViewGroup getExtendFootLayout() {
        return get(R.id.extend_foot_layout);
    }


    public void setEmptyNoNetwork(boolean isNetworkError, String error) {
        if (emptyViewHolder == null) {
            return;
        }
        if (isNetworkError) {
            if (getExtendNetworkEmptyLayout() != null) {
                View layout = getExtendNetworkEmptyLayout();
//                View extendEmpty = LayoutInflater.from(getActivity()).inflate(layout, null, false);
                getViewGroup(R.id.extend_empty_layout).removeAllViews();
                getViewGroup(R.id.extend_empty_layout).addView(layout);

            } else {
                emptyViewHolder.emptyImg.setImageResource(R.mipmap.img_erro_wifi);
                emptyViewHolder.emptyTitle.setText("网络异常");
                emptyViewHolder.emptyContent.setText("请检查您的网络设置，点击刷新");
            }

        } else {
            if (getExtendEmptyLayout() != null) {
                View layout = getExtendEmptyLayout();
//                View extendEmpty = LayoutInflater.from(getActivity()).inflate(layout, null, false);
                getViewGroup(R.id.extend_empty_layout).removeAllViews();
                getViewGroup(R.id.extend_empty_layout).addView(layout);
            } else {
                emptyViewHolder.emptyImg.setImageResource(R.mipmap.img_erro_loadfail);
                emptyViewHolder.emptyTitle.setText("");
                emptyViewHolder.emptyContent.setText(error);
            }
        }
    }

    public void setEmptyView(View emptyView) {

        getViewGroup(R.id.extend_empty_layout).removeAllViews();
        getViewGroup(R.id.extend_empty_layout).addView(emptyView);
    }


    public void setEmptyNoData() {
        if (emptyViewHolder == null) {
            return;
        }
        emptyViewHolder.emptyImg.setImageResource(R.mipmap.img_erro_empty);
        emptyViewHolder.emptyTitle.setText("");
        emptyViewHolder.emptyContent.setText("数据为空");
    }

    public void setEmpty(int imgRes, String title, String content) {
        if (emptyViewHolder == null) {
            return;
        }
        emptyViewHolder.emptyImg.setImageResource(imgRes);

        emptyViewHolder.emptyTitle.setText(title);
        emptyViewHolder.emptyContent.setText(content);

        if (TextUtils.isEmpty(title)) {
            emptyViewHolder.emptyTitle.setVisibility(View.GONE);
        } else {
            emptyViewHolder.emptyTitle.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(content)) {
            emptyViewHolder.emptyContent.setVisibility(View.GONE);
        } else {
            emptyViewHolder.emptyContent.setVisibility(View.VISIBLE);
        }
    }

    public void setShowEmpty(boolean isShow) {
        if (emptyViewHolder == null) {
            return;
        }
        emptyViewHolder.emptyLayout.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }


}
