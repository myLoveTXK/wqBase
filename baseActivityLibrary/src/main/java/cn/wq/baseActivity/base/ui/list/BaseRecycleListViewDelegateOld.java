package cn.wq.baseActivity.base.ui.list;


import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.andview.refreshview.XRefreshView;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.BaseViewDelegate;
import cn.wq.baseActivity.base.interfaces.list.IRecycleViewViewHolder;
import cn.wq.baseActivity.util.EmptyViewHolder;
import cn.wq.baseActivity.view.pullRecycleView.PullRecycler;
import cn.wq.baseActivity.view.pullRecycleView.layoutmanager.ILayoutManager;
import cn.wq.baseActivity.view.pullRecycleView.layoutmanager.MyLinearLayoutManager;


/**
 * Created by W~Q on 2017/3/20.
 * 基类V代理
 * 列表页基类
 */
public abstract class BaseRecycleListViewDelegateOld extends BaseViewDelegate implements IRecycleViewViewHolder {

    protected PullRecycler recycler;
    public XRefreshView commonRefresh;
    protected EmptyViewHolder emptyViewHolder;

    public PullRecycler getRecycler() {
        return recycler;
    }

    @Override
    public void initWidget() {
        super.initWidget();
        emptyViewHolder = new EmptyViewHolder(get(R.id.empty_layout));

        commonRefresh = get(R.id.common_refresh);
        commonRefresh.setPinnedTime(3 * 1000);

        recycler = get(R.id.pullRecycler);
        recycler.setLayoutManager(getLayoutManager());
        recycler.addItemDecoration(getItemDecoration());
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_base_recycleview_list;
    }


    public ILayoutManager getLayoutManager() {
        return new MyLinearLayoutManager(
                this.getActivity().getApplicationContext());
    }

    protected RecyclerView.ItemDecoration getItemDecoration() {
//        return new DividerItemDecoration(getActivity(), R.drawable.list_divider);
        return null;
    }

    public void setEmptyNoNetwork(boolean isNetworkError, String error) {
        if (emptyViewHolder == null) {
            return;
        }
        if (isNetworkError) {
            emptyViewHolder.emptyImg.setImageResource(R.mipmap.img_erro_wifi);
            emptyViewHolder.emptyTitle.setText("网络异常");
            emptyViewHolder.emptyContent.setText("请确定网络连接，并在空白处单击重新加载！");
        } else {
            emptyViewHolder.emptyImg.setImageResource(R.mipmap.img_erro_loadfail);
            emptyViewHolder.emptyTitle.setText("");
            emptyViewHolder.emptyContent.setText(error);
        }

    }

    public void setEmptyNoData() {
        if (emptyViewHolder == null) {
            return;
        }
        emptyViewHolder.emptyImg.setImageResource(R.mipmap.img_erro_empty);
        emptyViewHolder.emptyTitle.setText("");
        emptyViewHolder.emptyContent.setText("数据为空");
    }

    public void setEmpty(int imgRes, String title, String content) {
        if (emptyViewHolder == null) {
            return;
        }
        emptyViewHolder.emptyImg.setImageResource(imgRes);

        emptyViewHolder.emptyTitle.setText(title);
        emptyViewHolder.emptyContent.setText(content);

        if (TextUtils.isEmpty(title)) {
            emptyViewHolder.emptyTitle.setVisibility(View.GONE);
        } else {
            emptyViewHolder.emptyTitle.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(content)) {
            emptyViewHolder.emptyContent.setVisibility(View.GONE);
        } else {
            emptyViewHolder.emptyContent.setVisibility(View.VISIBLE);
        }
    }

    public void setShowEmpty(boolean isShow) {
        if (emptyViewHolder == null) {
            return;
        }
        emptyViewHolder.emptyLayout.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }


}
