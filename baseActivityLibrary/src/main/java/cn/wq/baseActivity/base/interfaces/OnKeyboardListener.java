package cn.wq.baseActivity.base.interfaces;

/**
 * Created by Administrator on 2016/9/14.
 * 键盘监听事件 弹起 和关闭
 */
public interface OnKeyboardListener {
    void onShowKeyboard();
    void onCloseKeyboard();
}
