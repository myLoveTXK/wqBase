package cn.wq.baseActivity.base.ui.interfaces;

import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;

/**
 * Created by xt on 2018/5/11.
 */

public interface IItemListenter<Data> {
    void onViewHolderListener(int viewType, BaseViewHolder baseViewHolder, Data data, final int position);
}
