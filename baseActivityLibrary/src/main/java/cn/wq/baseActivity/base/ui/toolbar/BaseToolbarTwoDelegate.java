package cn.wq.baseActivity.base.ui.toolbar;

import android.os.Build;
import android.view.View;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.interfaces.IUiInterface;

/**
 * Created by W~Q on 2017/3/20.
 * 默认设置左右按钮，中间title
 * 增加第二个按钮
 */
public abstract class BaseToolbarTwoDelegate extends BaseToolbarDelegate implements IUiInterface.BaseToolbarTwoButtonInterface {

    @Override
    protected int getBaseRootLayoutID() {
        return R.layout.activity_base_button_two;
    }

    @Override
    public void setShowLeftButtonTwo(boolean isShow) {
        get(R.id.base_left_layout_two).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setShowRightButtonTwo(boolean isShow) {
        get(R.id.base_right_layout_two).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setToolbarLeftButtonTwo(int imgResource, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getTextView(R.id.base_left_tv_two).setCompoundDrawablesRelativeWithIntrinsicBounds(imgResource, 0, 0, 0);
        } else {
            getTextView(R.id.base_left_tv_two).setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

        }
        getTextView(R.id.base_left_tv_two).setText((text == null ? "" : text) + "");
    }

    @Override
    public void setOnToolbarLeftButtonClickListenerTwo(final OnToolbarButtonTwoClickListener listener) {
        get(R.id.base_left_layout_two).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });
    }

    @Override
    public void setToolbarRightButtonTwo(int imgResource, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getTextView(R.id.base_right_tv_two).setCompoundDrawablesRelativeWithIntrinsicBounds(imgResource, 0, 0, 0);
        } else {
            getTextView(R.id.base_right_tv_two).setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
        }
        getTextView(R.id.base_right_tv_two).setText((text == null ? "" : text) + "");
    }

    @Override
    public void setOnToolbarRightButtonClickListenerTwo(final OnToolbarButtonTwoClickListener listener) {
        get(R.id.base_right_layout_two).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick();
                }
            }
        });
    }
}
