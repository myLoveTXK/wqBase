package cn.wq.baseActivity.base.ui.list;

import android.os.Bundle;
import android.view.View;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;

import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.interfaces.list.IRecycleAdapter;
import cn.wq.baseActivity.base.interfaces.list.IRecycleViewBind;
import cn.wq.baseActivity.util.ClickUtil;
import cn.wq.baseActivity.view.pullRecycleView.RecycleViewDataAdapter;

/**
 * Created by W~Q on 2017/3/21.
 */

public abstract class BaseRecycleListActivityOld<T extends BaseRecycleListViewDelegateOld, Data> extends BaseActivity<T>
        implements /*PullRecycler.OnRecyclerRefreshListener, */IRecycleAdapter, IRecycleViewBind {
    protected int page = 1;
    protected int pageSize = 20;
    protected RecycleViewDataAdapter adapter;

    public void setNeedRefresh(boolean isNeedRefresh) {
        getViewDelegate().commonRefresh.setPullRefreshEnable(isNeedRefresh);

    }

    public void setNeedLoadMore(boolean isNeedRefresh) {
        getViewDelegate().commonRefresh.setPullLoadEnable(isNeedRefresh);

    }

    @Override
    public RecycleViewDataAdapter<Data> getAdapter() {
        if (adapter == null) {
//            adapter = new RecycleViewDataAdapter<>(getViewDelegate());
//            adapter.setiRecycleViewBind(this);

            adapter = new RecycleViewDataAdapter<Data>(getViewDelegate()) {

                @Override
                public int getItemType(int position) {
                    return getAdapterItemType(position);
                }
            };
            adapter.setiRecycleViewBind(this);
            adapter.setCustomLoadMoreView(new XRefreshViewFooter(This));
        }
        return adapter;
    }

    public int getAdapterItemType(int position) {
        return 0;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getViewDelegate().getRecycler().setOnRefreshListener(this);
        getViewDelegate().getRecycler().setAdapter(getAdapter());
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        initRefreshListener();

        setNeedRefresh(true);
        setNeedLoadMore(false);
        //第一版本加载更多比较少 ，基类就使用false，使用加载更多的设置true

    }

    private void initRefreshListener() {
        getViewDelegate().commonRefresh.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh(boolean isPullDown) {
                if (isDestroyedSw()) {
                    return;
                }
                BaseRecycleListActivityOld.this.onRefresh(isPullDown);
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                if (isDestroyedSw()) {
                    return;
                }
                BaseRecycleListActivityOld.this.onLoadMore(isSilence);
            }
        });
        getViewDelegate().emptyViewHolder.emptyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClickUtil.isFastDoubleClick()) {
                    return;
                }
                if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
                    return;
                }
                getViewDelegate().commonRefresh.post(new Runnable() {
                    @Override
                    public void run() {
                        getViewDelegate().emptyViewHolder.emptyLayout.setVisibility(View.GONE);
                        startRefresh();
                    }
                });

            }
        });

    }

    public void setLoadMoreComplete(boolean isComplete) {
        getViewDelegate().commonRefresh.setLoadComplete(isComplete);
    }

    public void startRefresh() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        setLoadMoreComplete(true);
        getViewDelegate().commonRefresh.startRefresh();
    }

    public void stopRefresh() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        getViewDelegate().commonRefresh.stopRefresh();
    }

    public void stopLoadMore() {
        if (getViewDelegate() == null || getViewDelegate().commonRefresh == null) {
            return;
        }
        getViewDelegate().commonRefresh.stopLoadMore();
    }

    public abstract void onRefresh(boolean isPullDown);

    public abstract void onLoadMore(boolean isSilence);


}