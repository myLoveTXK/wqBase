package cn.wq.baseActivity.base.interfaces;

import java.util.List;

import cn.wq.baseActivity.base.broadcast.BaseBroadcastFragment;

/**
 * Created by W~Q on 2017/3/16.
 * 注册Fragment
 */
public interface IRegisterFragment<B extends BaseBroadcastFragment> {
    /**
     * 注册当前包含的fragment
     *
     * @param registerFragment
     */
    void resisterFragment(List<B> registerFragment);

    /**
     * 返回当前注册的fragment
     *
     * @return
     */
    List<B> getResisterFragment();
}
