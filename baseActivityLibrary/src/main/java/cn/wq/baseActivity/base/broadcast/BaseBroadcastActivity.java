package cn.wq.baseActivity.base.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import java.util.List;

import cn.igo.themvp.view.IDelegate;
import cn.wq.baseActivity.base.broadcast.BroadcastEntity.BaseBroadcastEntity;
import cn.wq.baseActivity.base.broadcast.manager.BroadcastManager;
import cn.wq.baseActivity.base.interfaces.IBroadcastListener;
import cn.wq.baseActivity.base.interfaces.IRegisterFragment;
import cn.wq.baseActivity.base.rx.RxBaseActivity;

/**
 * Created by W~Q on 2017/3/16.
 * 封装广播基类
 * 1.增加封装广播,基类注册，子类只需增加回调函数，处理自己业务逻辑,无需再次注册,
 * 并且覆盖到fragment(需要子类)
 */
public abstract class BaseBroadcastActivity<T extends IDelegate, B extends BaseBroadcastFragment> extends RxBaseActivity<T> implements IBroadcastListener, IRegisterFragment<B> {
    protected ManagerRegisterFragment managerRegisterFragment = new ManagerRegisterFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBaseBoradcastReceiver();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(baseBroadcastReceiver);
        super.onDestroy();
    }

    /**
     * 注册广播
     */
    private void registerBaseBoradcastReceiver() {
        IntentFilter myIntentFilter = new IntentFilter();
        for (BaseBroadcastEntity baseBroadcastEntity : getBroadcastMarks()) {
            myIntentFilter.addAction(baseBroadcastEntity.getMark());
        }
        registerReceiver(baseBroadcastReceiver, myIntentFilter);
    }

    /**
     * 返回全局的所有广播类型
     *
     * @return
     */
    private List<BaseBroadcastEntity> getBroadcastMarks() {
        return BroadcastManager.getInstance().getBaseBroadcastEntities();
    }

    /**
     * 定义广播接收，进行分发
     */
    protected BroadcastReceiver baseBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            onReceiveBroadcast(action, intent);
            onReceiveBroadcastFragmentChilds(action, intent);
        }
    };


    @Override
    public void onReceiveBroadcast(String broadcast, Intent intent) {

    }

    @Override
    public void onReceiveBroadcastFragmentChilds(String broadcastMark, Intent intent) {
        if (getResisterFragment() != null) {
            for (BaseBroadcastFragment fragment : getResisterFragment()) {
                fragment.baseReceiveBroadcast(broadcastMark, intent);
            }
        }
    }

    @Override
    public void resisterFragment(List<B> registerFragment) {
        if (managerRegisterFragment != null) {
            managerRegisterFragment.resisterFragment(registerFragment);
        }
    }

    @Override
    public List<B> getResisterFragment() {
        if (managerRegisterFragment == null) {
            managerRegisterFragment = new ManagerRegisterFragment();
        }
        return managerRegisterFragment.getResisterFragment();
    }


}
