/*
 * Copyright (c) 2015, 张涛.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.wq.baseActivity.base;


import android.graphics.Color;

import cn.wq.baseActivity.R;

/**
 * View视图层，完全移除与Presenter业务逻辑的耦合
 */
public abstract class BaseShareViewDelegate extends BaseToolbarTwoButtonViewDelegate {


    @Override
    public void initWidget() {
        super.initWidget();
        setToolbarTitle("shareTitle");
        setToolbarTitleColor(Color.BLACK);
        setToolbarLeftButton(R.mipmap.base_back, "");
        setToolbarBackgroundColorRes(R.color.baseColorPrimaryDark);
        setStatusBarColorRes(R.color.baseColorPrimaryDark);

        setToolbarRightButton(R.mipmap.icon_share, "");

        setShowLeftButtonTwo(false);
//        setShowRightButtonTwo(false);

    }

}