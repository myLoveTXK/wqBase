package cn.wq.baseActivity.base.broadcast.manager;


import java.util.ArrayList;
import java.util.List;

import cn.wq.baseActivity.base.broadcast.BroadcastEntity.BaseBroadcastEntity;

/**
 * Created by Administrator on 2016/5/31.
 * 单例模式管理广播类
 */
public class BroadcastManager {
    private static BroadcastManager instance;
    private final List<BaseBroadcastEntity> baseBroadcastEntities = new ArrayList<>();

    public List<BaseBroadcastEntity> getBaseBroadcastEntities() {
        return baseBroadcastEntities;
    }

    private BroadcastManager() {
    }

    public static BroadcastManager getInstance() {
        if (instance == null) {
            synchronized (BroadcastManager.class) {
                if (instance == null) {
                    instance = new BroadcastManager();
                }
            }
        }
        return instance;
    }

    /**
     * 注册App所有广播
     *
     * @param baseBroadcastEntities
     */
    public void initAppAllBroadcast(List<BaseBroadcastEntity> baseBroadcastEntities) {
        if (baseBroadcastEntities == null) {
            return;
        }
        if (this.baseBroadcastEntities == null) {
            return;
        }
        this.baseBroadcastEntities.addAll(baseBroadcastEntities);
    }


    /**
     * 所有的发送广播体，全部再此管理
     */
    private class SendEntity {
        public final ArrayList<String> marks;//

        private SendEntity() {
            marks = new ArrayList<>();

        }

        private void addMark(String mark) {
            marks.add(mark);
        }
    }

}
