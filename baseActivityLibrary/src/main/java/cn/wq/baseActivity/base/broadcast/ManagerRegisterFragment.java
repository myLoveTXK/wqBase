package cn.wq.baseActivity.base.broadcast;

import java.util.ArrayList;
import java.util.List;

import cn.wq.baseActivity.base.interfaces.IRegisterFragment;

/**
 * Created by W~Q on 2017/3/16.
 * 管理当前注册的Fragment
 */

public class ManagerRegisterFragment<B extends BaseBroadcastFragment> implements IRegisterFragment<B> {
    ArrayList<B> thisRegisterFragments = new ArrayList<>();

    @Override
    public void resisterFragment(List<B> registerFragment) {
        if (registerFragment == null) {
            return;
        }
        getResisterFragment().clear();
        getResisterFragment().addAll(registerFragment);
    }

    @Override
    public List<B> getResisterFragment() {
        if (thisRegisterFragments == null) {
            thisRegisterFragments = new ArrayList<>();
        }
        return thisRegisterFragments;
    }
}
