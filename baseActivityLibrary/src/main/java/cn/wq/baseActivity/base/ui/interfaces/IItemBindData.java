package cn.wq.baseActivity.base.ui.interfaces;

import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;

/**
 * Created by xt on 2018/5/11.
 */

public interface IItemBindData<Data> {

     void onBindData(int viewType, BaseViewHolder viewHolder, Data data, final int position, int size);
}
