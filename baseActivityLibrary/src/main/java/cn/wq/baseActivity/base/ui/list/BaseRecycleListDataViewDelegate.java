package cn.wq.baseActivity.base.ui.list;


import java.util.List;

import cn.wq.baseActivity.base.ui.interfaces.IItemBindData;
import cn.wq.baseActivity.view.pullRecycleView.RecycleViewDataAdapter;

/**
 * Created by W~Q on 2017/3/20.
 * 绑定数据
 */
public abstract class BaseRecycleListDataViewDelegate<Data> extends BaseRecycleListViewDelegate implements IItemBindData<Data> {

    public Data getItem(int position) {
        return (Data) ((RecycleViewDataAdapter) getRecycler().getAdapter()).getItem(position);
    }

    public List<Data> getDatas(int position) {
        return ((RecycleViewDataAdapter) getRecycler().getAdapter()).getDataList();
    }
}
