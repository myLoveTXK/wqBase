package cn.wq.baseActivity.base.interfaces.list;

import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;

/**
 * Created by W~Q on 2017/3/28.
 * 绑定数据
 */

public interface IRecycleViewBind {
    void onBindViewHolderData(int viewType, BaseViewHolder baseViewHolder, int position);
}
