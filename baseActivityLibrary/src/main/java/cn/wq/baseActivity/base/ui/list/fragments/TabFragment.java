package cn.wq.baseActivity.base.ui.list.fragments;

import android.os.Bundle;
import android.text.TextUtils;

import java.util.Date;

import cn.wq.baseActivity.base.ui.interfaces.ITabFragment;
import cn.wq.baseActivity.base.ui.list.BaseRecycleListDataFragment;
import cn.wq.baseActivity.base.ui.list.BaseRecycleListDataViewDelegate;

/**
 * Created by W~Q on 2016/8/22.
 * ViewPager滑动使用Fragment   非首页底部形式
 */
public abstract class TabFragment<T extends BaseRecycleListDataViewDelegate<Data>, Data> extends BaseRecycleListDataFragment<T, Data> implements ITabFragment {

//    public abstract String getTitle();


    boolean isCreate = false;
    Date starShowDate;

    public void dplusStart() {
        if (TextUtils.isEmpty(onDplusBrowseEventName())) {
            return;
        }
        if (starShowDate == null) {
            starShowDate = new Date();
        }
    }

    public void dplusEnd() {
        if (TextUtils.isEmpty(onDplusBrowseEventName())) {
            return;
        }
        Date nowDate = new Date();
        if (starShowDate != null) {
            long time = (nowDate.getTime() - starShowDate.getTime()) / 1000;
            //统计使用到
//            DplusHelp.getInstance().awuStatisticsBrowseTime(onDplusBrowseEventName(), time);
            starShowDate = null;
        }
    }

    protected String onDplusBrowseEventName() {
        return "";
    }


    protected boolean isCreated = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isCreated = true;
        if (isVisibleToUser) {
            dplusStart();
//            showTab();
        }
    }

    private boolean isVisibleToUser;

    public boolean isVisibleToUser() {
        return isVisibleToUser;
    }

    /**
     * 此方法目前仅适用于标示ViewPager中的Fragment是否真实可见
     * For 友盟统计的页面线性不交叉统计需求
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        if (!isCreated) {
            return;
        }

        if (isVisibleToUser) {
            dplusStart();
//            showTab();
        } else {
            dplusEnd();
//            hideTab();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (isVisibleToUser) {
            dplusEnd();
        }
    }

}