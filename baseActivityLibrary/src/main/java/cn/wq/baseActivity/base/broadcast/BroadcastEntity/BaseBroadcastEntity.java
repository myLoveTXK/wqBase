package cn.wq.baseActivity.base.broadcast.BroadcastEntity;

import android.content.Context;
import android.content.Intent;

import com.lidroid.xutils.util.LogUtils;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/1.
 */
public abstract class BaseBroadcastEntity {
    private Intent intent;

    public abstract String getMark();

    protected abstract Context getContext();

    private Intent getIntent() {
        intent = new Intent();
        intent.setAction(getMark());
        return intent;
    }

    /**
     * 主要用到发送方法
     */
    public void send() {
        Intent intent = getIntent();
        sendBroadcast(intent);
    }

    /**
     * 主要用到发送方法
     */
    public void send(Intent intent) {
        if (intent == null) {
            intent = getIntent();
        } else {
            intent.setAction(getMark());
        }
        sendBroadcast(intent);
    }

    protected void sendBroadcast(Intent intent) {
        if (getContext() == null) {
            LogUtils.i("wq 0327 发送广播失败：当前context为空");
            return;
        }
        getContext().sendBroadcast(intent);
    }
}
