package cn.wq.baseActivity.base.interfaces;

import android.content.Intent;

/**
 * Created by Administrator on 2016/6/1.
 */
public interface IBroadcastListener {
    /**
     * 接受广播
     * @param broadcastMark 广播标识
     * @param intent        意图对象
     */
    void onReceiveBroadcast(String broadcastMark, Intent intent);

    /**
     * 分发广播给fragment
     * @param broadcastMark 广播标识
     * @param intent        图对象
     */
    void onReceiveBroadcastFragmentChilds(String broadcastMark, Intent intent);
}
