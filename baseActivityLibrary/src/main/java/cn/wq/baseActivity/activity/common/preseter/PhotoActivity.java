package cn.wq.baseActivity.activity.common.preseter;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.transition.Explode;
import android.view.View;
import android.view.Window;

import com.github.chrisbanes.photoview.PhotoView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.utils.MD5Util;

import java.io.File;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.activity.common.TransitionHelper;
import cn.wq.baseActivity.activity.common.view.PhotoViewDelegate;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.util.anim.ActivityAnimUtil;
import cn.wq.baseActivity.util.file.Files;
import cn.wq.baseActivity.view.progress.RoundProgressBar;


public class PhotoActivity extends BaseActivity<PhotoViewDelegate> implements View.OnClickListener {

    String path;
    File file;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // 允许使用transitions
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

// 设置一个exit transition
        getWindow().setExitTransition(new Explode());//new Slide()  new Fade()
        super.onCreate(savedInstanceState);

//        Bitmap bm = BitmapFactory.decodeFile(file.getPath());
//        ((PhotoView) getView(R.id.photo)).setImageBitmap(bm);
        setLocalPhone();
        setUrlPhone();
    }



    @Override
    public boolean isFullScreen() {
        return true;
    }

    @Override
    protected boolean isNeedChangeLayoutSuit() {
        return false;
    }

    /**
     * 设置url图片
     */
    private void setUrlPhone() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                download();
            }
        }, 600);

    }

    /**
     * \设置本地图片
     */
    private void setLocalPhone() {
        try {
            if (file != null) {
                Uri destinationUri = Uri.fromFile(file);
                if (destinationUri != null) {
                    ((PhotoView) getView(R.id.photo)).setImageURI(destinationUri);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (path != null) {
                Uri destinationUri = Uri.fromFile(new File(path));
                if (destinationUri != null) {
                    ((PhotoView) getView(R.id.photo)).setImageURI(destinationUri);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initData() {
        super.initData();
        path = getIntent().getStringExtra("path");
        url = getIntent().getStringExtra("url");
        file = (File) getIntent().getSerializableExtra("file");
    }

    @Override
    protected Class<PhotoViewDelegate> getDelegateClass() {
        return PhotoViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        ((PhotoView) getView(R.id.photo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.i("wq 0523 onClick:");
                finish();
                ActivityAnimUtil.finishActivityBigToSmall(This);
            }
        });
        //可以同时对多个控件设置同一个点击事件,后面id参数可以传多个
//        viewDelegate.setOnClickListener(this, R.id.text, R.id.button1, R.id.toast_test, R.id.toast_activity);

    }

    @Override
    public void onClick(View v) {
    }


    public static void startPhotoActivity(BaseActivity baseActivity, String path) {
        Intent intent = new Intent();
        intent.putExtra("path", path);
        baseActivity.startActivity(PhotoActivity.class, intent);
    }

    public static void startPhotoActivity(BaseActivity baseActivity, View view, File file, String url) {
//        //设置允许通过ActivityOptions.makeSceneTransitionAnimation发送或者接收Bundle
//        baseActivity.getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
//        //设置使用TransitionManager进行动画，不设置的话系统会使用一个默认的TransitionManager
//        baseActivity.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        Intent intent = new Intent(baseActivity, PhotoActivity.class);
        intent.putExtra("file", file);
        intent.putExtra("url", url);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

//            ActivityOptions option = ActivityOptions.makeSceneTransitionAnimation(baseActivity, view, "photo_image");
//            baseActivity.startActivity(intent, option.toBundle());
            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(baseActivity, false, new Pair<>(view,"photo_image"));
            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(baseActivity, pairs);
//            i.putExtra("sample", sample);
            baseActivity.startActivity(intent, transitionActivityOptions.toBundle());
        } else {
            baseActivity.startActivity(intent);
        }
    }


    protected void download() {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        HttpUtils httpUtils = new HttpUtils();
        final String apkurl = cn.wq.baseActivity.util.UrlUtil.getStandardUrl(url);
        String fileName = MD5Util.MD5(apkurl);
        final String filePath = Files.getTempPath() + "/" + fileName + ".jpg";
        if (new File(filePath).exists()) {
            setPhone(filePath);
        }
        httpUtils.download(apkurl, filePath, true, true,
                new RequestCallBack<File>() {

                    @Override
                    public void onStart() {
                        super.onStart();
                        LogUtils.i("wq 1011 onStart:" + apkurl);
                        ((RoundProgressBar) getView(R.id.phone_round_progrees)).setProgress(0);
//                        ((RoundProgressBar) getView(R.id.phone_round_progrees)).setVisibility(View.VISIBLE);
                    }

                    // 下载成功的时候调用该方法
                    @Override
                    public void onSuccess(ResponseInfo<File> responseInfo) {
                        try {
                            LogUtils.i("wq 1011 responseInfo.result.getPath():" + responseInfo.result.getPath());
                            String path = responseInfo.result.getPath();
                            setPhone(path);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    // 下载失败调用该方法
                    @Override
                    public void onFailure(HttpException e, String error) {
                        LogUtils.i("wq 1011 error:" + error);

                        if ("maybe the file has downloaded completely".equals(error)) {
                            ((RoundProgressBar) getView(R.id.phone_round_progrees)).setProgress((int) (100));
                            setPhone(filePath);
                        }

                    }

                    //正在下载时调用该方法
                    @Override
                    public void onLoading(long total, long current, boolean isUploading) {
                        //total 代表总大小  current 代表当前下载的进度
                        super.onLoading(total, current, isUploading);
                        double propress = (double) current / (double) total;
                        ((RoundProgressBar) getView(R.id.phone_round_progrees)).setVisibility(View.VISIBLE);
                        LogUtils.i("wq 1011 propress:" + propress);
                        ((RoundProgressBar) getView(R.id.phone_round_progrees)).setProgress((int) (propress * 100));

                    }
                });
    }

    private void setPhone(String path) {
        try {
            File file = new File(path);
            final Uri uri = Uri.fromFile(file);
            if (uri != null) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
                ((PhotoView) getView(R.id.photo)).setImageURI(uri);
                ((RoundProgressBar) getView(R.id.phone_round_progrees)).setVisibility(View.GONE);
//                    }
//                }, 100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
