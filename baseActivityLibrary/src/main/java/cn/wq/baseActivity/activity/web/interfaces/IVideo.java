package cn.wq.baseActivity.activity.web.interfaces;

import android.webkit.JavascriptInterface;

/**
 * Created by xt on 2018/5/14.
 */

public interface IVideo {

    @JavascriptInterface
    void appPlayVideo(String videosrc);

}
