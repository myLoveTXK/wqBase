package cn.wq.baseActivity.activity.common.view;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.BaseViewDelegate;

/**
 * Created by W~Q on 2017/5/2.
 */

public class PhotoViewDelegate extends BaseViewDelegate {

    @Override
    public int getLayoutID() {
        return R.layout.activity_common_photo;
    }

    @Override
    public void initWidget() {
        super.initWidget();

        setToolbarTitle("");
        setToolbarLeftButton(0, "");

        setToolbarBackgroundColorRes(R.color.transparent);
        setStatusBarColorRes(R.color.transparent);
    }

    @Override
    public int getContentBaseRelativeLayout() {
        return 0;
    }

}
