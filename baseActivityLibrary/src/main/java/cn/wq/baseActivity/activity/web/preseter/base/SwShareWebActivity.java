package cn.wq.baseActivity.activity.web.preseter.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.lidroid.xutils.util.LogUtils;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.activity.web.view.BaseShareWebViewDelegate;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.bean.ShareBean;
import cn.wq.baseActivity.util.show.ShowToast;
import wq.share.shareUtil.YouMengShareUtil;

public class SwShareWebActivity extends SwWebActivity<BaseShareWebViewDelegate> {

    protected static String shareBeanKey = "shareBeanKey";
    protected ShareBean shareBean;

    @Override
    public String statisticsPageName() {
        return "PageId_share";
    }

    @Override
    protected void initData() {
        super.initData();
        shareBean = (ShareBean) getIntent().getSerializableExtra(shareBeanKey);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateShareButton();
    }

    @Override
    protected Class getDelegateClass() {
        return BaseShareWebViewDelegate.class;
    }

    private void updateShareButton() {
        getViewDelegate().setToolbarRightButton(R.mipmap.icon_share, "");
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();

        getViewDelegate().setOnToolbarRightButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {
                share();
            }
        });
//        getViewDelegate().get(R.id.invite).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SwLoginActivity.startLoginActivity(This, "", new IActivityCallback() {
//                    @Override
//                    public void callback(int resultCode, Intent data) {
//                        if (resultCode == RESULT_OK) {
//                            startActivity(SwInviteListActivity.class);
//                        }
//                    }
//                });
//
//            }
//        });
    }

//    public boolean isShareOnlyWxQQ() {
//        return false;
//    }

    protected void share() {

        ShareBean shareBean = this.shareBean;
        if (shareBean != null) {
            //分享。
            int shareIcon = 0;
            if (shareBean.getShareIcon() != 0) {
                shareIcon = shareBean.getShareIcon();
            }
            if (shareIcon == 0) {
                ShowToast.show(this, "分享图片为空");
                return;
            }
            YouMengShareUtil.shareWeb(this, shareBean.getUrl(), shareIcon, shareBean.getTitle(), shareBean.getContent(), umShareListener);
        } else {
            ShowToast.show(SwShareWebActivity.this, "分享信息丢失，请稍后再试");
        }

    }

    UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onResult(SHARE_MEDIA share_media) {
            ShowToast.show(SwShareWebActivity.this, "分享成功");
        }

        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {
            if (throwable != null) {
                ShowToast.show(SwShareWebActivity.this, "分享失败," + throwable.getMessage());
            } else {
                ShowToast.show(SwShareWebActivity.this, "分享失败");
            }
            LogUtils.i("wq 0518 share_media:" + share_media.toString());
            LogUtils.i("wq 0518 throwable.getMessage():" + throwable.getMessage());
            throwable.printStackTrace();
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media) {
            ShowToast.show(SwShareWebActivity.this, "分享取消");
        }
    };

    public static void startActivity(final Activity baseActivity, final String title, final String url, final ShareBean shareBean, Class startActivityClass) {
        startActivity(baseActivity, title, url, shareBean, null, startActivityClass);
    }

    public static void startActivity(final Activity baseActivity, final String title, final String url, final ShareBean shareBean, Intent intent, Class startActivityClass) {
        if (intent == null) {
            intent = new Intent();
        }
        intent.putExtra(shareBeanKey, shareBean);
        startActivity(baseActivity, title, url, intent, startActivityClass);
    }

}
