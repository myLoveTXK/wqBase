package cn.wq.baseActivity.activity.web.preseter.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.BridgeWebViewClient;
import com.lidroid.xutils.util.LogUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.activity.web.view.WebViewDelegate;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.interfaces.IActivityCallback;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.util.image.IPictureSelectCallback;
import cn.wq.baseActivity.util.image.SelectImageUtil;


public abstract class SwWebActivity<V extends WebViewDelegate> extends BaseActivity<V> {

//    boolean isDebug = false;

    public static final String URL_KEY = "URL_KEY";
    public static final String TITLE_KEY = "TITLE_KEY";

    ProgressBar progressbar;
    //    JavaScript javaScript;
    private String toolbarTitle;
    protected String url;

    public String getToolbarTitle() {
        return toolbarTitle;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public boolean isWebActivity() {
        return true;
    }

//    @Override
//    protected Class<V> getDelegateClass() {
//        return V;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initConfig();
    }

    protected boolean isDebug() {
        return false;
    }


    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        getViewDelegate().setOnToolbarLeftButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {
                back();
            }
        });
        getViewDelegate().setOnToolbarLeftButtonClickListenerTwo(new IUiInterface.BaseToolbarTwoButtonInterface.OnToolbarButtonTwoClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        });
    }

    public void initConfig() {
        try {
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_PLAY_SOUND);
//            audioManager.setSt
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = getIntent();
        toolbarTitle = intent.getStringExtra(TITLE_KEY);
        url = intent.getStringExtra(URL_KEY);
        getViewDelegate().setToolbarTitle(toolbarTitle);
        progressbar = (ProgressBar) getView(R.id.web_progress_bar);


        getViewDelegate().webView.setWebViewClient(new SwWebViewClient(getViewDelegate().webView));
        getViewDelegate().webView.setWebChromeClient(new AndroidWebChromeClient());
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            load();
                        }
                    });

                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
            load();
        }
    }

    protected void load() {
        if (isDebug()) {
//            javaScript.JS_H5_VAR = "javascript:";
            url = "wqtest";
        }
        if ("wqtest".equals(url)) {
            String htmlText = getFromAssets(getAssetsDebugFileName());
            getViewDelegate().webView.loadData(htmlText, "text/html; charset=UTF-8", null);
        } else {
            LogUtils.i("wq 0511 加载url：" + url);
            getViewDelegate().webView.loadUrl(url);
        }
    }

    protected String getAssetsDebugFileName() {
        return "test.htm";
    }


    class SwWebViewClient extends BridgeWebViewClient {

        public SwWebViewClient(BridgeWebView webView) {
            super(webView);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            LogUtils.i("wq 0825 shouldOverrideUrlLoading url:" + url);
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
                return true;
            }
//            view.loadUrl(url);
//            return true;
            WebView.HitTestResult hit = getViewDelegate().webView.getHitTestResult();
            if (hit != null) {
                int hitType = hit.getType();
                if (hitType != WebView.HitTestResult.UNKNOWN_TYPE) {
                    //这里执行自定义的操作
//                    WebActivity.startActivity(This, title, url);
                    view.loadUrl(url);
                    checkToolbarTitleButton();
                    return true;
                } else {
                    //重定向时hitType为0 ,执行默认的操作
//                    view.loadUrl(url);
                    checkToolbarTitleButton();
                    return false;
                }
            } else {
//                WebActivity.startActivity(This, title, url);
                view.loadUrl(url);
                checkToolbarTitleButton();
                return true;
            }

        }

        // 该方法从API 21开始引入
//            @SuppressLint("NewApi")
//            @Override
//            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
//                if (request != null && request.getUrl() != null && request.getMethod().equalsIgnoreCase("get")) {
//                    String scheme = request.getUrl().getScheme().trim();
//                    String url = request.getUrl().toString();
//
//                    WebResourceResponse webResourceResponse = getDnsWebResourceResponse(scheme, url);
//                    if (webResourceResponse != null) return webResourceResponse;
//                }
//                return super.shouldInterceptRequest(view, request);
//            }

//            @Nullable
//            private WebResourceResponse getDnsWebResourceResponse(String scheme, String url) {
//                if ((scheme.equalsIgnoreCase("http") || scheme.equalsIgnoreCase("https")) && url.contains(".html") && url.contains("m.awu.cn")) {
//                    try {
//                        URL oldUrl = new URL(url);
//                        URLConnection connection = oldUrl.openConnection();
//                        // 异步获取域名解析结果
//                        HttpDnsService httpdns = HttpDns.getService(This, "102640");
//                        httpdns.setPreResolveHosts(new ArrayList<>(Arrays.asList("m.awu.cn", "live800.com")));
//                        String ip = httpdns.getIpByHostAsync(oldUrl.getHost());
//                        if (ip != null) {
//                            // 通过HTTPDNS获取IP成功，进行URL替换和HOST头设置
////                                Log.d(TAG, "Get IP: " + ip + " for host: " + oldUrl.getHost() + " from HTTPDNS successfully!");
//                            String newUrl = url.replaceFirst(oldUrl.getHost(), ip);
//                            connection = (HttpURLConnection) new URL(newUrl).openConnection();
//                            LogUtils.i("wq 0110 oldUrl.getHost():" + oldUrl.getHost());
//                            // 设置HTTP请求头Host域
//                            connection.setRequestProperty("Host", oldUrl.getHost());
////                                connection.setRequestProperty("Host", "m.awu.cn");
//                        }
////                            Log.d(TAG, "ContentType: " + connection.getContentType());
//                        return new WebResourceResponse("text/html", "UTF-8", connection.getInputStream());
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                return null;
//            }

        // 从API 11开始
//            @Override
//            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
//                if (!TextUtils.isEmpty(url) && Uri.parse(url).getScheme() != null) {
//                    String scheme = Uri.parse(url).getScheme().trim();
////                    Log.d(TAG, "url: " + url);
//                    WebResourceResponse webResourceResponse = getDnsWebResourceResponse(scheme, url);
//                    if (webResourceResponse != null) return webResourceResponse;
//                }
//                return super.shouldInterceptRequest(view, url);
//            }

    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            back();
//
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }


    @Override
    public boolean onBackKeyDown() {
        back();
        return true;
    }

    protected void back() {
        if (getViewDelegate().webView.canGoBack()) {
//            getViewDelegate().webView.
            getViewDelegate().webView.goBack();// 返回前一个页面
        } else {
            finish();
        }
    }


    public class AndroidWebChromeClient extends WebChromeClient {

        // 支持alert弹出
        @Override
        public boolean onJsAlert(WebView view, String url, String message,
                                 JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            return super.onJsConfirm(view, url, message, result);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            // if (progressbar == null) {
            // return;
            // }
            checkToolbarTitleButton();
            progressbar.setProgress(newProgress);
            if (newProgress == 100) {
                // WaitWindow.close();
                progressbar.setVisibility(View.GONE);

            } else {
//                if (progressbar.getVisibility() == View.GONE) {
//                    progressbar.setVisibility(View.VISIBLE);
//                }
                progressbar.setVisibility(View.VISIBLE);

            }
        }


        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            LogUtils.i("wq 1208 TITLE=" + title);
//            SwWebActivity.this.title = title;
//            getViewDelegate().setToolbarTitle(title);
        }


        /**
         * 使用照片，实现相册选择和
         *
         * @param uploadMsg
         */
        private void usePhones(final android.webkit.ValueCallback<Uri[]> uploadMsg) {
            if (uploadMsg == null) {
                return;
            }
            SelectImageUtil.selectPicture(This, true, new IPictureSelectCallback() {
                @Override
                public void success(String path) {
                    if (TextUtils.isEmpty(path)) {
                        uploadMsg.onReceiveValue(null);
                        return;
                    }
                    File file = new File(path);
                    Uri uri = Uri.fromFile(file);
                    Uri[] uris = new Uri[]{uri};
                    uploadMsg.onReceiveValue(uris);
                }

                @Override
                public void fail() {
                    uploadMsg.onReceiveValue(null);
                }
            });
        }

        /**
         * 使用照片，实现相册选择和
         *
         * @param uploadMsg
         */
        private void usePhone(final android.webkit.ValueCallback<Uri> uploadMsg) {
            if (uploadMsg == null) {
                return;
            }
            SelectImageUtil.selectPicture(This, true, new IPictureSelectCallback() {
                @Override
                public void success(String path) {
                    if (TextUtils.isEmpty(path)) {

                        uploadMsg.onReceiveValue(null);
                        return;
                    }
                    File file = new File(path);
                    Uri uri = Uri.fromFile(file);
//                    Uri[] uris = new Uri[]{uri};
                    uploadMsg.onReceiveValue(uri);
                }

                @Override
                public void fail() {
                    uploadMsg.onReceiveValue(null);
                }
            });
        }

        @Override
        public boolean onShowFileChooser(WebView webView, android.webkit.ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            usePhones(filePathCallback);
            return true;
        }

        // Android > 4.1.1 调用这个方法
        public void openFileChooser(android.webkit.ValueCallback<Uri> uploadMsg,
                                    String acceptType, String capture) {
//            mUploadMessage = uploadMsg;
            usePhone(uploadMsg);
        }


        // 3.0 + 调用这个方法
        public void openFileChooser(android.webkit.ValueCallback<Uri> uploadMsg, String acceptType) {
//            mUploadMessage = uploadMsg;
            usePhone(uploadMsg);
        }

        // Android < 3.0 调用这个方法
        public void openFileChooser(android.webkit.ValueCallback<Uri> uploadMsg) {
//            mUploadMessage = uploadMsg;
            usePhone(uploadMsg);
        }

    }

    /**
     * 总是需要第二个按钮占位
     *
     * @return
     */
    public boolean isAlwaysNeedPlaceholderButtonTwo() {
        return false;
    }

    private void checkToolbarTitleButton() {
        if (getViewDelegate().webView.canGoBack()) {
            getViewDelegate().setShowLeftButtonTwo(true);
            getViewDelegate().setShowPlaceholderButtonTwo(true);
        } else {
            getViewDelegate().setShowLeftButtonTwo(false);
            getViewDelegate().setShowPlaceholderButtonTwo(false);
//            getViewDelegate().setShowRightButtonTwo(false);
        }
        if (isAlwaysNeedPlaceholderButtonTwo()) {
            getViewDelegate().setShowPlaceholderButtonTwo(true);
        }
    }


//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if (javaScript != null) {
//                javaScript.onAppTopLeftBackListener();
//            }
//        }
//        return super.onKeyDown(keyCode, event);
//    }


    public String getFromAssets(String fileName) {
        try {
            InputStreamReader inputReader = new InputStreamReader(
                    getResources().getAssets().open(fileName));

            BufferedReader bufReader = new BufferedReader(inputReader);

            String line = "";
            String Result = "";
            while ((line = bufReader.readLine()) != null)
                Result += line;
            if (bufReader != null)
                bufReader.close();
            if (inputReader != null)
                inputReader.close();
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void startActivity(Activity baseActivity, String title, String url, Class startActivityClass) {
        startActivity(baseActivity, title, url, null, startActivityClass);
    }

    public static void startActivity(Activity baseActivity, String title, String url, Intent intent, Class startActivityClass) {
        if (url != null) {
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://" + url;
            }
        }
        if (title == null) {
            title = "";
        }
        if (intent == null) {
            intent = new Intent();
        }
        intent.setClass(baseActivity, startActivityClass);
        intent.putExtra(TITLE_KEY, title);
        intent.putExtra(URL_KEY, url);
        baseActivity.startActivity(intent);
    }

    public static void startActivityForResult(BaseActivity baseActivity, Class clas, String title, String url, Intent intent, IActivityCallback iActivityCallback) {
        if (url != null) {
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://" + url;
            }
        }
        if (title == null) {
            title = "";
        }
        if (intent == null) {
            intent = new Intent(baseActivity, SwWebActivity.class);
        }
        intent.putExtra(TITLE_KEY, title);
        intent.putExtra(URL_KEY, url);
        baseActivity.startActivityForResult(clas, intent, iActivityCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            getViewDelegate().webView.destroy();
            getViewDelegate().webView.removeAllViews();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
