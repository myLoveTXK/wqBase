package cn.wq.baseActivity.activity.common.preseter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.activity.common.view.PhotoViewPagerViewDelegate;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.view.PhotoViewPager;


public class PhotoViewPagerActivity extends BaseActivity<PhotoViewPagerViewDelegate> implements View.OnClickListener {
    ArrayList<String> urls;

    @Override
    protected void initData() {
        super.initData();

        urls = getIntent().getStringArrayListExtra("urls");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((PhotoViewPager) getView(R.id.photo_viewpager)).setUrls(urls);

    }

    @Override
    protected Class<PhotoViewPagerViewDelegate> getDelegateClass() {
        return PhotoViewPagerViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        getViewDelegate().setOnToolbarLeftButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        });
        //可以同时对多个控件设置同一个点击事件,后面id参数可以传多个
//        viewDelegate.setOnClickListener(this, R.id.text, R.id.button1, R.id.toast_test, R.id.toast_activity);

    }

    @Override
    public void onClick(View v) {
    }

    public static void startActivity(BaseActivity baseActivity, ArrayList<String> urls) {
        Intent intent = new Intent();
        intent.putStringArrayListExtra("urls", urls);
        baseActivity.startActivity(PhotoViewPagerActivity.class, intent);
    }

}
