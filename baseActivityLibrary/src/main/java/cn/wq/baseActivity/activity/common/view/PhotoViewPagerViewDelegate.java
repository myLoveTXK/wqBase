package cn.wq.baseActivity.activity.common.view;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.BaseViewDelegate;

/**
 * Created by W~Q on 2017/5/2.
 * View视图层，完全移除与Presenter业务逻辑的耦合
 */

public class PhotoViewPagerViewDelegate extends BaseViewDelegate {

    @Override
    public int getLayoutID() {
        return R.layout.activity_common_photo_viewpager;
    }

    @Override
    public void initWidget() {
        super.initWidget();

        setToolbarTitle("");
        setToolbarLeftButton(R.mipmap.base_back, "");
        setToolbarBackgroundColorRes(R.color.transparent);
        setStatusBarColorRes(R.color.transparent);
    }

    @Override
    public int getContentBaseRelativeLayout() {
        return 0;
    }

}
