package cn.wq.baseActivity.activity.web.interfaces;

/**
 * 商品详情接口
 */
public interface ShoppingDetailInterface {

//    public static String setSignUpButtonMethod = "setEnableSignUpButton";


    /**
     * 设置支付按钮状态 0报名结束  1正常报名
     */
    public static String setShoppingDetailPromptlyPayButtonMethod = "setShoppingDetailPromptlyPayButtonMethod";//0或1

    /**
     * 调用点击报名
     */
    void onClickSignUp(String name, String phone);




    /**
     * 立即支付
     */
    void onShoppingDetailPromptlyPay(String productId);
}
