package cn.wq.baseActivity.activity.web.javaScript;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.andview.refreshview.utils.LogUtils;

import cn.wq.baseActivity.activity.web.interfaces.AppInterface;
import cn.wq.baseActivity.activity.web.interfaces.IVideo;
import cn.wq.baseActivity.activity.web.interfaces.ShareInterface;


//import com.tencent.smtt.sdk.WebView;

//import com.tencent.smtt.geetest.sdk.WebView;

/**
 * Created by Administrator on 2016/9/20.
 */
public class JavaScript implements ShareInterface, AppInterface, IVideo {
    public final String JS_NAME = "appMethod";
    public static final String JS_H5_VAR = "javascript:";
    //    public String JS_H5_VAR = "javascript:";
    public int RECEIVE_FREE_VIP_REQUEST_CODE = 801;
    private Activity activity;
    private WebView webView;

    public Activity getActivity() {
        return activity;
    }

    public WebView getWebView() {
        return webView;
    }

    public JavaScript(Activity activity, WebView webView) {
        this.activity = activity;
        this.webView = webView;
    }

    @JavascriptInterface
    public void test() {
        LogUtils.i("wq 0603 test");

    }

    @Override
    @JavascriptInterface
    public void appShare(final String sharePlatformType, final String title, final String content, final String imgUrl, final String url) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                share(sharePlatformType, title, content, imgUrl, url);
            }
        });

    }


    @Override
    @JavascriptInterface
    public void onClickAppLink() {//打开移民app
//        YouMentUtil.statisticsEvent("EventId_RegistrationConsultation");
        String packageName = "cn.ym.shinyway";
        if (cn.wq.baseActivity.util.AppUtil.isAppInstalled(activity, packageName)) {
            cn.wq.baseActivity.util.AppUtil.startApp(activity, packageName);
        } else {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri content_url = Uri.parse("http://t.cn/ROIWrGo");
            intent.setData(content_url);
            activity.startActivity(intent);

//            SwWebActivity.startActivity((BaseActivity) activity, "移民投资", "http://t.cn/ROIWrGo");
        }
    }


    @Override
    @JavascriptInterface
    public void appPlayVideo(String videosrc) {
        if (activity instanceof IVideo) {
            ((IVideo) activity).appPlayVideo(videosrc);
        }
        LogUtils.i("wq 0514 videosrc:" + videosrc);
    }
}
