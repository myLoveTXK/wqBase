/*
 * Copyright (c) 2015, 张涛.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.wq.baseActivity.activity.web.view;

import android.content.Context;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.activity.web.javaScript.JavaScript;
import cn.wq.baseActivity.base.BaseToolbarTwoButtonViewDelegate;
import cn.wq.baseActivity.view.MyWebView;

/**
 * View视图层，完全移除与Presenter业务逻辑的耦合
 */
public class WebViewDelegate extends BaseToolbarTwoButtonViewDelegate {

    public MyWebView webView;

    private JavaScript javaScript;

    public MyWebView getWebView() {
        return webView;
    }

    protected JavaScript getJavaScript() {
        return new JavaScript(getActivity(), webView);
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_web_detail;
    }

    @Override
    protected int getBaseRootLayoutID() {
        return super.getBaseRootLayoutID();
    }

    @Override
    public void initWidget() {
        super.initWidget();
        setToolbarTitle("webTitle");
        setToolbarTitleColor(Color.BLACK);
        setToolbarLeftButton(R.mipmap.base_back, "");
        setToolbarBackgroundColorRes(R.color.baseColorPrimaryDark);
        setStatusBarColorRes(R.color.baseColorPrimaryDark);

        setToolbarLeftButtonTwo(0, "关闭");
        setShowLeftButtonTwo(false);
        setShowRightButtonTwo(false);

        initView();

    }

    public void initView() {
        try {
            AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_PLAY_SOUND);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0, AudioManager.FLAG_PLAY_SOUND);
//        setBaseLeftImg(R.mipmap.back);
//        showBaseLeftLayout(true);
//        setTitle(getIntent().getStringExtra(TITLE_KEY));

        webView = get(R.id.webview);
        ViewTreeObserver vto = get(R.id.webview).getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                webView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        webView.getSettings().setDomStorageEnabled(false);
//        webView.setWebChromeClient(new AndroidWebChromeClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setDatabaseEnabled(true);
        webSettings.setAppCacheEnabled(false);
//        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setSavePassword(false);
        // 设置可以支持缩放
        webView.setVerticalScrollBarEnabled(true);
        webSettings.setSupportZoom(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }
        webView.getSettings().setSupportZoom(false);

//        webSettings.setUserAgentString(webSettings.getUserAgentString() + " AWUAPP/" + AppUtil.getVersionName(This));
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setJavaScriptEnabled(true);
        javaScript = getJavaScript();
        webView.addJavascriptInterface(javaScript, javaScript.JS_NAME);


//        webView.setDefaultHandler(new DefaultHandler());
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//        webView.setWebViewClient(new MyWebViewClient(mJsBridgeWebView));
    }




}