package cn.wq.baseActivity.activity.web.interfaces;

import android.webkit.JavascriptInterface;

/**
 * Created by Administrator on 2016/9/20.
 */
public interface ShareInterface {
    @JavascriptInterface
    void appShare(String sharePlatformType, String title, String content, String imgUrl, String url);//分享
//    @JavascriptInterface
//    void appShareBox(String title,String content,String imgUrl,String url);//分享框

}
