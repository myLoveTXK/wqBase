package cn.wq.baseActivity.activity.common.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.utils.MD5Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.util.UrlUtil;
import cn.wq.baseActivity.util.file.Files;
import cn.wq.baseActivity.view.progress.RoundProgressBar;

/**
 * Created by xt on 2018/6/14.
 */

public class PhotoViewAdapter extends PagerAdapter {

    private List<String> list = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;

    public PhotoViewAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setList(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.item_viewpager, container, false);
        PhotoView photoView = view.findViewById(R.id.photo);
        RoundProgressBar roundProgressBar = view.findViewById(R.id.phone_round_progrees);
        download(list.get(position), roundProgressBar, photoView);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    protected void download(String url, final RoundProgressBar roundProgressBar, final PhotoView photoView) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        HttpUtils httpUtils = new HttpUtils();
        final String apkurl = UrlUtil.getStandardUrl(url);
        String fileName = MD5Util.MD5(apkurl);
        final String filePath = Files.getTempPath() + "/" + fileName + ".jpg";
//        if (new File(filePath).exists()) {
//            setPhoto(filePath);
//        }
        httpUtils.download(apkurl, filePath, true, true,
                new RequestCallBack<File>() {

                    @Override
                    public void onStart() {
                        super.onStart();
                        LogUtils.i("wq 1011 onStart:" + apkurl);
                        roundProgressBar.setProgress(0);
                        roundProgressBar.setVisibility(View.VISIBLE);
                    }

                    // 下载成功的时候调用该方法
                    @Override
                    public void onSuccess(ResponseInfo<File> responseInfo) {
                        try {
                            LogUtils.i("wq 1011 responseInfo.result.getPath():" + responseInfo.result.getPath());
                            String path = responseInfo.result.getPath();
                            setPhoto(path, photoView, roundProgressBar);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    // 下载失败调用该方法
                    @Override
                    public void onFailure(HttpException e, String error) {
                        LogUtils.i("wq 1011 error:" + error);

                        if ("maybe the file has downloaded completely".equals(error)) {
                            roundProgressBar.setProgress((int) (100));
                            setPhoto(filePath, photoView, roundProgressBar);
                        }

                    }

                    //正在下载时调用该方法
                    @Override
                    public void onLoading(long total, long current, boolean isUploading) {
                        //total 代表总大小  current 代表当前下载的进度
                        super.onLoading(total, current, isUploading);
                        double propress = (double) current / (double) total;
                        roundProgressBar.setVisibility(View.VISIBLE);
                        LogUtils.i("wq 1011 propress:" + propress);
                        roundProgressBar.setProgress((int) (propress * 100));

                    }
                });
    }

    private void setPhoto(String path, PhotoView photoView, final RoundProgressBar roundProgressBar) {
        try {
            File file = new File(path);
            final Uri uri = Uri.fromFile(file);
            if (uri != null) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
                photoView.setImageURI(uri);
                roundProgressBar.setVisibility(View.GONE);
//                    }
//                }, 100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
