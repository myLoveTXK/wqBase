package cn.wq.baseActivity;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.lidroid.xutils.util.LogUtils;

import cn.wq.baseActivity.base.interfaces.IAppOnForegroundListener;


/**
 * Created by W~Q on 2017/3/16.
 * 继承application
 */

public class BaseApplication extends MultiDexApplication implements IAppOnForegroundListener {
    protected static BaseApplication context;

    /**
     * 屏幕宽度
     */
    public static int screenWidth;

    /**
     * 屏幕高度
     */
    public static int screenHeight;
    /**
     * 屏幕密度
     */
    public static float screenDensity;
    /**
     * 字体缩放比例
     */
    public float scaledDensity;


    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        MultiDex.install(this);


        //facebook 的fresco 注册
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this, config);


//        LogUtils.i("wq 0405 NBS:");
//        NBSAppAgent.setLicenseKey("965720f685e044fb8a054c461e0fa907").withLocationServiceEnabled(true).start(this.getApplicationContext());
        getDisplayMetrics();
        initBaseAppAllBroadcast();
    }

    /**
     * 注册App所有广播
     * 2018 05 07 弃用此广播
     */
    protected void initBaseAppAllBroadcast() {
//        BroadcastManager.getInstance().initAppAllBroadcast(getBaseAppAllBroadcastEntity());
    }

    /**
     * 获取app所有广播
     * 需要添加的广播在此方法注册
     */
//    protected abstract List<BaseBroadcastEntity> getBaseAppAllBroadcastEntity();
    public static BaseApplication getInstance() {
        return context;
    }

    /**
     * 获取高度、宽度、密度、缩放比例
     */
    private void getDisplayMetrics() {
        DisplayMetrics metric = getApplicationContext().getResources().getDisplayMetrics();
        screenWidth = metric.widthPixels;
        screenHeight = metric.heightPixels;
        screenDensity = metric.density;
        scaledDensity = metric.scaledDensity;
        LogUtils.i("wq 0817 查看 application 宽度：" + screenWidth);
        LogUtils.i("wq 0817 查看 application 高度：" + screenHeight);
        LogUtils.i("wq 0817 查看 application screenDensity：" + screenDensity);
        LogUtils.i("wq 0817 查看 application scaledDensity：" + scaledDensity);
    }

    @Override
    public void appOnForeground() {
        LogUtils.i("wq 0823 当前application监听到app退出到后台：" + getClass().getSimpleName());
    }

}
