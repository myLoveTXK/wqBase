package cn.wq.baseActivity.view.pullRecycleView;

import android.view.ViewGroup;

import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import cn.wq.baseActivity.base.interfaces.list.IRecycleViewBind;
import cn.wq.baseActivity.base.interfaces.list.IRecycleViewViewHolder;
import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;
import cn.wq.baseActivity.view.pullRecycleView.base.SwBaseRecyclerViewAdapter;

/**
 * Created by W~Q on 2017/3/21.
 * 封装基类数据
 */

public class RecycleViewDataAdapter<Data> extends SwBaseRecyclerViewAdapter {
    public boolean isSectionHeader = false;
    private IRecycleViewBind iRecycleViewBind;

    private int count = 0;

    public void setCount(int count) {
        this.count = count;
        notifyDataSetChanged();
    }

    IRecycleViewViewHolder iRecycleViewViewHolder;
    protected ArrayList<Data> mDataList = new ArrayList<>();

    public ArrayList<Data> getDataList() {
        return mDataList;
    }

    public void addData(Data data) {
        if (data != null) {
            mDataList.add(data);
            notifyItemChanged(mDataList.size() - 1);
        }
    }

    public void removeData(int position) {
        mDataList.remove(position);
        notifyItemRemoved(position);
        if (position != mDataList.size()) {
            notifyItemRangeChanged(0, mDataList.size());
        }
//        notifyDataSetChanged();
//        notifyItemChanged(position + getDataCount());
    }


    public void setiRecycleViewBind(IRecycleViewBind iRecycleViewBind) {
        this.iRecycleViewBind = iRecycleViewBind;
    }

    public void addData(List<Data> datas) {
        if (datas != null && datas.size() > 0) {
            int index = mDataList.size();
            mDataList.addAll(datas);
            notifyItemChanged(index, mDataList.size() - 1);
        }
        notifyDataSetChanged();
    }


    public void clear() {
        LogUtils.i("wq 1204 clear");
        mDataList.clear();
        notifyDataSetChanged();
    }

    /**
     * 获取某一条数据
     *
     * @param position
     * @return
     */
    public Data getItem(int position) {
        if (mDataList == null) {
            return null;
        }
        return mDataList.get(position);
    }

    public RecycleViewDataAdapter(IRecycleViewViewHolder iRecycleViewViewHolder) {
        this.iRecycleViewViewHolder = iRecycleViewViewHolder;
    }


    public void setSectionHeader(boolean sectionHeader) {
        isSectionHeader = sectionHeader;
    }

    @Override
    protected BaseViewHolder onCreateNormalViewHolder(ViewGroup parent, int viewType) {
        return getViewHolder(parent, viewType);
    }

    @Override
    protected int getDataCount() {
        return (mDataList != null ? mDataList.size() : 0) + count;
    }

    @Override
    protected int getDataViewType(int position) {
        return getItemType(position);
    }

//    @Override
//    public boolean isSectionHeader(int position) {
//        return isSectionHeader;
//    }

    public int getItemType(int position) {
        return 0;
    }

    public BaseViewHolder getViewHolder(ViewGroup parent, int viewType) {
        if (iRecycleViewViewHolder == null) {
            return null;
        }
        return iRecycleViewViewHolder.getViewHolder(parent, viewType, iRecycleViewBind);
    }

}