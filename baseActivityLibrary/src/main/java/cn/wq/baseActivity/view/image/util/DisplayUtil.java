package cn.wq.baseActivity.view.image.util;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import cn.wq.baseActivity.BaseApplication;


/**
 * Created by Victor on 2015-8-30.
 */
public class DisplayUtil {

    public static int designImageWidth = 750;//设计图的宽度

    /**
     * 获取实际显示在屏幕的宽高对象
     *
     * @param width  设计图中图片的宽
     * @param height 设计图中图片的高
     * @return
     */
    public static ScreenBean getScreenBean(double width, double height) {
        ScreenBean bean = new ScreenBean();
        if (width == 0 || height == 0) {
            bean.setWidth(0);
            bean.setHeight(0);
        } else {
            double douebleWidth = BaseApplication.screenWidth * (width / designImageWidth);
            bean.setWidth((int) (douebleWidth + 0.5f));
            double doubleHeight = douebleWidth / width * height;
            bean.setHeight((int) (doubleHeight + 0.5f));
        }
        return bean;
    }

//    public static int getDesignHeight(String widthStr, String heightStr, int defaultHeight) {
//        double width = JsonBeanUtil.getDouble(widthStr, 0);
//        double height = JsonBeanUtil.getDouble(heightStr, 0);
//
////        ScreenBean bean = new ScreenBean();
//        if (width == 0 || height == 0) {
//            return defaultHeight;
//        } else {
//            double douebleHeight = BaseApplication.screenWidth * height / width;
//            return (int) (douebleHeight + 0.5f);
//        }
//    }

    /**
     * 获取手机屏幕显示的真实长度
     *
     * @param designLength
     * @return
     */
    public static int getScreenRealLength(double designLength) {
        double realLength = BaseApplication.screenWidth * (designLength / designImageWidth);
        return ((int) (realLength + 0.5f));
    }

    /**
     *
     * @return
     */
//    public static int getImageScreenWidthLength(){
//
//    }


    /**
     * 将px值转换为dip或dp值，保证尺寸大小不变
     *
     * @param pxValue
     * @param density （DisplayMetrics类中属性density）
     * @return
     */

    public static int px2dip(float pxValue, float density) {
        return (int) (pxValue / density + 0.5f);
    }

    /**
     * 将dip或dp值转换为px值，保证尺寸大小不变
     *
     * @param dipValue
     * @param density  （DisplayMetrics类中属性density）
     * @return
     */

    public static int dip2px(float dipValue, float density) {
        return (int) (dipValue * density + 0.5f);
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     *
     * @param pxValue
     * @param fontScale （DisplayMetrics类中属性scaledDensity）
     * @return
     */

    public static int px2sp(float pxValue, float fontScale) {
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     *
     * @param spValue
     * @param fontScale （DisplayMetrics类中属性scaledDensity）
     * @return
     */

    public static int sp2px(float spValue, float fontScale) {
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 改变字符串中个别字体大小
     *
     * @param text
     * @param textSize 要改变的字体大小（sp）
     * @param isDip    字体单位是否是dip
     * @param start    开始位置
     * @param end      结束位置 （前包后不包）
     * @return
     */
    public static SpannableString changeTextSize(String text, int textSize, boolean isDip, int start, int end) {
        SpannableString sp = new SpannableString(text);
        sp.setSpan(new AbsoluteSizeSpan(textSize, isDip),
                start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return sp;
    }

    /**
     * 改变字符串中个别字体加粗
     *
     * @param text
     * @param start 开始位置
     * @param end   结束位置 （前包后不包）
     * @return
     */
    public static SpannableString changeTextBold(String text, int start, int end) {
        SpannableString sp = new SpannableString(text);
        sp.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        return sp;
    }

    /**
     * 根据ListView的子项目重新计算ListView的高度，然后把高度再作为LayoutParams设置给ListView
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


    public static int dip2px(final Context context, final float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);

    }

}
