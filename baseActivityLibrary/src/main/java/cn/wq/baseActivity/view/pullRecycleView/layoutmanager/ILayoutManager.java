package cn.wq.baseActivity.view.pullRecycleView.layoutmanager;

import android.support.v7.widget.RecyclerView;


/**
 * Created by W~Q on 2017/03/21.
 *
 */
public interface ILayoutManager {
    RecyclerView.LayoutManager getLayoutManager();
    int findLastVisiblePosition();
//    void setUpAdapter(SwBaseRecyclerViewAdapter adapter);
}
