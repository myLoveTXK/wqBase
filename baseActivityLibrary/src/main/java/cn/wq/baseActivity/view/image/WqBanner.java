package cn.wq.baseActivity.view.image;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.flyco.banner.widget.Banner.base.BaseBanner;
import com.flyco.pageindicator.indicator.FlycoPageIndicaor;
import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.wq.baseActivity.R;
import cn.wq.baseActivity.R2;
import cn.wq.baseActivity.view.MyLeftRightOnTouchView;
import cn.wq.baseActivity.view.image.util.DisplayUtil;

public class WqBanner extends FrameLayout {
    List<? extends IWqBannerBean> wqBannerBeanList;

    OnItemClick onItemClick;

    @BindView(R2.id.banner)
    ImageBanner imageBanner;
    @BindView(R2.id.indicator_square_stroke)
    FlycoPageIndicaor indicator;
    @BindView(R2.id.one_image)
    WqImageView oneImage;
    @BindView(R2.id.myLeftRightOnTouchView)
    MyLeftRightOnTouchView myLeftRightOnTouchView;


    public FlycoPageIndicaor getIndicator() {
        return indicator;
    }

    public ImageBanner getImageBanner() {
        return imageBanner;
    }

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public WqBanner(@NonNull Context context) {
        super(context);
        initView();
    }

    public WqBanner(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    void initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.wq_view_banner, null, false);
        addView(view);
        ButterKnife.bind(this);
    }


    public void setWqBannerBeanList(List<? extends IWqBannerBean> wqBannerBeanList) {
        this.wqBannerBeanList = wqBannerBeanList;


        imageBanner.setImgDefault(0);
        if (wqBannerBeanList == null) {
            return;
        }

        List<String> imgs = new ArrayList<>();
        for (IWqBannerBean bean : wqBannerBeanList) {
            if (bean != null) {
                imgs.add(bean.getBannerImgUrl());
            }
        }
        imageBanner.setSource(imgs).startScroll();
//                    indicator.
//                    indicator.removeAllViews();
        if (indicator.getTag() == null) {
            indicator.setTag(imgs.size());
            indicator.setViewPager(imageBanner.getViewPager(), imgs.size());
        }

        if (indicator.getTag() != null && ((int) indicator.getTag()) != imgs.size()) {
            indicator.setViewPager(imageBanner.getViewPager(), imgs.size());
        }

        imageBanner.setTag(wqBannerBeanList);
        imageBanner.setOnItemClickL(new BaseBanner.OnItemClickL() {
            @Override
            public void onItemClick(int position) {
                if (onItemClick != null) {
                    try {
                        onItemClick.onItemClick(wqBannerBeanList, wqBannerBeanList.get(position));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        imageBanner.getViewPager().setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                LogUtils.i("wq 1205 position:" + position);
                try {
                    indicator.setCurrentItem(position);
                    indicator.onPageSelected(position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    /**
     * 数据对象，实现此接口即可
     */
    public interface IWqBannerBean {
        String getBannerImgUrl();
    }

    public void setDesignWidth(double designWidth) {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = DisplayUtil.getScreenRealLength(designWidth);
    }

    public void setDesignHeight(double designHeight) {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = DisplayUtil.getScreenRealLength(designHeight);
    }


    public interface OnItemClick {
        public void onItemClick(List<? extends IWqBannerBean> been, IWqBannerBean bean);
    }
}
