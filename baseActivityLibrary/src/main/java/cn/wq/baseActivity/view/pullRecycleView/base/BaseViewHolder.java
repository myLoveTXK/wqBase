package cn.wq.baseActivity.view.pullRecycleView.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import cn.wq.baseActivity.base.interfaces.list.IRecycleViewBind;

/**
 * Created by Stay on 1/3/16.
 * Powered by www.stay4it.com
 */
public class BaseViewHolder extends RecyclerView.ViewHolder {
    SwBaseRecyclerViewAdapter.SldingViewHolder baseParentViewHolder;
    IRecycleViewBind iRecycleViewBind;


    public BaseViewHolder(View itemView, IRecycleViewBind iRecycleViewBind) {
        super(itemView);
        itemView.setTag(this);
        this.iRecycleViewBind = iRecycleViewBind;
    }

    public void onBindViewHolder(int viewType, int position) {
        if (iRecycleViewBind != null) {
            iRecycleViewBind.onBindViewHolderData(viewType, this, position);
        }
    }


    public SwBaseRecyclerViewAdapter.SldingViewHolder getBaseParentViewHolder() {
        return baseParentViewHolder;
    }

    public void setBaseParentViewHolder(SwBaseRecyclerViewAdapter.SldingViewHolder baseParentViewHolder) {
        this.baseParentViewHolder = baseParentViewHolder;
    }
//    public abstract void onItemClick(View view, int position);
}
