package cn.wq.baseActivity.view.pullRecycleView;

import android.support.v7.widget.GridLayoutManager;

import cn.wq.baseActivity.view.pullRecycleView.base.SwBaseRecyclerViewAdapter;

/**
 * Created by W~Q on 2017/03/21.
 *
 */
public class FooterSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {
    private SwBaseRecyclerViewAdapter adapter;
    private int spanCount;

    public FooterSpanSizeLookup(SwBaseRecyclerViewAdapter adapter, int spanCount) {
        this.adapter = adapter;
        this.spanCount = spanCount;
    }

    @Override
    public int getSpanSize(int position) {
//        if (adapter.isLoadMoreFooter(position) || adapter.isSectionHeader(position)) {
//            return spanCount;
//        }
        return 1;
    }
}
