package cn.wq.baseActivity.view.pullRecycleView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import cn.wq.baseActivity.view.pullRecycleView.base.SwBaseRecyclerViewAdapter;
import cn.wq.baseActivity.view.pullRecycleView.layoutmanager.ILayoutManager;


/**
 * Created by W~Q on 2017/03/21.
 */
public class PullRecycler extends RecyclerView {

    public PullRecycler(Context context) {
        super(context);
        setUpView();
    }

    public PullRecycler(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpView();
    }

    public PullRecycler(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUpView();
    }

    private void setUpView() {
    }

    public void setLayoutManager(ILayoutManager manager) {
//        this.mLayoutManager = manager;
        setLayoutManager(manager.getLayoutManager());
    }

    @Override
    public void addItemDecoration(RecyclerView.ItemDecoration decoration) {
        if (decoration != null) {
            super.addItemDecoration(decoration);
        }
    }

    public void setAdapter(SwBaseRecyclerViewAdapter adapter) {
//        this.adapter = adapter;
        super.setAdapter(adapter);
//        mLayoutManager.setUpAdapter(adapter);
    }

    public void setSelection(int position) {
        scrollToPosition(position);
    }


    public interface OnRecyclerRefreshListener {
        void onRefresh(int action);
    }

    public RecyclerView getmRecyclerView() {
        return this;
    }

//    // 滑动距离及坐标
//    private float xDistance, yDistance, xLast, yLast;
//
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        switch (ev.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                xDistance = yDistance = 0f;
//                xLast = ev.getX();
//                yLast = ev.getY();
//                break;
//            case MotionEvent.ACTION_MOVE:
//                final float curX = ev.getX();
//                final float curY = ev.getY();
//
//                xDistance += Math.abs(curX - xLast);
//                yDistance += Math.abs(curY - yLast);
//                xLast = curX;
//                yLast = curY;
//
//                if (xDistance > yDistance) {
//                    return false;   //表示向下传递事件
//                }
//        }
//
//        return super.onInterceptTouchEvent(ev);
//    }

}
