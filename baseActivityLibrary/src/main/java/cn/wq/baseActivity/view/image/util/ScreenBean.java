package cn.wq.baseActivity.view.image.util;

/**
 * Created by W~Q on 2016/8/18.
 * 保存实际显示在屏幕上的宽高
 */
public class ScreenBean {
    private int width;
    private int height;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "ScreenBean{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
