package cn.wq.baseActivity.view.edit;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.view.image.util.DisplayUtil;


/**
 * Created by Administrator on 2016/11/15.
 */
public class ClearEditText extends AppCompatEditText implements View.OnFocusChangeListener, TextWatcher {

    boolean isNeedClearDrawable = true;
    private Drawable mClearDrawable;
    private boolean hasFocus;
    Context context;
    OnFocusChangeListenerSw onFocusChangeListenerSw;

    public interface OnFocusChangeListenerSw {
        void onFocusChange(View v, boolean hasFocus);
    }

    public void setOnFocusChangeListenerSw(OnFocusChangeListenerSw onFocusChangeListenerSw) {
        this.onFocusChangeListenerSw = onFocusChangeListenerSw;
    }

    public void setNeedClearDrawable(boolean needClearDrawable) {
        isNeedClearDrawable = needClearDrawable;
    }

    public ClearEditText(Context context) {
        this(context, null);
    }

    public ClearEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ClearEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }


    protected int getEditDeleteImgRes() {
        return R.mipmap.edit_delete;
    }

    protected void init(Context context) {

        setBackgroundDrawable(null);
        setSingleLine();
        setTextColor(Color.parseColor("#291C01"));
        setHintTextColor(Color.parseColor("#999999"));


        mClearDrawable = getCompoundDrawables()[2]; // 获取drawableRight
        if (mClearDrawable == null) {
            // 如果为空，即没有设置drawableRight，则使用R.mipmap.close这张图片
            mClearDrawable = getResources().getDrawable(getEditDeleteImgRes());
        }
        mClearDrawable.setBounds(0, 0, mClearDrawable.getIntrinsicWidth(), mClearDrawable.getIntrinsicHeight());
        setOnFocusChangeListener(this);
        addTextChangedListener(this);
        // 默认隐藏图标
        setDrawableVisible(false);
        clearFocus();
        setGravity(Gravity.CENTER_VERTICAL);
//        setOnEditorActionListener();
    }

    /**
     * 我们无法直接给EditText设置点击事件，只能通过按下的位置来模拟clear点击事件
     * 当我们按下的位置在图标包括图标到控件右边的间距范围内均算有效
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (getCompoundDrawables()[2] != null) {
                int start = getWidth() - getTotalPaddingRight() + getPaddingRight(); // 起始位置
                int end = getWidth(); // 结束位置
                boolean available = (event.getX() > start) && (event.getX() < end);
                if (available) {
                    this.setText("");
                }
            }
        }
        return super.onTouchEvent(event);
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        this.hasFocus = hasFocus;
        if (onFocusChangeListenerSw != null) {
            onFocusChangeListenerSw.onFocusChange(v, hasFocus);
        }
        if (hasFocus && getText().length() > 0) {
            setDrawableVisible(true); // 有焦点且有文字时显示图标
        } else {
            setDrawableVisible(false);
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int count, int after) {
        if (hasFocus) {
            setDrawableVisible(s.length() > 0);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    int length30 = DisplayUtil.getScreenRealLength(36);
    int length16 = DisplayUtil.getScreenRealLength(16);

    protected void setDrawableVisible(boolean visible) {
        Drawable right = (isNeedClearDrawable && visible) ? mClearDrawable : null;

        setCompoundDrawablesWithIntrinsicBounds(getCompoundDrawables()[0], getCompoundDrawables()[1], right, getCompoundDrawables()[3]);
//        getpa
        setPadding(getPaddingLeft() /*== 0 ? length16 : getPaddingLeft()*/, getPaddingTop(), length30, getPaddingBottom());
        setCompoundDrawablePadding(length30);
    }

}