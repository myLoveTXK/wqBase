package cn.wq.baseActivity.view.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.binaryresource.FileBinaryResource;
import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.lidroid.xutils.util.LogUtils;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import cn.wq.baseActivity.BaseApplication;
import cn.wq.baseActivity.R;
import cn.wq.baseActivity.view.image.util.DisplayUtil;
import cn.wq.baseActivity.view.image.util.ScreenBean;


/**
 * Created by W~Q on 2017/04/14.
 * 封装一层图片处理类
 */
public class WqImageView extends FrameLayout {

    private Context context;
    private SimpleDraweeView simpleDraweeView;
    private ImageView backgroundImage;
    private ProgressBar progressBar;
    private TextView progressBarText;
    private TextView errorText;
    private View mask;

    String imgUrl;

    public TextView getErrorText() {
        return errorText;
    }

    public WqImageView(Context context) {
        super(context);
        init(context);
    }

    public WqImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public WqImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WqImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public SimpleDraweeView getSimpleDraweeView() {
        return simpleDraweeView;
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        simpleDraweeView.setScaleType(scaleType);
    }

    public void setRoundAsCircle(boolean isCircle) {
//        simpleDraweeView.setroundAsCircle(isCircle);
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
//        roundingParams.setBorder(Color.parseColor("#dfdfdf"), 1.0f);
        roundingParams.setRoundAsCircle(isCircle);
        simpleDraweeView.getHierarchy().setRoundingParams(roundingParams);
//        simpleDraweeView.getHierarchy().setimage
    }

    /**
     * 设置圆角
     *
     * @param radius
     */
    public void setCornersRadius(float radius) {
//        simpleDraweeView.setroundAsCircle(isCircle);
        GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
        RoundingParams roundingParams = new RoundingParams();
        roundingParams.setCornersRadius(radius);
        hierarchy.setRoundingParams(roundingParams);
    }

    public void setBorder(float border) {
//        RoundingParams roundingParams = RoundingParams.fromCornersRadius(border);
        RoundingParams roundingParams = simpleDraweeView.getHierarchy().getRoundingParams();
        if (roundingParams == null) {
            roundingParams = RoundingParams.fromCornersRadius(border);
        }
        roundingParams.setBorder(context.getResources().getColor(R.color.white), 50);
//        roundingParams.setOverlayColor(R.color.white);
//        roundingParams.setb
//        roundingParams.setBorder(R.color.white, border);
        simpleDraweeView.getHierarchy().setRoundingParams(roundingParams);
//        simpleDraweeView.setScaleType(ImageView.ScaleType.FIT_XY);

    }

    public void setImageURI(Uri uri) {
//        Uri uri = Uri.parse("res://包名(实际可以是任何字符串甚至留空)/" + R.drawable.ic_launcher);
        simpleDraweeView.setImageURI(uri);
    }

    public void setImageResource(int resource) {
//        Uri uri = Uri.parse("res://包名(实际可以是任何字符串甚至留空)/" + R.drawable.ic_launcher);
        simpleDraweeView.setImageResource(resource);
    }

    private void init(Context context) {
        setBackgroundColor(context.getResources().getColor(R.color.transparent));
        this.context = context;
        simpleDraweeView = new SimpleDraweeView(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            simpleDraweeView.setTransitionName("photo_image");
        }
//        progressBar = new ProgressBar(context);
        progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
        mask = new View(context);

        mask.setBackgroundColor(Color.argb(150, 0, 0, 0));

        backgroundImage = new ImageView(context);
        backgroundImage.setScaleType(ImageView.ScaleType.CENTER);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        backgroundImage.setLayoutParams(layoutParams);
        mask.setLayoutParams(layoutParams);

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(30, 0, 30, 0);
        params.height = 40;
        params.gravity = Gravity.CENTER_VERTICAL;
        progressBar.setLayoutParams(params);
        progressBar.setProgress(30);
        progressBar.setMax(100);
        progressBar.setBackgroundDrawable(null);
        progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_bg));

        progressBarText = new TextView(context);
        progressBarText.setTextSize(10);
        progressBarText.setText("50%");
        progressBarText.setTextColor(Color.WHITE);
        LayoutParams paramsText = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        paramsText.gravity = Gravity.CENTER;
        progressBarText.setLayoutParams(paramsText);

        errorText = new TextView(context);
        errorText.setTextSize(10);
        errorText.setTextColor(Color.WHITE);
        errorText.setBackgroundResource(R.drawable.rounded_corners_big_291c01);
        errorText.setPadding(20, 8, 20, 8);
        errorText.setLayoutParams(paramsText);
        errorText.setVisibility(GONE);
        errorText.setText("重新上传");

        addView(simpleDraweeView);
        addView(backgroundImage);
        addView(mask);
        addView(progressBar);
        addView(progressBarText);
        addView(errorText);

        setNeedProgress(false);
    }

    public void setNeedProgress(boolean show) {
        if (show) {
            mask.setVisibility(VISIBLE);
            progressBar.setVisibility(VISIBLE);
            progressBarText.setVisibility(VISIBLE);
        } else {
            mask.setVisibility(GONE);
            progressBar.setVisibility(GONE);
            progressBarText.setVisibility(GONE);
        }
        errorText.setVisibility(GONE);
    }

    public void setNeedError(boolean show) {
        if (show) {
            mask.setVisibility(VISIBLE);
            errorText.setVisibility(VISIBLE);

        } else {
            mask.setVisibility(GONE);
            errorText.setVisibility(GONE);
        }

        progressBar.setVisibility(GONE);
        progressBarText.setVisibility(GONE);
    }

    /**
     * 0-100
     */
    public void setProgress(int progress) {
        progressBar.setProgress(progress);
        progressBarText.setText(progress + "%");
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 设置图片（根据设计图的高宽）
     *
     * @param url
     * @param designImgWidth
     * @param designImgHeight
     */
    public void setDesignImage(String url, int designImgWidth, int designImgHeight) {
        setDesignImage(url, designImgWidth, designImgHeight, true);
    }

    /**
     * 设置图片（根据设计图的高宽）
     *
     * @param url
     * @param designImgWidth
     * @param designImgHeight
     * @param isNeedDefaultBackground
     */
    public void setDesignImage(String url, int designImgWidth, int designImgHeight, boolean isNeedDefaultBackground) {
        ScreenBean screenBean = DisplayUtil.getScreenBean(designImgWidth, designImgHeight);
        setImage(url, screenBean.getWidth(), screenBean.getHeight(), isNeedDefaultBackground);
    }

    /**
     * @param url    图片url
     * @param width  显示宽度
     * @param height 显示高度
     */
    private void setImage(String url, int width, int height) {
        setImage(url, width, height, true);
    }

    /**
     * 加载图片 , 动态设置默认展位图
     *
     * @return
     */
    public String getImgUrl() {
        return imgUrl;
    }

    public void setDesignImage(String url, int designImgWidth, int designImgHeight, int defaultImageResource) {
        ScreenBean screenBean = DisplayUtil.getScreenBean(designImgWidth, designImgHeight);
        setImage(url, screenBean.getWidth(), screenBean.getHeight(), defaultImageResource);
    }

    /**
     * 设置默认图片
     *
     * @param defaultImageResource
     */
    public void setDefaultImageResource(int defaultImageResource) {
        GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
        hierarchy.setPlaceholderImage(defaultImageResource);
    }

    /**
     * 加载图片 , 动态设置默认展位图
     *
     * @param url
     * @param width
     * @param height
     * @param defaultImageResource 默认底图
     */
    private void setImage(String url, int width, int height, int defaultImageResource) {
        url = getUrl(url);
        imgUrl = url;
        LogUtils.i("wq 0414 加载图片url：" + url);
        try {
            setWidthHeight(width, height);
//            GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
//            hierarchy.setPlaceholderImage(defaultImageResource);

//                int resource = ImageUtil.getDefaultBackgroundImage(width);
            backgroundImage.setImageResource(defaultImageResource);
            backgroundImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

            Uri uri = null;

            if (!TextUtils.isEmpty(url)) {
                uri = Uri.parse(url);
                simpleDraweeView.setVisibility(VISIBLE);
            } else {
                simpleDraweeView.setVisibility(INVISIBLE);
                return;
            }
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setProgressiveRenderingEnabled(true)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()

                    .setImageRequest(request)
                    .setControllerListener(new AwuBaseControllerListener(uri, url, defaultImageResource))
                    .setOldController(simpleDraweeView.getController())
                    .build();
            simpleDraweeView.setController(controller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void setPlaceholderImage() {
//        GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(context.getResources());
//        builder.setPlaceholderImage(R.mipmap.ic_launcher);
//    }

    static int defaultColor = Color.parseColor("#cccccc");

    public static void setDefaultColor(int defaultColor) {
        WqImageView.defaultColor = defaultColor;
    }

    /**
     * 加载图片
     *
     * @param url
     * @param width
     * @param height
     * @param isNeedDefaultBackground 是否需要默认底图
     */
    private void setImage(String url, int width, int height, boolean isNeedDefaultBackground) {
        url = getUrl(url);
        imgUrl = url;
        LogUtils.i("wq 0819 加载图片url：" + url);
        try {
//            fresco:progressBarAutoRotateInterval="5000"
            setWidthHeight(width, height);
            if (isNeedDefaultBackground) {
                GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
//            LogUtils.i("wq 0418 screenBean:" + screenBean.toString());
                ColorDrawable colorDrawable = new ColorDrawable();
                colorDrawable.setColor(defaultColor);
                hierarchy.setBackgroundImage(colorDrawable);
//                simpleDraweeView.

//                setBackgroundColor(defaultColor);
//                setBackgroundColor(context.getResources().getColor(R.color.awu_text_black_666666));
//                backgroundImage.setScaleType(ImageView.ScaleType.CENTER);
//                return;
            }
            if (TextUtils.isEmpty(url)) {
                return;
            }

            Uri uri = Uri.parse(url);
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setProgressiveRenderingEnabled(true)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setControllerListener(new AwuBaseControllerListener(uri, url, 0))
                    .setOldController(simpleDraweeView.getController())
                    .build();
            simpleDraweeView.setController(controller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 设置图片资源
     *
     * @param resource
     * @param designImgWidth
     * @param designImgHeight
     */
    public void setImageResource(int resource, int designImgWidth, int designImgHeight) {
        try {
//            fresco:progressBarAutoRotateInterval="5000"

            ScreenBean screenBean = DisplayUtil.getScreenBean(designImgWidth, designImgHeight);
            setWidthHeight(screenBean.getWidth(), screenBean.getHeight());
            GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
//            LogUtils.i("wq 0418 screenBean:" + screenBean.toString());
            hierarchy.setPlaceholderImage(resource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Map<String, SoftReference<File>> cache = Collections.synchronizedMap(new HashMap<String, SoftReference<File>>());

    private File getSdFile(String path) {
        try {
            if (cache.get(path) == null) {
                File file = new File(path);
                cache.put(path, new SoftReference<>(file));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cache.get(path).get();
    }

    /**
     * 设置SD卡图片
     *
     * @param path
     * @param designImgWidth
     * @param designImgHeight
     */
    public void setSDImage(String path, int designImgWidth, int designImgHeight) {

        path = getUrl(path);
        imgUrl = path;
        LogUtils.i("wq 0819 加载SD图片path：" + path);
        try {
            ScreenBean screenBean = DisplayUtil.getScreenBean(designImgWidth, designImgHeight);
            int width = screenBean.getWidth();
            int height = screenBean.getHeight();
            setWidthHeight(width, height);
//            setBackgroundColor(context.getResources().getColor(R.color.sw_color_fafafa));
//            int resource = ImageUtil.getDefaultBackgroundImage(width);
//            backgroundImage.setImageResource(resource);
            backgroundImage.setScaleType(ImageView.ScaleType.CENTER);
            if (TextUtils.isEmpty(path)) {
                return;
            }
//            Uri uri = Uri.fromFile(new File(path));
            File file = getSdFile(path);
            finishFile = file;
            Uri uri = Uri.fromFile(file);
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setProgressiveRenderingEnabled(true)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setControllerListener(new AwuBaseControllerListener(uri, path, 0))
                    .setOldController(simpleDraweeView.getController())
                    .build();
            simpleDraweeView.setController(controller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getUrl(String url) {
        final String reserveHttp = "WQ_haha_heihei_xixi_no_s";
        final String http = "http://";
        final String https = "https://";

        final String reserveHttps = "WQ_haha_heihei_xixi_has_s";
        if (url != null) {
            if (url.contains(http)) {
                url = url.replace(http, reserveHttp);
            }
            if (url.contains(https)) {
                url = url.replace(https, reserveHttps);
            }
            url = url.replace("//", "/");
            url = url.replace(reserveHttp, http);
            url = url.replace(reserveHttps, https);

            url.replace(" ", "");
        }
        return url;
    }

    int width;
    int height;

    /**
     * 设置宽高
     *
     * @param width
     * @param height
     */
    public void setWidthHeight(int width, int height) {
        ViewGroup.LayoutParams para;
        para = getLayoutParams();
        if (para == null) {
            para = new ViewGroup.LayoutParams(width, height);
        }
        if (width > BaseApplication.screenWidth) {
            double realHeight = (double) height / (double) width * (double) BaseApplication.screenWidth;
            width = BaseApplication.screenWidth;
            height = ((int) (realHeight + 0.5f));
        }
        this.width = width;
        this.height = height;
        para.width = width;
        para.height = height;
        setLayoutParams(para);
        ViewGroup.LayoutParams imgPara = simpleDraweeView.getLayoutParams();
        if (imgPara == null) {
            imgPara = new LayoutParams(width, height);
        }
        simpleDraweeView.setLayoutParams(imgPara);
    }

    /**
     * 设置真实图片高度
     *
     * @param width
     * @param height
     */
    public void setRealHeight(int width, int height) {
        if (width == 0 || height == 0 || this.width == 0 || this.height == 0) {
            return;
        }
//        double bili = (double) width / (double) height;
//        double h = (double) width / bili;

        double realHeight = (double) this.width * (double) height / (double) width;
//        LogUtils.i("wq 1111 setRealHeight this.width:" + this.width);
//        LogUtils.i("wq 1111 setRealHeight ((int) (realHeight + 0.5f)):" + ((int) (realHeight + 0.5f)));
//        LogUtils.i("wq 1111 setRealHeight --------------------------------------------");
        setWidthHeight(this.width, ((int) (realHeight + 0.5f)));
    }

//    ControllerListener controllerListener =

    boolean isNeedRealWh = false;
    private File finishFile;

    public File getFinishFile() {
        return finishFile;
    }

    public boolean isNeedRealWH() {
        return isNeedRealWh;
    }

    //            setNeedRealWh
    public void setNeedRealWh(boolean needRealWh) {
        isNeedRealWh = needRealWh;
    }

    class AwuBaseControllerListener extends BaseControllerListener<ImageInfo> {
        public Uri uri;
        public String path;
        public int defaultImgRes;

        public AwuBaseControllerListener(Uri uri, String path, int defaultImgRes) {
            this.uri = uri;
            this.path = path;
            this.defaultImgRes = defaultImgRes;
        }

        @Override
        public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable anim) {

            backgroundImage.setImageDrawable(null);
//            LogUtils.i("wq 0820 下载图片监听1 onFinalImageSet");
//            setBackgroundDrawable(null);
//            backgroundImage.setImageDrawable(null);
//            if (imageInfo == null) {
//                return;
//            }
//            QualityInfo qualityInfo = imageInfo.getQualityInfo();
            try {
                simpleDraweeView.setVisibility(VISIBLE);
//                FileBinaryResource resource = (FileBinaryResource) Fresco.getImagePipelineFactory().getMainDiskStorageCache().getResource(new SimpleCacheKey(uri.toString()));
                FileBinaryResource resource = (FileBinaryResource) Fresco.getImagePipelineFactory().getMainFileCache().getResource(new SimpleCacheKey(uri.toString()));
                File file = resource.getFile();
                finishFile = file;
                BitmapFactory.Options options = new BitmapFactory.Options();
                /**
                 * 最关键在此，把options.inJustDecodeBounds = true;
                 * 这里再decodeFile()，返回的bitmap为空，但此时调用options.outHeight时，已经包含了图片的高了
                 */
                options.inJustDecodeBounds = true;
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options); // 此时返回的bitmap为null
                if (isNeedRealWH()) {
                    setRealHeight(options.outWidth, options.outHeight);
                }

                if (realWidthHeightCallback != null) {
                    realWidthHeightCallback.realWH(options.outWidth, options.outHeight);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onIntermediateImageSet(String id, @Nullable ImageInfo imageInfo) {
        }

        @Override
        public void onFailure(String id, Throwable throwable) {
//            img_mycenter_detail_default

            if (defaultImgRes != 0) {
                backgroundImage.setImageDrawable(null);
                GenericDraweeHierarchy hierarchy = simpleDraweeView.getHierarchy();
                hierarchy.setPlaceholderImage(defaultImgRes);
            }
        }
    }

    RealWidthHeightCallback realWidthHeightCallback;

    public void setRealWidthHeightCallback(RealWidthHeightCallback realWidthHeightCallback) {
        this.realWidthHeightCallback = realWidthHeightCallback;
    }

    public interface RealWidthHeightCallback {
        void realWH(int width, int height);
    }

}
