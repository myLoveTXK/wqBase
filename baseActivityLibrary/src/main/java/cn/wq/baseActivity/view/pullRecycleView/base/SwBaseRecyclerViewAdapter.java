package cn.wq.baseActivity.view.pullRecycleView.base;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.interfaces.list.IRecycleViewBind;
import cn.wq.baseActivity.view.sliding.SlidingButtonContentView;
import cn.wq.baseActivity.view.sliding.SlidingButtonView;


/**
 * Created by W~Q on 2017/03/21.
 */
public abstract class SwBaseRecyclerViewAdapter extends BaseRecyclerAdapter<BaseViewHolder> {
    private boolean isNeedMenu = false;
    private SlidingButtonView mMenu = null;

    public void setNeedMenu(boolean needMenu) {
        isNeedMenu = needMenu;
    }

    @Override
    public BaseViewHolder getViewHolder(View view) {
        return new SimpleAdapterViewHolder(view, false);
    }


    public class SimpleAdapterViewHolder extends BaseViewHolder {

        public SimpleAdapterViewHolder(View itemView, boolean isItem) {
            super(itemView, null);
            if (isItem) {
            }
        }
    }


    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {

        final ViewGroup baseView = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.sliding_layout_item, parent, false);
        SldingViewHolder sldingViewHolder = new SldingViewHolder(baseView, null);

        BaseViewHolder normalViewHolder = onCreateNormalViewHolder(baseView, viewType);
        sldingViewHolder.layoutContent.addView(normalViewHolder.itemView);
//        parent.post(new Runnable() {
//            @Override
//            public void run() {
//                LogUtils.i("wq 1222 parent.getWidth():" + parent.getWidth());
//                LogUtils.i("wq 1222 sldingViewHolder.itemView.getWidth():" + sldingViewHolder.itemView.getWidth());
//                LogUtils.i("wq 1222 sldingViewHolder.slidingButton.getWidth():" + sldingViewHolder.slidingButton.getWidth());
//                LogUtils.i("wq 1222 sldingViewHolder.layoutItem.getWidth():" + sldingViewHolder.layoutItem.getWidth());
//                LogUtils.i("wq 1222 sldingViewHolder.layoutContent.getWidth():" + sldingViewHolder.layoutContent.getWidth());
//                LogUtils.i("wq 1222 -----------------------------------------------------------------");
//            }
//        });
        sldingViewHolder.itemView.post(new Runnable() {
            @Override
            public void run() {
//                sldingViewHolder.layoutItem
                ViewGroup.LayoutParams layoutParams = sldingViewHolder.layoutContent.getLayoutParams();
                layoutParams.width = sldingViewHolder.itemView.getWidth();
                sldingViewHolder.layoutContent.setLayoutParams(layoutParams);
            }
        });
        return sldingViewHolder;
//        return normalViewHolder;
    }

    /**
     * 关闭菜单
     */
    public void closeMenu() {
        mMenu.closeMenu();
//        mMenu = null;

    }

    /**
     * 判断是否有菜单打开
     */
    public Boolean menuIsOpen() {
        if (mMenu != null) {
            return true;
        }
        Log.i("asd", "mMenu为null");
        return false;
    }

//    @Override
//    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//    }

    @Override
    public void onBindViewHolder(final BaseViewHolder holder, int position, boolean isItem) {
        ((SldingViewHolder) holder).slidingButton.setNeedScroller(isNeedMenu);
        ((SldingViewHolder) holder).slidingButton.setSlidingButtonListener(new SlidingButtonView.IonSlidingButtonListener() {
            @Override
            public void onMenuIsOpen(View view) {
                mMenu = (SlidingButtonView) view;
            }

            @Override
            public boolean isCanOperation(SlidingButtonView slidingButtonView) {
                if (mMenu == null) {
                    return true;
                }
                if (mMenu.getOpen()) {
                    if (mMenu == slidingButtonView) {
                        return true;
                    }
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public void onDownOrMove(SlidingButtonView slidingButtonView) {
                if (menuIsOpen()) {
                    if (mMenu != slidingButtonView) {
                        closeMenu();
                    }
                }
            }
        });

        View normalItem = ((SldingViewHolder) holder).layoutContent.getChildAt(0);
        ((SldingViewHolder) holder).layoutContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuIsOpen()) {
                    closeMenu();
                }
            }
        });
//        ((SldingViewHolder) holder).layoutContent.getLayoutParams().width = SlidingUtils.getScreenWidth(holder.itemView.getContext());

        BaseViewHolder normalViewHolder = (BaseViewHolder) normalItem.getTag();

        normalViewHolder.setBaseParentViewHolder((SldingViewHolder) holder);
        normalViewHolder.onBindViewHolder(getItemViewType(position), position);
    }

    @Override
    public int getAdapterItemCount() {
        return getDataCount();
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return getDataViewType(position);
    }

    protected abstract int getDataCount();

    protected abstract BaseViewHolder onCreateNormalViewHolder(ViewGroup parent, int viewType);


    protected int getDataViewType(int position) {
        return 0;
    }

    public class SldingViewHolder extends BaseViewHolder {
        //        @BindView(R.id.tv_delete)
        public TextView tvDelete;
        //        @BindView(R.id.layout_content)
        public SlidingButtonContentView layoutContent;
        public LinearLayout layoutItem;
        public FrameLayout textHead;
        public FrameLayout textFoot;
        //        @BindView(R.id.sliding_button)
        public SlidingButtonView slidingButton;

        SldingViewHolder(View itemView, IRecycleViewBind iRecycleViewBind) {
            super(itemView, iRecycleViewBind);
            tvDelete = (TextView) itemView.findViewById(R.id.tv_delete);
            layoutContent = (SlidingButtonContentView) itemView.findViewById(R.id.layout_content);
            layoutItem = (LinearLayout) itemView.findViewById(R.id.sliding_item);
            textHead = (FrameLayout) itemView.findViewById(R.id.text_head);
            textFoot = (FrameLayout) itemView.findViewById(R.id.text_foot);
            slidingButton = (SlidingButtonView) itemView.findViewById(R.id.sliding_button);
        }

        public void closeMenu() {
            slidingButton.closeMenu();
        }

        public boolean isOpenMenu() {
            return slidingButton.getOpen();
        }
    }
}
