package cn.wq.baseActivity.view.edit;

import android.content.Context;
import android.text.TextPaint;
import android.util.AttributeSet;

/**
 * Created by Administrator on 2016/11/15.
 */
public class ClearEditBoldText extends ClearEditText {


    public ClearEditBoldText(Context context) {
        this(context, null);
    }

    public ClearEditBoldText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ClearEditBoldText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }


    @Override
    public void onTextChanged(CharSequence s, int start, int count, int after) {
        super.onTextChanged(s, start, count, after);
        TextPaint paint = getPaint();
        if (s.length() == 0) {
            paint.setFakeBoldText(false);
        } else {
            paint.setFakeBoldText(true);
        }
    }


}