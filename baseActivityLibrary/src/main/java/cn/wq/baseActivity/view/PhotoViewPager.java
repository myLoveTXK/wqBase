package cn.wq.baseActivity.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import java.util.List;

import cn.wq.baseActivity.activity.common.adapter.PhotoViewAdapter;

/**
 * Created by xt on 2018/6/14.
 */

public class PhotoViewPager extends ViewPager {

    PhotoViewAdapter adapter;

    public PhotoViewPager(@NonNull Context context) {
        super(context);
        init();
    }

    public PhotoViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        adapter = new PhotoViewAdapter(getContext());
        setAdapter(adapter);
    }

    public void setUrls(List<String> urls) {
        adapter.setList(urls);
    }
}
