package cn.wq.baseActivity.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.lidroid.xutils.util.LogUtils;

import cn.wq.baseActivity.R;


/**
 * Created by W~Q on 2016/6/6.
 */
public class NoReadView extends View {

    private int centerColor;
    private int edgeLineColor;
    private int textColor;
    private float edgeLineWidth;
    private float height;
    private float width;
    private final float half = (float) 2.0;
    private int noReadText;
    private float textSize;
    private float textHeight;
    Paint.FontMetrics fontMetrics;
    private Paint textPaint;
    private Paint edgeLinePaint;
    private Paint centerPaint;
    private final int maxNoRead = 99;

    public NoReadView(Context context) {
        super(context);
        init();
    }

    public NoReadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        initData(attrs);
    }

    public NoReadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        initData(attrs);
    }

//    public NoReadView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        init();
//        initData(attrs);
//    }

    private void initData(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NoReadView);
        centerColor = typedArray.getColor(R.styleable.NoReadView_noReadViewCenterColor, centerColor);
        edgeLineColor = typedArray.getColor(R.styleable.NoReadView_noReadViewEdgeLineColor, edgeLineColor);
        textColor = typedArray.getColor(R.styleable.NoReadView_noReadViewTextColor, textColor);
        edgeLineWidth = typedArray.getDimension(R.styleable.NoReadView_noReadViewEdgeLineWidth, edgeLineWidth);
//        height = typedArray.getDimension(R.styleable.NoReadView_noReadViewHeight, height);
//        width = typedArray.getDimension(R.styleable.NoReadView_noReadViewWidth, width);
        height = typedArray.getDimension(R.styleable.NoReadView_noReadViewHeight, height);
        width = typedArray.getDimension(R.styleable.NoReadView_noReadViewWidth, width);
        noReadText = typedArray.getInt(R.styleable.NoReadView_noReadViewText, 0);
        textSize = typedArray.getDimension(R.styleable.NoReadView_noReadViewTextSize, textSize);
//        if(noReadText==null){
//            noReadText="";
//        }
        width = ((int) width);
        height = ((int) height);
        initContent();

    }

    int num = 0;

    private void init() {
        num++;
        centerColor = getContext().getResources().getColor(R.color.noReadViewCenterColor);
        edgeLineColor = getContext().getResources().getColor(R.color.noReadViewEdgeLineColor);
        textColor = getContext().getResources().getColor(R.color.noReadViewTextColor);
        edgeLineWidth = getContext().getResources().getDimension(R.dimen.noReadViewEdgeLineWidth);
        height = getContext().getResources().getDimension(R.dimen.noReadViewHeight);
        width = getContext().getResources().getDimension(R.dimen.noReadViewWidth);
//        noReadText = "99+";
        textSize = getContext().getResources().getDimension(R.dimen.noReadViewTextSize);

        textPaint = new Paint();
        edgeLinePaint = new Paint();
        centerPaint = new Paint();

    }

    private void initContent() {
        textPaint.setAntiAlias(true);
        edgeLinePaint.setAntiAlias(true);
        centerPaint.setAntiAlias(true);

//        textPaint.setStrokeWidth(1);
        textPaint.setTextSize(textSize);
        textPaint.setColor(textColor);
        textPaint.setTextAlign(Paint.Align.CENTER);

        fontMetrics = textPaint.getFontMetrics();
        textHeight = fontMetrics.bottom - fontMetrics.top;

        edgeLinePaint.setColor(edgeLineColor);
        edgeLinePaint.setStrokeWidth(edgeLineWidth);
        centerPaint.setColor(centerColor);

        edgeLinePaint.setStyle(Paint.Style.STROKE); //设置空心
        centerPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension((int) (width), (int) (height));
        //自定义view的宽高时，不实用下面函数
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        noReadText =99;
        float change = 0;
        if (noReadText > 99) {
            change = 0;
        } else if (noReadText >= 10) {
            change = getContext().getResources().getDimension(R.dimen.noReadViewTextSizeChange);
        } else if (noReadText > 0) {
            change = getContext().getResources().getDimension(R.dimen.noReadViewTextSizeChange) * 2;
        } else {
            change = 0;
        }
        textPaint.setTextSize(textSize + change);

        canvas.drawCircle(width / half, height / half, width / half, centerPaint);
        canvas.drawCircle(width / half, height / half, width / half - edgeLineWidth / half, edgeLinePaint);

        fontMetrics = textPaint.getFontMetrics();
//        textHeight = -fontMetrics.ascent;
        textHeight = fontMetrics.bottom - fontMetrics.top;
        float textBaseY = height - (height - textHeight) / half - fontMetrics.bottom * (float) 1.1;

        if (noReadText > maxNoRead) {
            canvas.drawText(maxNoRead + "+", width / half, textBaseY, textPaint);
        } else {
            canvas.drawText(noReadText + "", width / half, textBaseY, textPaint);
        }
//        setNoRead();
        LogUtils.i("wq 1220 noread:" + noReadText);
        if (noReadText <= 0) {
            setVisibility(View.GONE);
        } else {
            setVisibility(View.VISIBLE);
        }
        super.onDraw(canvas);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     */
    private int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 将px值转换为dip或dp值，保证尺寸大小不变
     */
    private int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

//    private void textViewInit() {
//        setBackgroundResource(R.drawable.no_read_bg);
//        setTextColor(Color.RED);
//        ViewGroup.LayoutParams params=new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//        setLayoutParams(params);
//        setGravity(Gravity.CENTER);
//        setVisibility(View.GONE);
//    }


    public void setNoRead(int number) {
        noReadText = number;

        if (number <= 0) {
            setVisibility(View.GONE);
        } else {
            setVisibility(View.VISIBLE);
        }
//        invalidate();
        postInvalidate();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                setNoRead(noReadText + 1);
//            }
//        }, 1000);
    }

}
