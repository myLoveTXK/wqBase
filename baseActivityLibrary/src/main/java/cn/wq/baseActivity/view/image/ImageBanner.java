package cn.wq.baseActivity.view.image;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.flyco.banner.widget.Banner.base.BaseBanner;

import cn.wq.baseActivity.view.image.util.DisplayUtil;


public class ImageBanner extends BaseBanner<String, ImageBanner> {
    private int width = DisplayUtil.designImageWidth;
    private int height;

    int imgDefault = 0;

    private float radius = 0;

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public ImageBanner(Context context) {
        this(context, null, 0);
    }

    public ImageBanner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageBanner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImgDefault(int imgDefault) {
        this.imgDefault = imgDefault;
    }

    @Override
    public void onTitleSlect(TextView tv, int position) {
    }


    @Override
    public View onCreateItemView(int position) {
        WqImageView awuImageView = new WqImageView(context);
        String img = list.get(position);
        LayoutParams layoutParams = (LayoutParams) vp.getLayoutParams();
        layoutParams.height = LayoutParams.MATCH_PARENT;
        vp.setLayoutParams(layoutParams);
        if (imgDefault != 0) {
            awuImageView.setDesignImage(img, width, height, imgDefault);
        } else {
            awuImageView.setDesignImage(img, width, height);
        }

        if (radius != 0) {
            awuImageView.setCornersRadius(radius);
        }
        return awuImageView;
    }

    @Override
    public View onCreateIndicator() {
        return null;
    }

    @Override
    public void setCurrentIndicator(int i) {
    }
}
