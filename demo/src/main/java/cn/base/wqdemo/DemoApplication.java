package cn.base.wqdemo;

import cn.base.wqdemo.utils.Config;
import cn.wq.baseActivity.BaseApplication;

/**
 * Created by xt on 2018/6/21.
 */

public class DemoApplication extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Config.init();
    }
}
