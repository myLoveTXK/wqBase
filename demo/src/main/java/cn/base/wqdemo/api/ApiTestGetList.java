package cn.base.wqdemo.api;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import shinyway.request.base.post.BaseProjectHttpPostRequest;

/**
 * Created by xt on 2018/6/21.
 */

public class ApiTestGetList extends BaseProjectHttpPostRequest{
    int page;
    int pageSize;
    public ApiTestGetList(Context context,int page,int pageSize) {
        super(context);
        this.page = page;
        this.pageSize = pageSize;
    }

    @Override
    protected Map<String, String> getMapParam()  {
        Map<String, String> mapParam = new HashMap<>();

        mapParam.put("page", page + "");
        mapParam.put("pageSize", pageSize + "");

        return mapParam;
    }

    @Override
    protected String getUrl() {
        return "http://ssl.xt.cn/wholeway/api.php?action=listzt";
    }

    @Override
    protected String apiName() {
        return "获取精选活动";
    }

    @Override
    protected boolean isNeedEncrypt() {
        return false;
    }
    @Override
    protected boolean isNeedParseOuterLayerStatus() {
        return false;
    }
}
