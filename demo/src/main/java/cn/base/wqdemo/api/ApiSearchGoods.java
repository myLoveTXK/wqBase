package cn.base.wqdemo.api;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import shinyway.request.base.post.BaseProjectHttpPostRequest;

/**
 * Created by xt on 2018/6/21.
 */

public class ApiSearchGoods extends BaseProjectHttpPostRequest {

    public ApiSearchGoods(Context context) {
        super(context);
    }


    class ParamBean {

        /**
         * brandList : []
         * goodsName :
         * pageNum : 0
         * pageSize : 0
         * startNum : 0
         * typeList : []
         */

        private String goodsName;
        private int pageNum;
        private int pageSize;
        private int startNum;
        private List<?> brandList;
        private List<?> typeList = new ArrayList<>();

    }

//    @Override
//    protected Map<String, Object> getStringJsonBodyParam(Map map) {
//        return super.getStringJsonBodyParam(map);
//    }

    @Override
    protected Map getStringJsonBodyParam(Map objectMap) {
        objectMap.put("brandList", new ArrayList<>());
        objectMap.put("pageNum", 1);
        objectMap.put("goodsName", "");
        return objectMap;
    }

//    @Override
//    protected String getStringBodyParam() {
//        return JsonBeanUtil.getObjectJson(new ParamBean());
//    }

    @Override
    protected String getUrl() {
        return url + "/api/goods/query";
    }

    @Override
    protected String apiName() {
        return "查询商品信息";
    }

}
