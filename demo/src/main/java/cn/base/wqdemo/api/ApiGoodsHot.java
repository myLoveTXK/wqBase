package cn.base.wqdemo.api;

import android.content.Context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.base.wqdemo.bean.GoodsBean;
import shinyway.request.base.post.BaseProjectHttpPostRequest;

/**
 * Created by xt on 2018/6/21.
 */

public class ApiGoodsHot extends BaseProjectHttpPostRequest<List<GoodsBean>> {

    public ApiGoodsHot(Context context) {
        super(context);
    }

    @Override
    protected Map<String, String> getMapParam() {
        Map<String, String> mapParam = new HashMap<>();
        return mapParam;
    }
    @Override
    protected Map getHeadMap() {
        Map map = new HashMap();
        map.put("wqTest33","wqTest44");
        return map;
    }
    @Override
    protected String getUrl() {
        return url + "/api/goods/hot";
    }

    @Override
    protected String apiName() {
        return "查询热门商品";
    }

}
