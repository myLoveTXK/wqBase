package cn.base.wqdemo.api;

import android.content.Context;

import com.wq.baseRequest.bean.HttpResponseInfoBean;

import java.util.HashMap;
import java.util.Map;

import shinyway.request.base.BaseProjectHttpRequestCommonParam;
import shinyway.request.base.get.BaseProjectHttpGetRequest;
import shinyway.request.interfaces.ProjectRequestCallback;


/**
 * Created by xt on 2018/6/21.
 */

public class ApiGetUserJifen extends BaseProjectHttpGetRequest<String> {

    public ApiGetUserJifen(Context context) {
        super(context);
    }

    @Override
    protected Map getStringJsonBodyParam(Map objectMap) {
        return objectMap;
    }

    @Override
    protected String getUrl() {
        return url + "/api/customer/score";
    }

    @Override
    protected Map getHeadMap() {
        Map map = new HashMap();
        map.put("token", "CCN4ANWDXCNKVZDPVBUH2AE9G3F2SYTV");
        return map;
    }

    @Override
    protected String apiName() {
        return "查询会员可用积分";
    }

    @Override
    protected BaseProjectHttpRequestCommonParam getTokenApi() {
        ApiGoodsHot apiGoodsHot = new ApiGoodsHot(getContext());
        apiGoodsHot.setTokenCallback(new ProjectRequestCallback() {
            @Override
            public void swSuccess(HttpResponseInfoBean result) {

            }

            @Override
            public void swFail(HttpResponseInfoBean result) {
            }
        });
        return apiGoodsHot;
    }

}
