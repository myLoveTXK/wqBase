package cn.base.wqdemo.bean;

import cn.wq.baseActivity.view.image.WqBanner;

public class BannerBean implements WqBanner.IWqBannerBean {
    String imgUrl;

    public BannerBean(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public String getBannerImgUrl() {
        return imgUrl;
    }

    @Override
    public String toString() {
        return "BannerBean{" +
                "imgUrl='" + imgUrl + '\'' +
                '}';
    }
}