package cn.base.wqdemo.bean;

/**
 * Created by xt on 2019/12/12.
 * 商品
 */

public class GoodsBean {

    /**
     * goodsId : 5960971974889465
     * goodsName : 玉米
     * url : https://www.iovogueholic.com/api/goods/img/download/2182497152023309.jpg
     * price : 1
     * saleType : SPOT
     */

    private String goodsId;
    private String goodsName;
    private String url;
    private String price;
    private String saleType;

    public String getGoodsId() { return goodsId;}

    public void setGoodsId(String goodsId) { this.goodsId = goodsId;}

    public String getGoodsName() { return goodsName;}

    public void setGoodsName(String goodsName) { this.goodsName = goodsName;}

    public String getUrl() { return url;}

    public void setUrl(String url) { this.url = url;}

    public String getPrice() { return price;}

    public void setPrice(String price) { this.price = price;}

    public String getSaleType() { return saleType;}

    public void setSaleType(String saleType) { this.saleType = saleType;}

    @Override
    public String toString() {
        return "GoodsBean{" +
                "goodsId='" + goodsId + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", url='" + url + '\'' +
                ", price='" + price + '\'' +
                ", saleType='" + saleType + '\'' +
                '}';
    }
}
