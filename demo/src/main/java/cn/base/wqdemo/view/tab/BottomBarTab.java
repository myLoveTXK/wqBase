package cn.base.wqdemo.view.tab;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lidroid.xutils.util.LogUtils;


/**
 * Created by Administrator on 6/15 0015.
 */
public class BottomBarTab extends LinearLayout {
    Paint mPaint;
    Context context;
    private LinearLayout mTabLayout;

    private LayoutParams mTabParams;
    private int mCurrentPosition = 0;

    public BottomBarTab(Context context) {
        this(context, null);
    }

    public BottomBarTab(Context context, AttributeSet attrs) {

        this(context, attrs, 0);
    }

    public BottomBarTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setOrientation(VERTICAL);
        setGravity(Gravity.BOTTOM);
        mTabLayout = new LinearLayout(context);
        mTabLayout.setBackgroundColor(Color.TRANSPARENT);
        mTabLayout.setOrientation(LinearLayout.HORIZONTAL);
        mTabLayout.setGravity(Gravity.CENTER);
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mTabLayout.setLayoutParams(lp);
        addView(mTabLayout);
        mTabParams = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        mTabParams.weight = 1;


        this.context = context;
        mPaint = new Paint();
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    public BottomBarTab addItem(final BottomBarButton tab) {
        LogUtils.i("wq 0317 addItem:" + tab.getDataBean().getText());
        tab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = tab.getTabPosition();
//                LogUtils.i("wq 0317 点击了tab:" + position);
                if (mCurrentPosition == position) {
                    tab.setSelected(true);
                    mCurrentPosition = position;
                } else {
                    tab.setSelected(true);
                    mTabLayout.getChildAt(mCurrentPosition).setSelected(false);
                    mCurrentPosition = position;
                }
            }
        });

        tab.setTabPosition(mTabLayout.getChildCount());
        tab.setLayoutParams(mTabParams);
        mTabLayout.addView(tab);
        return this;
    }

    /**
     * @param position
     */
    public void setCurrentItem(final int position) {
        mTabLayout.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mTabLayout.getChildAt(position).performClick();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        mPaint.setColor(Color.argb(224, 255, 255, 255));
//        // 设置画笔遮罩滤镜  ,传入度数和样式
//        mPaint.setMaskFilter(new BlurMaskFilter(1, BlurMaskFilter.Blur.NORMAL));
        // 画一个矩形
//        canvas.drawRect(0, 0, 1125, 204, ,.);
//        Bitmap srcBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.bg_tabbar);
//        Bitmap shadowBitmap = srcBitmap.extractAlpha();
////        getBackground()
//
////        Rect mSrcRect = new Rect(0, 0, mBitWidth, mBitHeight);
//        Rect mDestRect = new Rect(0, 3, getWidth(), getHeight());
//
//        canvas.drawBitmap(shadowBitmap, null, mDestRect, mPaint);
//        set
//        canvas.drawRect(0, 80, getWidth(), srcBitmap.getHeight(), mPaint);

    }
}
