package cn.base.wqdemo.view.tab;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import cn.base.wqdemo.R;
import cn.wq.baseActivity.view.NoReadView;


/**
 * Created by Administrator on 6/15 0015.
 */
public class BottomBarButton extends FrameLayout {

    private DataBean dataBean;
    private final int tabUnselectColor = Color.parseColor("#666666");
    private int tabSelectColor = Color.parseColor("#291c01");

    private String content;
    private ImageView mIcon;
    private TextView tv;
    private View bottomLayout;
    private View onlyImgLayout;
    private ImageView onlyImg;

    private int mTabPosition = -1;
    private NoReadView tabNoread;

    private Context mContext;
    private TabSelectListener tabSelectListener;

    public BottomBarButton(Context context, @DrawableRes int icon, @StringRes int content) {
        this(context, null, icon, content);
    }

    public BottomBarButton(Context context, AttributeSet attrs, @DrawableRes int icon, @StringRes int content) {
        this(context, attrs, 0, icon, content);
    }

    public BottomBarButton(Context context, AttributeSet attrs, int defStyleAttr, @DrawableRes int icon, @StringRes int content) {
        super(context, attrs, defStyleAttr);
        init(context, icon, content);
    }

    public View getBottomLayout() {
        return bottomLayout;
    }

    public View getOnlyImgLayout() {
        return onlyImgLayout;
    }

    public ImageView getOnlyImg() {
        return onlyImg;
    }

    public DataBean getDataBean() {
        return dataBean;
    }

    public String getContent() {
        return content;
    }

    private void init(Context context, int icon, int content) {
        this.content = context.getString(content);
        this.mContext = context;
//        tabSelectColor = context.getResources().getColor(R.color.colorPrimary);
        dataBean = new DataBean(icon, context.getResources().getString(content));

//        TypedArray typedArray = context.obtainStyledAttributes(
//                new int[]{R.attr.selectableItemBackgroundBorderless});
//        Drawable drawable = typedArray.getDrawable(0);
//        setBackgroundDrawable(drawable);
//        typedArray.recycle();
        View view = LayoutInflater.from(context).inflate(R.layout.tab_bottom_bar, null);

        bottomLayout = view.findViewById(R.id.bottomLayout);
        onlyImgLayout = view.findViewById(R.id.onlyImgLayout);
        onlyImg = (ImageView) view.findViewById(R.id.onlyImg);
        mIcon = (ImageView) view.findViewById(R.id.bottom_img);
        tabNoread = (NoReadView) view.findViewById(R.id.tab_noread);
        tv = (TextView) view.findViewById(R.id.bottom_tv);


        if (!TextUtils.isEmpty(dataBean.getText())) {
            tv.setText(dataBean.getText());
            tv.setVisibility(VISIBLE);
        } else {
            tv.setVisibility(GONE);
        }
        tv.setTextColor(tabUnselectColor);
        mIcon.setImageResource(0);
        mIcon.setImageResource(dataBean.getSelectImageResource());
//        mIcon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setSelected(true);
//            }
//        });
//        mIcon.setColorFilter(tabUnselectColor);
        addView(view);
    }

    @Override
    public synchronized void setSelected(boolean selected) {
        mIcon.setSelected(selected);
//        mIcon.set(selected);
        if (selected) {
//            mIcon.setColorFilter(tabSelectColor);
            tv.setTextColor(tabSelectColor);
        } else {
//            mIcon.setColorFilter(tabUnselectColor);
            tv.setTextColor(tabUnselectColor);
        }
        if (tabSelectListener != null) {
            tabSelectListener.onSelect(selected);
        }
        super.setSelected(selected);
    }

    public synchronized void setTabPosition(int position) {
        mTabPosition = position;
        if (position == 0) {
            setSelected(true);
        }
    }

    public int getTabPosition() {
        return mTabPosition;
    }

    public void setOnSelectListener(TabSelectListener tabSelectListener) {
        this.tabSelectListener = tabSelectListener;
    }

    public void setNoReadCount(int count) {
        if (tabNoread != null) {
            tabNoread.setNoRead(count);
        }
    }


    public interface TabSelectListener {
        void onSelect(boolean isSelect);
    }

    /**
     * 底部数据bean
     */
    public class DataBean {
        private int selectImageResource;//选择的图片
        //        private int unSelectImageResource;//未选择时候的图片
        private String text;//文字

        public DataBean(int selectImageResource, String text) {
            this.selectImageResource = selectImageResource;
//            this.unSelectImageResource = unSelectImageResource;
            this.text = text;
        }

        public int getSelectImageResource() {
            return selectImageResource;
        }

        public String getText() {
            return text;
        }
    }
}
