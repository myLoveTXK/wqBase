package cn.base.wqdemo.utils;

import com.wq.baseRequest.utils.request.okhttp.DomainManager;
import com.wq.baseRequest.utils.request.okhttp.WqOkHttp;

import shinyway.request.ProjectRequestConfig;

/**
 * Created by xt on 2019/12/12.
 */

public class Config {
    public static final String PATH = "/WQ_path";


    public static boolean isDebug = true;//debug模式

    public static String SERVICE_API_URL;//API接口URL
    public static boolean SERVICE_API_IS_NEED_ENCRYPT = false;//API接口是否需要加密,有需要再开发


    public static void init() {
        if (isDebug) {
            SERVICE_API_URL = "http://test.iovogueholic.com";
        } else {
            SERVICE_API_URL = DomainManager.domainList.get(0);
        }
        WqOkHttp.isDebug = isDebug;
        setApiUrl(SERVICE_API_URL, SERVICE_API_IS_NEED_ENCRYPT, true);

    }


    /**
     * 设置基类api路径
     *
     * @param SERVICE_API_URL             apiUrl
     * @param SERVICE_API_IS_NEED_ENCRYPT 是否需要加密
     * @param isNeedParseOuterLayerStatus 是否需要解析外层(status)，如果不需要，继承后自定义解析即可
     */
    static void setApiUrl(String SERVICE_API_URL, boolean SERVICE_API_IS_NEED_ENCRYPT, boolean isNeedParseOuterLayerStatus) {
        ProjectRequestConfig.SERVICE_API_URL = SERVICE_API_URL;
        ProjectRequestConfig.SERVICE_API_IS_NEED_ENCRYPT = SERVICE_API_IS_NEED_ENCRYPT;
        ProjectRequestConfig.isNeedParseOuterLayerStatus = isNeedParseOuterLayerStatus;
    }


}
