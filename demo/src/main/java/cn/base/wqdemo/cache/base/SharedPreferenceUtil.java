package cn.base.wqdemo.cache.base;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Victor on 2015/8/29.
 */
public class SharedPreferenceUtil {
    private final static String sharedPreferencePath = "sw_preferences_version_1";
    private static SharedPreferences mSharedPreferences;

    private static void init(Context context) {
        if (mSharedPreferences == null) {
            mSharedPreferences = context.getSharedPreferences(sharedPreferencePath, Context.MODE_PRIVATE);
//            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    /**
     * 添加Int类型
     *
     * @param context
     * @param key
     * @param value
     */
    public static void setSharedIntData(Context context, String key, int value) {
        if (mSharedPreferences == null) {
            init(context);
        }
        mSharedPreferences.edit().putInt(key, value).commit();
    }

    /**
     * 获取Int类型
     *
     * @param context
     * @param key
     * @return
     */
    public static int getSharedIntData(Context context, String key) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return getSharedIntData(context, key, -1);
    }

    /**
     * 获取Int类型
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    public static int getSharedIntData(Context context, String key,
                                       int defaultValue) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return mSharedPreferences.getInt(key, defaultValue);
    }

    /**
     * 添加Long类型
     *
     * @param context
     * @param key
     * @param value
     */
    public static void setSharedlongData(Context context, String key, long value) {
        if (mSharedPreferences == null) {
            init(context);
        }
        mSharedPreferences.edit().putLong(key, value).commit();
    }

    /**
     * 获取Long类型
     *
     * @param context
     * @param key
     * @return
     */
    public static long getSharedlongData(Context context, String key) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return getSharedlongData(context, key, -1L);
    }

    /**
     * 获取Long类型
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    public static long getSharedlongData(Context context, String key,
                                         long defaultValue) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return mSharedPreferences.getLong(key, defaultValue);
    }

    /**
     * 添加Float类型
     *
     * @param context
     * @param key
     * @param value
     */
    public static void setSharedFloatData(Context context, String key,
                                          float value) {
        if (mSharedPreferences == null) {
            init(context);
        }
        mSharedPreferences.edit().putFloat(key, value).commit();
    }

    /**
     * 获取Float类型
     *
     * @param context
     * @param key
     * @return
     */
    public static Float getSharedFloatData(Context context, String key) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return getSharedFloatData(context, key, -1f);
    }

    /**
     * 获取Float类型
     *
     * @param context
     * @param key
     * @return
     */
    public static Float getSharedFloatData(Context context, String key,
                                           float defaultValue) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return mSharedPreferences.getFloat(key, defaultValue);
    }

    /**
     * 添加Boolean类型
     *
     * @param context
     * @param key
     * @param value
     */
    public static void setSharedBooleanData(Context context, String key,
                                            boolean value) {
        if (mSharedPreferences == null) {
            init(context);
        }
        mSharedPreferences.edit().putBoolean(key, value).commit();
    }

    /**
     * 获取Boolean类型
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean getSharedBooleanData(Context context, String key) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return getSharedBooleanData(context, key, false);
    }

    /**
     * 获取Boolean类型
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean getSharedBooleanData(Context context, String key,
                                               boolean defaultValue) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    /**
     * 添加String类型
     *
     * @param context
     * @param key
     * @param value
     */
    public static void setSharedStringData(Context context, String key,
                                           String value) {
        if (mSharedPreferences == null) {
            init(context);
        }
        mSharedPreferences.edit().putString(key, value).commit();
    }

    /**
     * 获取String类型
     *
     * @param context
     * @param key
     * @return
     */
    public static String getSharedStringData(Context context, String key) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return getSharedStringData(context, key, "");
    }

    /**
     * 获取String类型
     *
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getSharedStringData(Context context, String key,
                                             String defaultValue) {
        if (mSharedPreferences == null) {
            init(context);
        }
        return mSharedPreferences.getString(key, defaultValue);
    }

    /**
     * 删除某个键�?内容
     *
     * @param context
     * @param key
     */
    public static void remove(Context context, String key) {
        if (mSharedPreferences == null) {
            init(context);
        }
        mSharedPreferences.edit().remove(key).commit();
    }

    /**
     * 清空�?��
     *
     * @param context
     */
    public static void clearAll(Context context) {
        if (mSharedPreferences == null) {
            init(context);
        }
        mSharedPreferences.edit().clear().commit();
    }

}
