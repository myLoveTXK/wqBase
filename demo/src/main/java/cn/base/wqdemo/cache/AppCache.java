package cn.base.wqdemo.cache;

import android.support.annotation.NonNull;

import com.lidroid.xutils.util.LogUtils;

import java.util.Calendar;

import cn.base.wqdemo.DemoApplication;
import cn.base.wqdemo.cache.base.SharedPreferenceUtil;

/**
 * Created by Administrator on 2016/12/8.
 */
public class AppCache {
    private final static String ALI_SOPHIX_GET_COUNT_KEY = "ali_sophix_get_count_key_";//热修复获取次数
    private final static String SERVICE_CONFIG_KEY = "service_config_key";//服务端配置项
    private final static String FIRST_FUNCTION_GUIDE = "first_function_guide_key";//功能引导
    private final static String FIRST_OPEN_APP_KEY = "FIRST_OPEN_APP_KEY";
    private final static String IS_SHOW_GUIDE_CONTACT = "IS_SHOW_GUIDE_CONTACT";


    /**
     * 设置是否功能引导过程
     *
     * @param isFirst
     */
    public static void setFirstFunctionGuide(boolean isFirst) {
        SharedPreferenceUtil.setSharedBooleanData(DemoApplication.getInstance(), FIRST_FUNCTION_GUIDE, isFirst);
    }

    /**
     * 是否第一次功能引导
     *
     * @return
     */
    public static boolean isFirstFunctionGuide() {
        return SharedPreferenceUtil.getSharedBooleanData(DemoApplication.getInstance(), FIRST_FUNCTION_GUIDE, true);
    }


    public static void setFirstOpenApp(boolean isFirst) {
        SharedPreferenceUtil.setSharedBooleanData(DemoApplication.getInstance(), FIRST_OPEN_APP_KEY, isFirst);
    }

    public static boolean isFirstOpenApp() {
        return SharedPreferenceUtil.getSharedBooleanData(DemoApplication.getInstance(), FIRST_OPEN_APP_KEY, true);
    }

    public static void saveConfig(String configStr) {
        SharedPreferenceUtil.setSharedStringData(DemoApplication.getInstance(), SERVICE_CONFIG_KEY, configStr);
    }

    public static String getConfig() {
        String config = SharedPreferenceUtil.getSharedStringData(DemoApplication.getInstance(), SERVICE_CONFIG_KEY);
        return config;
    }


    public static void setTodaySophixGetCountAdd() {
        String data = getTodayDateString();
        int count = getTodaySophixGetCount();

        SharedPreferenceUtil.setSharedIntData(DemoApplication.getInstance(), ALI_SOPHIX_GET_COUNT_KEY + data, count + 1);
    }

    public static int getTodaySophixGetCount() {
        String data = getTodayDateString();
        int count = SharedPreferenceUtil.getSharedIntData(DemoApplication.getInstance(), ALI_SOPHIX_GET_COUNT_KEY + data, 0);
        return count;
    }

    @NonNull
    private static String getTodayDateString() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String date = year + "-" + month + "-" + day;
        LogUtils.i("wq 0824 当前日期date：" + date);
        return date;
    }


}
