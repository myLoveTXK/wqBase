package cn.base.wqdemo.activity.demo.tab.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.interfaces.list.IRecycleViewBind;
import cn.wq.baseActivity.base.ui.list.BaseRecycleListDataViewDelegate;
import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;

/**
 * Created by W~Q on 2017/5/2.
 * View视图层，完全移除与Presenter业务逻辑的耦合
 */

public class TabFragmentListViewDelegate extends BaseRecycleListDataViewDelegate<String> {


    @Override
    public void initWidget() {
        super.initWidget();
        setToolbarTitle(getClass().getSimpleName());
//        setToolbarLeftButton(R.mipmap.base_back, "");
//        setToolbarRightButton(0, "右边");
        setToolbarBackgroundColorRes(R.color.baseColorPrimaryDark);
        setStatusBarColorRes(R.color.baseColorPrimaryDark);
    }

    @Override
    public BaseViewHolder getViewHolder(ViewGroup parent, int viewType, IRecycleViewBind iRecycleViewBind) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_demo, parent, false);
        return new ViewHolder(view, iRecycleViewBind);
    }

    @Override
    public void onBindData(int viewType, BaseViewHolder baseViewHolder, String bean, int position, int size) {
        if (bean == null) {
            return;
        }
        ViewHolder viewHolder = (ViewHolder) baseViewHolder;
//        viewHolder.demoTextView.setText(bean);
    }

    public class ViewHolder extends BaseViewHolder {
//        @BindView(R.id.demoTextView)
//        TextView demoTextView;

        ViewHolder(View view, IRecycleViewBind iRecycleViewBind) {
            super(view, iRecycleViewBind);
            ButterKnife.bind(this, view);
        }
    }

}
