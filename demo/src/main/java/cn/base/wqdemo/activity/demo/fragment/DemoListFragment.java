package cn.base.wqdemo.activity.demo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import cn.wq.baseActivity.base.ui.list.fragments.TabFragment;
import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;

/**
 * Created by xt on 2019/12/11.
 * ViewPager使用
 */

public class DemoListFragment extends TabFragment<FragmentListViewDelegate, String> {

    @Override
    public String getTabTitle() {
        return "DemoListFragment";
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDelegate().setShowToolbar(true);
    }

    @Override
    protected Class<FragmentListViewDelegate> getDelegateClass() {
        return FragmentListViewDelegate.class;
    }

    @Override
    public void onRefresh(boolean isPullDown) {

    }

    @Override
    public void onLoadMore(boolean isSilence) {

    }

    @Override
    public void onViewHolderListener(int viewType, BaseViewHolder baseViewHolder, String s, int position) {

    }
}
