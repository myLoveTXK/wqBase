package cn.base.wqdemo.activity.welcome.view;

import cn.base.wqdemo.R;
import cn.wq.baseActivity.base.BaseViewDelegate;

/**
 * Created by W~Q on 2017/5/2.
 */

public class WelcomeViewDelegate extends BaseViewDelegate {

    @Override
    public int getLayoutID() {
        return R.layout.activity_welcome;
    }

    @Override
    public void initWidget() {
        super.initWidget();

        setToolbarTitle("");
        setToolbarLeftButton(0, "");
        setToolbarBackgroundColorRes(R.color.transparent);
        setStatusBarColorRes(R.color.transparent);
        setShowStatus(false);
        setShowToolbar(false);
    }
    @Override
    public int getContentBaseRelativeLayout() {
        return 0;
    }
}
