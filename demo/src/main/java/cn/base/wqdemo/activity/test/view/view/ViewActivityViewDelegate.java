package cn.base.wqdemo.activity.test.view.view;

import butterknife.BindView;
import cn.base.wqdemo.R;
import cn.wq.baseActivity.base.BaseViewDelegate;
import cn.wq.baseActivity.view.image.WqBanner;
import cn.wq.baseActivity.view.image.WqImageView;

/**
 * Created by W~Q on 2017/5/2.
 * View视图层，完全移除与Presenter业务逻辑的耦合
 */

public class ViewActivityViewDelegate extends BaseViewDelegate {

    @BindView(R.id.wqBanner)
    public WqBanner wqBanner;
    @BindView(R.id.wqImg)
    WqImageView wqImg;

    @Override
    public int getLayoutID() {
        return R.layout.activity_wq_test_view;
    }

    @Override
    public void initWidget() {
        super.initWidget();

        setToolbarTitle(getClass().getSimpleName());
//        setToolbarLeftButton(R.mipmap.base_back, "");
        setToolbarBackgroundColorRes(R.color.baseColorPrimaryDark);
        setStatusBarColorRes(R.color.baseColorPrimaryDark);
    }

    @Override
    public int getContentBaseRelativeLayout() {
        return 2;
    }

}
