package cn.base.wqdemo.activity.demo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import cn.wq.baseActivity.base.BaseFragment;

/**
 * Created by xt on 2019/12/11.
 */

public class DemoFragment extends BaseFragment<FragmentViewDelegate> {
    @Override
    protected Class<FragmentViewDelegate> getDelegateClass() {
        return FragmentViewDelegate.class;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //默认不显示的
        getViewDelegate().setShowToolbar(true);
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
    }

}
