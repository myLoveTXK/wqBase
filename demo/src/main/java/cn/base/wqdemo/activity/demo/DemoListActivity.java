package cn.base.wqdemo.activity.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.andview.refreshview.utils.LogUtils;
import com.wq.baseRequest.bean.HttpResponseInfoBean;

import java.util.ArrayList;
import java.util.List;

import cn.base.wqdemo.api.ApiGetUserJifen;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.base.ui.list.BaseRecycleListDataActivity;
import cn.wq.baseActivity.view.pullRecycleView.base.BaseViewHolder;
import shinyway.request.interfaces.ProjectRequestCallback;

/**
 * Created by W~Q on 2017/5/2.
 * 列表页
 */

public class DemoListActivity extends BaseRecycleListDataActivity<ActivityListViewDelegate, String> {

    private static final String demoKey = "demoKey";

    String demo;

    @Override
    protected void initData() {
        super.initData();
        demo = getIntent().getStringExtra(demoKey);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startRefresh();
    }


    @Override
    public void onRefresh(final boolean isPullDown) {
        getData(true);
    }

    @Override
    public void onLoadMore(boolean isSilence) {
        getData(false);
    }

    @Override
    protected Class<ActivityListViewDelegate> getDelegateClass() {
        return ActivityListViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        getViewDelegate().setOnToolbarLeftButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        });
    }

    @Override
    public void onViewHolderListener(int viewType, BaseViewHolder baseViewHolder, final String bean, int position) {
        ActivityListViewDelegate.ViewHolder viewHolder = (ActivityListViewDelegate.ViewHolder) baseViewHolder;

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiGetUserJifen apiGetUserJifen = new ApiGetUserJifen(This);
                apiGetUserJifen.request(new ProjectRequestCallback() {
                    @Override
                    public void swSuccess(HttpResponseInfoBean result) {
                        LogUtils.i("wq 1230 result:" + result);
                    }

                    @Override
                    public void swFail(HttpResponseInfoBean result) {
                        LogUtils.i("wq 1230 result:" + result);
                    }
                });
//                ApiGoodsHot apiGoodsHot = new ApiGoodsHot(This);
//                apiGoodsHot.request(new ProjectRequestCallback() {
//                    @Override
//                    public void swSuccess(HttpResponseInfoBean result) {
//                    }
//
//                    @Override
//                    public void swFail(HttpResponseInfoBean result) {
//                    }
//                });
            }
        });
    }

    /**
     * 获取数据设置范例
     *
     * @param isRefresh
     */
    private void getData(final boolean isRefresh) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            strings.add("什么鬼拉看数据地方裂开就---什么鬼拉看数据地方裂开就---什么鬼拉看数据地方裂开就---什么鬼拉看数据地方裂开就---什么鬼拉看数据地方裂开就---" + i);
        }
        setApiData(strings, true);
//        final ApiGetToken apiGetToken = new ApiGetToken(This);
//        apiGetToken.request(new ProjectRequestCallback() {
//            @Override
//            public void swSuccess(String result) {
//                List list = null;
//                setApiData(list, isPullDown);
//            }
//
//            @Override
//            public void swFail(String result) {
//                setApiError(result, isPullDown, apiGetToken);
//            }
//        });
    }

    public static void startActivity(BaseActivity baseActivity, String demo) {
        Intent intent = new Intent();
        intent.putExtra(demoKey, demo);
        baseActivity.startActivity(DemoListActivity.class, intent);
    }

}

