package cn.base.wqdemo.activity.test.view.preseter;

import android.os.Bundle;
import android.view.View;

import com.andview.refreshview.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.base.wqdemo.R;
import cn.base.wqdemo.activity.test.view.view.ViewActivityViewDelegate;
import cn.base.wqdemo.bean.BannerBean;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.util.show.ShowToast;
import cn.wq.baseActivity.view.image.WqBanner;
import cn.wq.baseActivity.view.image.WqImageView;


public class WqViewActivity extends BaseActivity<ViewActivityViewDelegate> {

    @BindView(R.id.wqBanner)
    public WqBanner wqBanner;
    @BindView(R.id.wqImg)
    WqImageView wqImg;
    @Override
    protected void initData() {
        super.initData();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wqBanner.setDesignHeight(300);

        List<BannerBean> bannerBeans = new ArrayList<>();
        bannerBeans.add(new BannerBean("http://img.qct-cg.shinyway.org/2019/12/5/6ee21041-a2ca-4deb-a3fc-d2c5d5e2dd6f.jpeg"));
        bannerBeans.add(new BannerBean("http://img.qct-cg.shinyway.org/2019/11/20/e90f74f5-c208-489c-b9d7-b4276b1cc1b5.jpeg"));
        bannerBeans.add(new BannerBean("http://img.qct-cg.shinyway.org/2019/11/20/e90f74f5-c208-489c-b9d7-b4276b1cc1b5.jpeg"));
        bannerBeans.add(new BannerBean("http://img.qct-cg.shinyway.org/2019/11/18/55f8a817-ea36-4fff-970d-a8d7ee18106c.jpeg"));
        bannerBeans.add(new BannerBean("1"));
        wqBanner.setWqBannerBeanList(bannerBeans);
        wqBanner.setOnItemClick(new WqBanner.OnItemClick() {
            @Override
            public void onItemClick(List<? extends WqBanner.IWqBannerBean> been, WqBanner.IWqBannerBean bean) {
                LogUtils.i("wq 1219 been:" + been);
                LogUtils.i("wq 1219 bean:" + bean);
                LogUtils.i("wq 1219 --------------------------------------");
            }
        });
        wqImg.setRoundAsCircle(true);
        wqImg.setDesignImage("https://11timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1577371476782&di=19dbc7d1594412939410cc9410425ad1&imgtype=0&src=http%3A%2F%2Fimage.biaobaiju.com%2Fuploads%2F20180802%2F03%2F1533152912-BmPIzdDxuT.jpg",220,220);
    }

    @Override
    protected Class<ViewActivityViewDelegate> getDelegateClass() {
        return ViewActivityViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        getViewDelegate().setOnToolbarLeftButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        });
        wqImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowToast.show("wqImg");
            }
        });
        //可以同时对多个控件设置同一个点击事件,后面id参数可以传多个
//        viewDelegate.setOnClickListener(this, R.id.text, R.id.button1, R.id.toast_test, R.id.toast_activity);

    }


}
