package cn.base.wqdemo.activity.welcome.preseter;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import cn.base.wqdemo.R;
import cn.base.wqdemo.activity.welcome.view.FunctionGuideViewDelegate;
import cn.base.wqdemo.cache.AppCache;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.util.anim.ActivityAnimUtil;


public class FunctionGuideActivity extends BaseActivity<FunctionGuideViewDelegate> implements View.OnClickListener {

    int startNumber = 0;
    int maxNumber = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateImg();
    }

    private void updateImg() {
        switch (startNumber) {
            case 1:
                ((ImageView) getView(R.id.defaultImg)).setImageResource(R.mipmap.img_guide_function_1);
                break;
            case 2:
                ((ImageView) getView(R.id.defaultImg)).setImageResource(R.mipmap.img_guide_function_1);
                break;
            case 3:
                ((ImageView) getView(R.id.defaultImg)).setImageResource(R.mipmap.img_guide_function_1);
                break;
        }
    }

    @Override
    protected Class<FunctionGuideViewDelegate> getDelegateClass() {
        return FunctionGuideViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        //可以同时对多个控件设置同一个点击事件,后面id参数可以传多个
        viewDelegate.setOnClickListener(this, R.id.defaultImg);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.defaultImg) {
            startNumber++;
            if (startNumber == maxNumber) {
                AppCache.setFirstFunctionGuide(false);
                finish();
                return;
            }
            updateImg();
        }
    }

    @Override
    public void finish() {
        super.finish();
        ActivityAnimUtil.finishActivityTopToBottom(this);
    }

    public static void startActivity(BaseActivity baseActivity) {
        if (baseActivity.isDestroyedSw()) {
            return;
        }
        if (AppCache.isFirstFunctionGuide()) {
            baseActivity.startActivity(FunctionGuideActivity.class);
            ActivityAnimUtil.startActivityBottomToTop(baseActivity);
        }
    }
}
