package cn.base.wqdemo.activity.welcome.preseter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.base.wqdemo.R;
import cn.base.wqdemo.activity.welcome.view.GuideViewDelegate;
import cn.base.wqdemo.cache.AppCache;
import cn.wq.baseActivity.base.BaseActivity;


public class GuideActivity extends BaseActivity<GuideViewDelegate> {

    public static GuideActivity guideActivity;
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public static final String ARG_SECTION_NUMBER = "section_number";

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private LinearLayout dotsLayout;
    private ViewPager mViewPager;
    private static final int page = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        guideActivity = this;
        super.onCreate(savedInstanceState);
        initView();
    }

    @Override
    protected Class<GuideViewDelegate> getDelegateClass() {
        return GuideViewDelegate.class;
    }


    public void initView() {
        dotsLayout = (LinearLayout) findViewById(R.id.dots);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setDot(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        initDots();
        mViewPager.setAdapter(mSectionsPagerAdapter);
//        initDots();
    }

    private void initDots() {
        LayoutInflater layoutInflater = getLayoutInflater();
        for (int i = 0; i < page; i++) {
            View dot = layoutInflater.inflate(R.layout.item_dot, null);
            dotsLayout.addView(dot);
        }
        int selectDot = 0;
        setDot(selectDot);
    }

    private void setDot(int selectDot) {
//        dotsLayout.setVisibility(View.GONE);
        for (int i = 0; i < dotsLayout.getChildCount(); i++) {
            ImageView dot = (ImageView) dotsLayout.getChildAt(i).findViewById(R.id.dot);
            if (i == selectDot) {
                dot.setImageResource(R.mipmap.icon_guide_dot_color);
            } else {
                dot.setImageResource(R.mipmap.icon_guide_dot_gray);
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    protected class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, position);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return page;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_guide, null);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            final TextView go_home = (TextView) rootView.findViewById(R.id.go_home_btn);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            if (getArguments().getInt(ARG_SECTION_NUMBER) == 0) {
                textView.setBackgroundResource(R.mipmap.img_guide_page_1);
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                textView.setBackgroundResource(R.mipmap.img_guide_page_1);
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
                textView.setBackgroundResource(R.mipmap.img_guide_page_1);
            }
            if (page - 1 == getArguments().getInt(ARG_SECTION_NUMBER)) {
                go_home.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            goHome();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                go_home.setVisibility(View.GONE);
                go_home.setOnClickListener(null);
            }
            return rootView;
        }
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, GuideActivity.class);
        context.startActivity(intent);
    }

    static boolean isStart = false;

    public static void goHome() {
        if (!isStart && guideActivity != null) {
            //TODO 访问首页
//            guideActivity.startActivity(MainActivity.class);
            AppCache.setFirstOpenApp(false);
            isStart = true;
        }
        if (guideActivity != null) {
            guideActivity.finish();
            guideActivity = null;
        }
    }


    @Override
    public String statisticsPageName() {
        return "PageId_Lauch";
    }
}
