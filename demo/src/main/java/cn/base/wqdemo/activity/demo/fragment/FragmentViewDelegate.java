package cn.base.wqdemo.activity.demo.fragment;

import cn.wq.baseActivity.R;
import cn.wq.baseActivity.base.BaseViewDelegate;

/**
 * Created by W~Q on 2017/5/2.
 * View视图层，完全移除与Presenter业务逻辑的耦合
 */

public class FragmentViewDelegate extends BaseViewDelegate {

    @Override
    public int getLayoutID() {
        return 0;
    }

    @Override
    public void initWidget() {
        super.initWidget();

        setToolbarTitle(getClass().getSimpleName());
//        setToolbarLeftButton(R.mipmap.base_back, "");
        setToolbarBackgroundColorRes(R.color.baseColorPrimaryDark);
        setStatusBarColorRes(R.color.baseColorPrimaryDark);
    }

    @Override
    public int getContentBaseRelativeLayout() {
        return 2;
    }

}
