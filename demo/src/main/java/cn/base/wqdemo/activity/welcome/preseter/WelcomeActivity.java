package cn.base.wqdemo.activity.welcome.preseter;

import android.os.Bundle;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cn.base.wqdemo.R;
import cn.base.wqdemo.activity.welcome.view.WelcomeViewDelegate;
import cn.base.wqdemo.cache.AppCache;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.util.StatusUtil;

/**
 * 祝福！~
 * I_Android
 * 此页面为启动闪屏页面，现在有两种做法，静态页面闪过3s，或者动态广告，但是广告加载需要时间
 * 动态广告思路是，先下载加入缓存下次加载的方式，或者对加载显示效果无要求也可以直接加载。
 * 因为是第一个页面，可以在3s之内做一些数据加载操作，但是在3s之内不管数据是否加载完整，
 * 都应该做跳转操作...
 * Created by YichenZ on 2015/9/18 09:54.
 */
public class WelcomeActivity extends BaseActivity<WelcomeViewDelegate> {
//    ImageView defaultImg;

    //判断页面运行时间
    long sendPageTime = System.currentTimeMillis();
    long endPageTime = 0;
    int sc = 1 * 1000;//加载时长
    ScheduledExecutorService scheduledExecutorService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StatusUtil.setFullScreen(true, getView(R.id.defaultImg));


        //页面加载
        jumpPage();
    }

    private void jumpPage() {
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(mRunnable, 0, 1, TimeUnit.SECONDS);
    }

    boolean isStarted = false;
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (endPageTime - sendPageTime >= sc) {
                if (isStarted) {
                    return;
                }
                if (AppCache.isFirstOpenApp()) {//是否首次打开
                    startActivity(GuideActivity.class);
                    finish();
                } else {
                    //TODO 访问首页
//                    startActivity(MainActivity.class);
                    finish();
                }
                isStarted = true;
            } else {
                endPageTime = System.currentTimeMillis();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scheduledExecutorService.shutdown();
    }

    @Override
    protected Class<WelcomeViewDelegate> getDelegateClass() {
        return WelcomeViewDelegate.class;
    }

    @Override
    public String statisticsPageName() {
        return "PageId_Welcome";
    }
}
