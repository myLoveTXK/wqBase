package cn.base.wqdemo.activity.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.interfaces.IUiInterface;


public class DemoActivity extends BaseActivity<ActivityViewDelegate> implements View.OnClickListener {

    private static final String demoKey = "demoKey";

    String demo;

    @Override
    protected void initData() {
        super.initData();
        demo = getIntent().getStringExtra(demoKey);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected Class<ActivityViewDelegate> getDelegateClass() {
        return ActivityViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        getViewDelegate().setOnToolbarLeftButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        });
        //可以同时对多个控件设置同一个点击事件,后面id参数可以传多个
//        viewDelegate.setOnClickListener(this, R.id.text, R.id.button1, R.id.toast_test, R.id.toast_activity);

    }

    @Override
    public void onClick(View v) {
    }

    public static void startActivity(BaseActivity baseActivity, String demo) {
        Intent intent = new Intent();
        intent.putExtra(demoKey, demo);
        baseActivity.startActivity(DemoActivity.class, intent);
    }
}
