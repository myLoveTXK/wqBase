package cn.base.wqdemo.activity.test.tab.view;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import cn.base.wqdemo.R;
import cn.base.wqdemo.view.tab.BottomBarButton;
import cn.base.wqdemo.view.tab.BottomBarTab;
import cn.wq.baseActivity.base.BaseViewDelegate;

/**
 * Created by W~Q on 2017/5/2.
 * View视图层，完全移除与Presenter业务逻辑的耦合
 */

public class DemoTabViewDelegate extends BaseViewDelegate {
    List<BottomBarButton> bottomBarButtons;
    OnSelectListener selectListener;

    @Override
    public int getLayoutID() {
        return R.layout.activity_tab;
    }


    /**
     * 设置监听
     *
     * @param selectListener 选择监听
     */
    public void setSelectListener(final OnSelectListener selectListener) {
        this.selectListener = selectListener;
        if (selectListener == null) {
            return;
        }
        if (bottomBarButtons != null) {
            for (int i = 0; i < bottomBarButtons.size(); i++) {
                final int position = i;

                bottomBarButtons.get(position).setOnSelectListener(new BottomBarButton.TabSelectListener() {
                    @Override
                    public void onSelect(boolean isSelect) {
                        if (isSelect) {
//                            updateSelectPosition(position);
                            selectListener.onSelect(position);
                        }
                    }
                });
            }
        }
    }


    public void updateSelectPosition(int position) {
        if (position == 0) {
            bottomBarButtons.get(0).getBottomLayout().setVisibility(View.INVISIBLE);
            bottomBarButtons.get(0).getOnlyImgLayout().setVisibility(View.VISIBLE);
        } else {
            bottomBarButtons.get(0).getBottomLayout().setVisibility(View.VISIBLE);
            bottomBarButtons.get(0).getOnlyImgLayout().setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 设置选择的tab
     *
     * @param position
     */
    public void setSelectTab(int position) {
        ((BottomBarTab) get(R.id.bar)).setCurrentItem(position);
    }

    @Override
    public void initWidget() {
        super.initWidget();

        setShowToolbar(false);
//        setToolbarTitle("");
//        setToolbarLeftButton(R.mipmap.base_back, "");
//        setToolbarBackgroundColorRes(R.color.baseColorPrimaryDark);
//        setStatusBarColorRes(R.color.baseColorPrimaryDark);

        initBottom();
    }

    /**
     * 初始化底部按钮
     */
    private void initBottom() {
        bottomBarButtons = new ArrayList<>();
//        TextUtils.i
        BottomBarButton index1 = new BottomBarButton(getActivity(), R.drawable.icon_tabbar_index_bg_1, R.string.tab_bottom1);
        bottomBarButtons.add(index1);
        bottomBarButtons.add(new BottomBarButton(getActivity(), R.drawable.icon_tabbar_index_bg_2, R.string.tab_bottom2));
        bottomBarButtons.add(new BottomBarButton(getActivity(), R.drawable.icon_tabbar_index_bg_3, R.string.tab_bottom3));
        bottomBarButtons.add(new BottomBarButton(getActivity(), R.drawable.icon_tabbar_index_bg_4, R.string.tab_bottom4));

        for (BottomBarButton bottomBarButton : bottomBarButtons) {
            ((BottomBarTab) get(R.id.bar)).addItem(bottomBarButton);
        }

    }

    public List<BottomBarButton> getBottomBarButtons() {
        return bottomBarButtons;
    }

    public interface OnSelectListener {
        void onSelect(int selectPosition);
    }

    @Override
    public int getContentBaseRelativeLayout() {
        return 2;
    }

}
