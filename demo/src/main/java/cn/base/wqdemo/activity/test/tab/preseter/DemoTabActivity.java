package cn.base.wqdemo.activity.test.tab.preseter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import cn.base.wqdemo.R;
import cn.base.wqdemo.activity.demo.fragment.DemoListFragment;
import cn.base.wqdemo.activity.demo.tab.DemoTabViewPagerFragment;
import cn.base.wqdemo.activity.demo.tab.fragment.DemoTabFragment;
import cn.base.wqdemo.activity.demo.tab.fragment.DemoTabListFragment;
import cn.base.wqdemo.activity.test.tab.view.DemoTabViewDelegate;
import cn.base.wqdemo.api.ApiSearchGoods;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.BaseFragment;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.util.show.ShowToast;
import shinyway.request.interfaces.ProjectRequestCallback;


/**
 * @author W~Q
 */
public class DemoTabActivity extends BaseActivity<DemoTabViewDelegate> {

    List<BaseFragment> baseFragments = new ArrayList<>();
    Fragment currentFragment;

    @Override
    protected void initData() {
        super.initData();
        LogUtils.i("wq 1212 init");
//        baseFragments.add(0, new DemoFragment());
        baseFragments.add(new DemoListFragment());
        baseFragments.add(new DemoTabFragment());
        baseFragments.add(new DemoTabListFragment());
        baseFragments.add(new DemoTabViewPagerFragment());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectTab(0);
//        final ApiGoodsHot apiGoodsHot = new ApiGoodsHot(This);
//        apiGoodsHot.request(new ProjectRequestCallback() {
//            @Override
//            public void swSuccess(String result) {
//                LogUtils.i("wq 1212 result:" + result);
//                LogUtils.i("wq 1212 apiGoodsHot.getDataBean():" + apiGoodsHot.getDataBean());
//            }
//
//            @Override
//            public void swFail(String result) {
//                LogUtils.i("wq 1212 result:" + result);
//            }
//        });

    }

    @Override
    protected Class<DemoTabViewDelegate> getDelegateClass() {
        return DemoTabViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        getViewDelegate().setOnToolbarLeftButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {

            }
        });
        getViewDelegate().setSelectListener(new DemoTabViewDelegate.OnSelectListener() {
            @Override
            public void onSelect(int selectPosition) {
                selectTab(selectPosition);
            }
        });
        getViewDelegate().setOnToolbarRightButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {

            }
        });
    }

    public void selectTab(final int selectPosition) {
        if (baseFragments != null && selectPosition < baseFragments.size() && selectPosition >= 0) {
            switchContent(baseFragments.get(selectPosition), selectPosition);
        }
    }

    /**
     * 显示内容
     *
     * @param showFragment
     * @param position
     */
    public synchronized void switchContent(BaseFragment showFragment, int position) {
        setShowFragment(position, showFragment);
    }


    public synchronized void setShowFragment(int position, Fragment showFragment) {
        if (currentFragment != showFragment) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (currentFragment != null) {
                transaction.hide(currentFragment);
            }
            if (!showFragment.isAdded()) { // 先判断是否被add过
                transaction.add(R.id.tab_content, showFragment).commitAllowingStateLoss(); // 隐藏当前的fragment，add下一个到Activity中
            } else {
                transaction.show(showFragment).commitAllowingStateLoss(); // 隐藏当前的fragment，显示下一个
            }
            currentFragment = showFragment;
        }
    }

}
