package cn.base.wqdemo.activity.demo.tab;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.androidkun.xtablayout.XTabLayout;

import java.util.ArrayList;
import java.util.List;

import cn.base.wqdemo.R;
import cn.base.wqdemo.activity.demo.tab.fragment.DemoTabFragment;
import cn.base.wqdemo.activity.demo.tab.fragment.DemoTabListFragment;
import cn.wq.baseActivity.base.BaseActivity;
import cn.wq.baseActivity.base.BaseFragment;
import cn.wq.baseActivity.base.interfaces.IUiInterface;
import cn.wq.baseActivity.base.ui.list.adapter.TabFragmentPagerAdapter;


public class DemoTabViewPagerActivity extends BaseActivity<TabViewPagerActivityViewDelegate> implements View.OnClickListener {

    private static final String demoKey = "demoKey";

    String demo;

    @Override
    protected void initData() {
        super.initData();
        demo = getIntent().getStringExtra(demoKey);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initTab();

    }

    private void initTab() {
        TabFragmentPagerAdapter tabFragmentPagerAdapter = new TabFragmentPagerAdapter(getSupportFragmentManager());//getChildFragmentManager 是Fragment使用
        ((ViewPager) getView(R.id.viewPager)).setAdapter(tabFragmentPagerAdapter);//需要先设置adapter

        ((XTabLayout) getView(R.id.tab)).setTabMode(TabLayout.MODE_SCROLLABLE);
        ((XTabLayout) getView(R.id.tab)).setupWithViewPager((ViewPager) getView(R.id.viewPager));

        List<BaseFragment> tabFragments = getBaseFragment();
        ((ViewPager) getView(R.id.viewPager)).setOffscreenPageLimit(tabFragments.size());

        tabFragmentPagerAdapter.addFragments(tabFragments);
    }

    private List<BaseFragment> getBaseFragment() {
        List<BaseFragment> tabFragments = new ArrayList<>();
        tabFragments.add(new DemoTabFragment());
        tabFragments.add(new DemoTabFragment());
        tabFragments.add(new DemoTabListFragment());
        tabFragments.add(new DemoTabListFragment());
        return tabFragments;
    }


    @Override
    protected Class<TabViewPagerActivityViewDelegate> getDelegateClass() {
        return TabViewPagerActivityViewDelegate.class;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
        getViewDelegate().setOnToolbarLeftButtonClickListener(new IUiInterface.BaseToolbarInterface.OnToolbarButtonClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        });
        //可以同时对多个控件设置同一个点击事件,后面id参数可以传多个
//        viewDelegate.setOnClickListener(this, R.id.text, R.id.button1, R.id.toast_test, R.id.toast_activity);

    }

    @Override
    public void onClick(View v) {
    }

    public static void startActivity(BaseActivity baseActivity, String demo) {
        Intent intent = new Intent();
        intent.putExtra(demoKey, demo);
        baseActivity.startActivity(DemoTabViewPagerActivity.class, intent);
    }
}
