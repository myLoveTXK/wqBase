package cn.base.wqdemo.activity.demo.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.androidkun.xtablayout.XTabLayout;

import java.util.ArrayList;
import java.util.List;

import cn.base.wqdemo.R;
import cn.base.wqdemo.activity.demo.tab.fragment.DemoTabFragment;
import cn.base.wqdemo.activity.demo.tab.fragment.DemoTabListFragment;
import cn.wq.baseActivity.base.BaseFragment;
import cn.wq.baseActivity.base.ui.list.adapter.TabFragmentPagerAdapter;

/**
 * Created by xt on 2019/12/11.
 */

public class DemoTabViewPagerFragment extends BaseFragment<TabViewPagerFragmentViewDelegate> {


    @Override
    protected Class<TabViewPagerFragmentViewDelegate> getDelegateClass() {
        return TabViewPagerFragmentViewDelegate.class;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //默认不显示的
        getViewDelegate().setShowToolbar(true);

        initTab();

    }

    private void initTab() {
        TabFragmentPagerAdapter tabFragmentPagerAdapter = new TabFragmentPagerAdapter(getChildFragmentManager());//getChildFragmentManager 是Fragment使用
        ((ViewPager) getView(R.id.viewPager)).setAdapter(tabFragmentPagerAdapter);//需要先设置adapter

        ((XTabLayout) getView(R.id.tab)).setTabMode(TabLayout.MODE_SCROLLABLE);
        ((XTabLayout) getView(R.id.tab)).setupWithViewPager((ViewPager) getView(R.id.viewPager));

        List<BaseFragment> tabFragments = getBaseFragment();
        ((ViewPager) getView(R.id.viewPager)).setOffscreenPageLimit(tabFragments.size());

        tabFragmentPagerAdapter.addFragments(tabFragments);
    }

    private List<BaseFragment> getBaseFragment() {
        List<BaseFragment> tabFragments = new ArrayList<>();
        tabFragments.add(new DemoTabFragment());
        tabFragments.add(new DemoTabFragment());
        tabFragments.add(new DemoTabListFragment());
        tabFragments.add(new DemoTabListFragment());
        return tabFragments;
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
    }

}
