package cn.base.wqdemo.activity.demo.tab.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import cn.wq.baseActivity.base.BaseFragment;
import cn.wq.baseActivity.base.ui.interfaces.ITabFragment;

/**
 * Created by xt on 2019/12/11.
 */

public class DemoTabFragment extends BaseFragment<TabFragmentViewDelegate> implements ITabFragment {
    @Override
    protected Class<TabFragmentViewDelegate> getDelegateClass() {
        return TabFragmentViewDelegate.class;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDelegate().setShowToolbar(true);
    }

    @Override
    protected void bindEvenListener() {
        super.bindEvenListener();
    }

    @Override
    public String getTabTitle() {
        return "DemoTabFragment";
    }
}
