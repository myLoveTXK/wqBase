package com.wq.baseRequest.bean;

import okhttp3.Headers;

/**
 * http请求，返回参数
 */
public class HttpResponseInfoBean {
    String requestUrl;
    int httpStatusCode;
    Headers httpHeads;
    String httpMessage;
    String httpBody;

    /**
     * 复制了OkHttp 对于成功的判断
     *
     * @return
     */
    public boolean isSuccessful() {
        return httpStatusCode >= 200 && httpStatusCode < 300;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Headers getHttpHeads() {
        return httpHeads;
    }

    public void setHttpHeads(Headers httpHeads) {
        this.httpHeads = httpHeads;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getHttpMessage() {
        return httpMessage;
    }

    public void setHttpMessage(String httpMessage) {
        this.httpMessage = httpMessage;
    }

    public String getHttpBody() {
        return httpBody;
    }

    public void setHttpBody(String httpBody) {
        this.httpBody = httpBody;
    }


    public static HttpResponseInfoBean createBean(String url, int code, Headers headers, String message, String body) {
        HttpResponseInfoBean httpResponseInfoBean = new HttpResponseInfoBean();
        httpResponseInfoBean.setRequestUrl(url);
        httpResponseInfoBean.setHttpStatusCode(code);
        httpResponseInfoBean.setHttpHeads(headers);
        httpResponseInfoBean.setHttpMessage(message);
        httpResponseInfoBean.setHttpBody(body);
        return httpResponseInfoBean;
    }

    @Override
    public String toString() {
        return "HttpResponseInfoBean{" +
                "requestUrl='" + requestUrl + '\'' +
                ", httpStatusCode=" + httpStatusCode +
                ", httpHeads=" + httpHeads +
                ", httpMessage='" + httpMessage + '\'' +
                ", httpBody='" + httpBody + '\'' +
                '}';
    }
}
