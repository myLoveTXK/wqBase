package com.wq.baseRequest.bean.enums;

public enum HttpType {
    UNKNOW(-1), POST(0), GET(1), PUT(2) , UPLOAD(3);

    private int type;

    private HttpType(int type) {
        this.type = type;
    }

    public int getValue() {
        return type;
    }

}
