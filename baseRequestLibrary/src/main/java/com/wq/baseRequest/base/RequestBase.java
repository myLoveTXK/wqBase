package com.wq.baseRequest.base;

import android.content.Context;

import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.bean.HttpResponseInfoBean;
import com.wq.baseRequest.interfaces.HttpRequestCallback;
import com.wq.baseRequest.interfaces.RequestProcessStateListener;
import com.wq.baseRequest.utils.ShowProcessDialog;


/**
 * Created by W~Q on 2016/9/26.
 * api访问配置类
 * 准备基础 数据 （配置和基础显示状态等 绝大多数api能用到的配置信息 都可以在这里配置）
 */
public abstract class RequestBase {
    protected boolean isDebug = true;//调试状态下使用
    protected boolean isNeedApiLog = true;//是否需要API 日誌信息
    protected Context context;
    protected boolean isNeedLoading = false;//默认不需要加载框
    protected ShowProcessDialog mProcessDialog;
    private String loadingTitle = "";

    private RequestBase seriesRequestBases;//串联的api
    private boolean isRequestSuccess;

    protected abstract void requestApi(HttpRequestCallback callback, RequestProcessStateListener requestProcessStateListener);

    public Context getContext() {
        return context;
    }

    protected abstract String apiName();//当前请求api名字，便于开发查找，无特殊逻辑意义

    public RequestBase(Context context) {
        this.context = context;
        if (context != null) {
            mProcessDialog = new ShowProcessDialog(context);
            mProcessDialog.setTitle(loadingTitle);
        }
    }

    protected void requestSuccess(HttpResponseInfoBean result) {
        isRequestSuccess = true;
    }

    protected void requestFail(HttpResponseInfoBean result) {
        isRequestSuccess = false;
    }

    /**
     * 是否能操作
     *
     * @param cancelable
     */
    public void setProcessCancelable(boolean cancelable) {
        if (mProcessDialog != null) {
            mProcessDialog.setCancelable(cancelable);
        }
    }

    /**
     * 发起请求
     *
     * @param callback
     */
    protected void baseRequest(HttpRequestCallback callback) {
        requestApi(callback, new RequestProcessStateListener() {
            @Override
            public void requestStartCallback(String url) {
                addStartLog(url);
                showProgress();
            }

            @Override
            public void requestStopCallback(String url) {
                addStopLog(url);
                if ((seriesRequestBases == null
                        || !seriesRequestBases.isNeedLoading()
                        || seriesRequestBases.getmProcessDialog() == null)) {
                    dismissProgress();
                }

            }
        });

    }

    /**
     * 增加开始Log
     */
    private void addStartLog(String url) {
        if (isNeedApiLog) {
            LogUtils.i("wq API开始：" + apiName() + "开始:" + url);
        }
    }

    /**
     * 增加结束log
     */
    private void addStopLog(String url) {
        if (isNeedApiLog) {
            LogUtils.i("wq API结束：" + apiName() + "结束:" + url);
        }
    }

//    public Context getActivity() {
//        return context;
//    }

    /**
     * 是否需要加载等待框
     */
    public void isNeedLoading(boolean isNeedLoading) {
        this.isNeedLoading = isNeedLoading;
    }

    public void showProgress() {
        try {
            if (isNeedLoading) {
                if (mProcessDialog != null && !mProcessDialog.isShowing()) {
                    mProcessDialog.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissProgress() {
        try {
            if (isNeedLoading) {
                if (mProcessDialog != null) {//如果没有串联，或者串联的API不需要loading
                    mProcessDialog.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isNeedLoading() {
        return isNeedLoading;
    }

    public void setProgressContent(String content) {
        if (mProcessDialog != null) {
            mProcessDialog.setTitle(content);
        }
    }

    public RequestBase getSeriesRequestBases() {
        return seriesRequestBases;
    }

    public void setSeriesRequestBases(RequestBase seriesRequestBases) {
        this.seriesRequestBases = seriesRequestBases;
        if (this.seriesRequestBases != null) {
            this.seriesRequestBases.setmProcessDialog(getmProcessDialog());
        }
    }

    public ShowProcessDialog getmProcessDialog() {
        return mProcessDialog;
    }

    public void setmProcessDialog(ShowProcessDialog mProcessDialog) {
        this.mProcessDialog = mProcessDialog;
    }
}
