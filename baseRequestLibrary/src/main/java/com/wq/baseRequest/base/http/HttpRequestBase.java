package com.wq.baseRequest.base.http;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.bean.HttpResponseInfoBean;
import com.wq.baseRequest.interfaces.HttpRequestCallback;
import com.wq.baseRequest.interfaces.RequestProcessStateListener;
import com.wq.baseRequest.utils.NetworkUtil;

import java.util.Map;


/**
 * Created by W~Q on 2016/9/26.
 * http 访问基础配置和返回处理方式   （包括：url配置，日志）
 */
public abstract class HttpRequestBase extends HttpRequestModeBase {

    private final int TAG_SUCCESS = 30001;
    private final int TAG_FAIL = 30002;
    private final String noNetworkConnection = "没有连接网络";

    protected abstract void parseDataBackground(String result) throws Exception;//background解析和处理数据


    public HttpRequestBase(Context context) {
        super(context);
    }


    /**
     * 请求API
     *
     * @param requestCallback             api结果返回
     * @param requestProcessStateListener api过程状态返回
     */
    @Override
    protected void requestApi(final HttpRequestCallback requestCallback, final RequestProcessStateListener requestProcessStateListener) {
        requestProcessStateListener.requestStartCallback(getUrl());
        if (!NetworkUtil.isNetworkAvailable(context)) {
            HttpResponseInfoBean httpResponseInfoBean = HttpResponseInfoBean.createBean(getUrl(), TAG_FAIL, null, noNetworkConnection, noNetworkConnection);
            addApiLog(false, httpResponseInfoBean);
            DataBoundCallback dataBoundCallback = new DataBoundCallback(httpResponseInfoBean, requestCallback, requestProcessStateListener);
            callbackUI(TAG_FAIL, dataBoundCallback);
            return;
        }
        httpRequestMode(new HttpRequestCallback() {

            @Override
            public void httpSuccess(final HttpResponseInfoBean httpResponseInfoBean) {

                final String result = httpResponseInfoBean.getHttpBody();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            addApiLog(true, httpResponseInfoBean);
                            parseDataBackground(result);
                            DataBoundCallback dataBoundCallback = new DataBoundCallback(httpResponseInfoBean, requestCallback, requestProcessStateListener);
                            callbackUI(TAG_SUCCESS, dataBoundCallback);
                        } catch (Exception e) {
                            addApiLog(false, httpResponseInfoBean);
                            e.printStackTrace();
                            DataBoundCallback dataBoundCallback = new DataBoundCallback(httpResponseInfoBean, requestCallback, requestProcessStateListener);
                            callbackUI(TAG_FAIL, dataBoundCallback);
                        }
                    }
                }).start();
            }

            @Override
            public void httpFail(HttpResponseInfoBean httpResponseInfoBean) {

                addApiLog(false, httpResponseInfoBean);
                try {
                    String result = httpResponseInfoBean.getHttpBody();
                    parseDataBackground(httpResponseInfoBean.getHttpBody());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DataBoundCallback dataBoundCallback = new DataBoundCallback(httpResponseInfoBean, requestCallback, requestProcessStateListener);
                callbackUI(TAG_FAIL, dataBoundCallback);
            }
        });
    }

    private void callbackUI(int successTag, DataBoundCallback dataBoundCallback) {
        uiHandler.sendMessage(uiHandler.obtainMessage(successTag, dataBoundCallback));
    }

    Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DataBoundCallback dataBoundCallback = (DataBoundCallback) msg.obj;
            HttpRequestCallback callback = dataBoundCallback.getRequestCallback();
            HttpResponseInfoBean httpResponseInfoBean = dataBoundCallback.getHttpResponseInfoBean();
            if (callback != null) {
                if (msg.what == TAG_SUCCESS) {
                    requestSuccess(httpResponseInfoBean);
                    callback.httpSuccess(dataBoundCallback.httpResponseInfoBean);
                } else if (msg.what == TAG_FAIL) {
                    requestFail(httpResponseInfoBean);
                    callback.httpFail(dataBoundCallback.httpResponseInfoBean);
                }
            }
            dataBoundCallback.getRequestProcessStateListener().requestStopCallback(dataBoundCallback.getHttpResponseInfoBean().getRequestUrl());
        }
    };

    private class DataBoundCallback {
        private HttpResponseInfoBean httpResponseInfoBean;
        private HttpRequestCallback requestCallback;
        private RequestProcessStateListener requestProcessStateListener;

        public DataBoundCallback(HttpResponseInfoBean httpResponseInfoBean, HttpRequestCallback requestCallback, RequestProcessStateListener requestProcessStateListener) {
            this.httpResponseInfoBean = httpResponseInfoBean;
            this.requestCallback = requestCallback;
            this.requestProcessStateListener = requestProcessStateListener;
        }


        public HttpResponseInfoBean getHttpResponseInfoBean() {
            return httpResponseInfoBean;
        }

        public HttpRequestCallback getRequestCallback() {
            return requestCallback;
        }


        public RequestProcessStateListener getRequestProcessStateListener() {
            return requestProcessStateListener;
        }
    }

    /**
     * 增加日志信息
     *
     * @param isSuccess
     * @param httpResponseInfoBean
     */
    private void addApiLog(boolean isSuccess, HttpResponseInfoBean httpResponseInfoBean) {
        if (isNeedApiLog) {
            try {
                if (!getUrl().equals(httpResponseInfoBean.getRequestUrl())) {
                    LogUtils.i("wq API成功 " + apiName() + "访问成功内置更换原始url：" + getUrl());
                }
                if (isSuccess) {
                    LogUtils.i("wq API 成功 " + apiName() + " 访问成功url：" + httpResponseInfoBean.getRequestUrl());
                    Map<String, String> parm = getParmLog();
                    LogUtils.i("wq API 成功 " + apiName() + " 访问成功 param：" + parm);
                    if (isNeedResponseHeadLog()) {
                        LogUtils.i("wq API 成功 " + apiName() + " 访问成功信息 head：" + httpResponseInfoBean.getHttpHeads());
                    }

                    LogUtils.i("wq API 成功 " + apiName() + " 访问成功信息 message：" + httpResponseInfoBean.getHttpMessage());
                    LogUtils.i("wq API 成功 " + apiName() + " 访问成功信息 body：" + httpResponseInfoBean.getHttpBody());
                    LogUtils.i("wq API 成功 " + apiName() + " --------------------------------------------------------------------------------------------------------------------------------------------");
                } else {
                    LogUtils.e("wq API 失败 " + apiName() + " 访问失败url：" + httpResponseInfoBean.getRequestUrl());
                    Map<String, String> parm = getParmLog();
                    LogUtils.e("wq API 失败 " + apiName() + " 访问失败getParam：" + parm);

                    if (isNeedResponseHeadLog()) {
                        LogUtils.e("wq API 失败 " + apiName() + " 访问失败信息 head：" + httpResponseInfoBean.getHttpHeads());
                    }
                    LogUtils.e("wq API 失败 " + apiName() + " 访问失败信息 message：" + httpResponseInfoBean.getHttpMessage());
                    LogUtils.e("wq API 失败 " + apiName() + " 访问失败信息 body：" + httpResponseInfoBean.getHttpBody());
                    LogUtils.e("wq API 失败 " + apiName() + " --------------------------------------------------------------------------------------------------------------------------------------------");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 是否显示response头log
     *
     * @return
     */
    public boolean isNeedResponseHeadLog() {
        return false;
    }

    /**
     * 参数日志信息子类实现
     *
     * @return
     * @throws Exception
     */
    protected abstract Map<String, String> getParmLog() throws Exception;

}
