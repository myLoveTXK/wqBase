package com.wq.baseRequest.base.http;

import android.content.Context;

import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.base.RequestBase;
import com.wq.baseRequest.bean.HttpResponseInfoBean;
import com.wq.baseRequest.bean.enums.HttpType;
import com.wq.baseRequest.interfaces.HttpRequestCallback;
import com.wq.baseRequest.utils.request.interfaces.OkHttpCallback;
import com.wq.baseRequest.utils.request.okhttp.WqOkHttp;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by W~Q on 2016/6/2.
 * 当前继承类主要实现api的访问方式，以后如果需要更换请求方式，只需修改当前类的代码即可
 * 当前类处理了失败逻辑，成功逻辑还需要判断数据返回状态码，所以子类解析数据时候进行处理
 */
public abstract class HttpRequestModeBase extends RequestBase {

    private WqOkHttp wqOkHttp;

    /**
     * 子类实现http请求类型
     *
     * @return
     */
    protected abstract HttpType getHttpType();

    protected abstract String getUrl();//获取url

    protected abstract Map<String, String> getParam() throws Exception;//获取参数

    protected abstract String getStringBodyParam() throws Exception;//获取参数


    public HttpRequestModeBase(Context context) {
        super(context);
        wqOkHttp = new WqOkHttp();
    }

    public WqOkHttp getWqOkHttp() {
        return wqOkHttp;
    }

    protected void httpRequestMode(final HttpRequestCallback requestCallback) {
        if (getHttpType() == HttpType.GET) {
            get(requestCallback);
        } else if (getHttpType() == HttpType.POST) {
            post(requestCallback);
        } /*else if(getHttpType() == HttpType.UPLOAD){
            post(requestCallback);
        } */ else {
            LogUtils.i("wq 0401 未知http请求处理类型");
        }
    }

    /**
     * 是否body传递Str
     *
     * @return
     */
    protected boolean isStringBodyParam() {
        return true;
    }

    protected Map getHeadMap() {
        Map map = new HashMap();
        return map;
    }

    protected void get(final HttpRequestCallback requestCallback) {


        final String url = getUrl();
        Map<String, String> param = null;
        try {
            param = getParam();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String strParam = "";
        try {
            strParam = getStringBodyParam();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        wqOkHttp.setHeadMap(getHeadMap());
        wqOkHttp.setStringBodyParam(isStringBodyParam());
        wqOkHttp.getData(url, param, strParam, new HttpCallback(requestCallback));
    }


    class HttpCallback implements OkHttpCallback {

        HttpRequestCallback requestCallback;

        public HttpCallback(HttpRequestCallback requestCallback) {
            this.requestCallback = requestCallback;
        }

        @Override
        public void onHttpResult(HttpResponseInfoBean httpResponseInfoBean) {
            if (requestCallback != null) {
                if (httpResponseInfoBean.isSuccessful()) {
                    requestCallback.httpSuccess(httpResponseInfoBean);
                } else {
                    requestCallback.httpFail(httpResponseInfoBean);
                }
            }
        }

//            @Override
//            public void successOkHttpCode(GttpResponseInfoBean) {
//                if (requestCallback != null) {
//                    requestCallback.httpSuccess(url, result);
//                }
//            }
//
//
//            @Override
//            public void failOkHttpCode(String url, String code, String msg, String body) {
//                if (requestCallback != null) {
//                    requestCallback.httpFail(url, code, msg, body);
//                }
//            }
    }

    ;

    private void post(final HttpRequestCallback requestCallback) {
        final String url = getUrl();
        Map<String, String> parm = null;
        try {
            parm = getParam();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String strParam = "";
        try {
            strParam = getStringBodyParam();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        wqOkHttp.setHeadMap(getHeadMap());
        wqOkHttp.setStringBodyParam(isStringBodyParam());
        wqOkHttp.postData(url, parm, strParam, new HttpCallback(requestCallback));
    }
}
