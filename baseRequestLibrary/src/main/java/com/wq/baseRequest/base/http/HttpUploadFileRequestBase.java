package com.wq.baseRequest.base.http;

import android.content.Context;

import com.wq.baseRequest.bean.HttpResponseInfoBean;
import com.wq.baseRequest.bean.enums.HttpType;
import com.wq.baseRequest.interfaces.HttpRequestCallback;
import com.wq.baseRequest.interfaces.HttpUploadLoadingCallback;
import com.wq.baseRequest.utils.request.upload.UploadUtil;

import java.util.Map;


/**
 * Created by W~Q on 2016/11/07.
 * 上传文件
 */
public abstract class HttpUploadFileRequestBase extends HttpRequestBase {

    protected abstract boolean isNeedProgress();

    protected abstract Map<String, String> getMapParam() throws Exception;

    protected HttpUploadLoadingCallback httpUploadLoadingCallback;
    //    protected abstract String getPath();
//    protected abstract void onStart();
//
//    protected abstract void onLoading(long total, long current, boolean isUploading);

    private String path;

    public HttpUploadFileRequestBase(Context context, String path) {
        super(context);
        this.path = path;
    }

    @Override
    protected Map<String, String> getParam() {//这里预留一层对于公参处理，比如加密，手机设备号等公参
        Map<String, String> map = null;
        try {
            map = getMapParam();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //预留位置
        return map;
    }

    @Override
    protected Map<String, String> getParmLog() throws Exception {
        return getParam();
    }

    @Override
    protected HttpType getHttpType() {
        return HttpType.UPLOAD;
    }

    public void upload(final HttpRequestCallback requestCallback) {
        super.baseRequest(requestCallback);
    }

    public void upload(final HttpRequestCallback requestCallback, HttpUploadLoadingCallback httpUploadLoadingCallback) {
        super.baseRequest(requestCallback);
        this.httpUploadLoadingCallback = httpUploadLoadingCallback;
    }

    @Override
    protected void httpRequestMode(final HttpRequestCallback requestCallback) {
        Map<String, String> parm = null;
        try {
            parm = getParam();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String url = getUrl();
        boolean isNeedProgress = isNeedProgress();
        UploadUtil.uploadFile(context, url, parm, path, new UploadUtil.UploadCallback() {
            @Override
            public void onStart() {
                if (httpUploadLoadingCallback != null) {
                    httpUploadLoadingCallback.onStart();
                }
            }

            @Override
            public void onLoading(long total, long current, boolean isUploading) {
                if (httpUploadLoadingCallback != null) {
                    httpUploadLoadingCallback.onLoading(total, current, isUploading);
                }
            }

            @Override
            public void success(String result) {
                try {
                    parseDataBackground(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (requestCallback != null) {
                    HttpResponseInfoBean httpResponseInfoBean = HttpResponseInfoBean.createBean(url, 200, null, result, result);
                    requestCallback.httpSuccess(httpResponseInfoBean);
                }
            }

            @Override
            public void fail(String msg) {
                if (requestCallback != null) {
                    HttpResponseInfoBean httpResponseInfoBean = HttpResponseInfoBean.createBean(url, 500, null, msg, msg);
                    requestCallback.httpFail(httpResponseInfoBean);
                }
            }
        }, isNeedProgress);
    }


}
