package com.wq.baseRequest.utils.request.interfaces;

import okhttp3.Request;

/**
 * Created by W~Q on 2017/9/1.
 * 请求开始之前监听
 */

public interface IRequestBeforeStartListener {
    void beforeStar(Request.Builder requestBuilder);
}
