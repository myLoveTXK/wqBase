package com.wq.baseRequest.utils.request.okhttp;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.lidroid.xutils.util.LogUtils;
import com.wq.baseRequest.bean.HttpResponseInfoBean;
import com.wq.baseRequest.bean.enums.HttpType;
import com.wq.baseRequest.utils.request.interfaces.IRequestBeforeStartListener;
import com.wq.baseRequest.utils.request.interfaces.OkHttpCallback;
import com.wq.baseRequest.utils.request.okhttp.interfaces.UpdateDomainInterface;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.wq.baseRequest.utils.request.okhttp.DomainManager.domainList;

/**
 * Created by W~Q on 2016/8/21.
 * okHttp封装工具类,使用单例模式
 */
public class WqOkHttp {

    public static boolean isDebug;

    private boolean isStringBodyParam = true;//String 特殊传递body


    public final static int CONNECT_TIMEOUT = 20;
    public final static int READ_TIMEOUT = 20;
    public final static int WRITE_TIMEOUT = 20;

    private static OkHttpClient client;
    private static UpdateDomainInterface updateDomainInterface;

    protected OkHttpCallback callback;
    public static int CLIENT_FAIL_CODE = -8001;
    public static int PARSE_FAIL_CODE = -8002;
    public static String CLIENT_FAIL = "访问失败";
    public static String PARSE_FAIL = "装载数据失败";


    private IRequestBeforeStartListener iRequestBeforeStartListener;

    public void setStringBodyParam(boolean stringBodyParam) {
        isStringBodyParam = stringBodyParam;
    }

    public boolean isStringBodyParam() {
        return isStringBodyParam;
    }

    public static void setUpdateDomainInterface(UpdateDomainInterface updateDomainInterface) {
        WqOkHttp.updateDomainInterface = updateDomainInterface;
    }

//    private Map<String, String> headMap = new HashMap<>();
//
//    public void setHeadMap(Map<String, String> headMap) {
//        this.headMap.clear();
//        this.headMap.putAll(headMap);
//    }

    public WqOkHttp() {
        client = new OkHttpClient.Builder()
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Chain chain) throws IOException {
//
//                        Request.Builder builder = chain.request().newBuilder();
//                        for (String key : headMap.keySet()) {
//                            String value = headMap.get(key);
//                            builder.header(key, value);
//
//                        }
////                        LogUtils.i("wq 1226 添加头 " + headMap);
//                        Request build = builder.build();
//
//                        return chain.proceed(build);
//                    }
//                })
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    public void setiRequestBeforeStartListener(IRequestBeforeStartListener iRequestBeforeStartListener) {
        this.iRequestBeforeStartListener = iRequestBeforeStartListener;
    }

    /**
     * post请求,兼容多个域名
     *
     * @param url                    URL
     * @param parameter
     * @param callback
     * @param isNeedSwitchManyDomain
     */
    private void postDataManyDomain(final HttpType httpType, final String url, final Map<String, String> parameter, final String strBody, final OkHttpCallback callback, final boolean isNeedSwitchManyDomain, final String originalUrl, final int position) {
        this.callback = callback;
        try {
            Request request = null;
            if (isStringBodyParam()) {
                RequestBody requestBody = getBotyBuilder(strBody);
                final HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

                Request.Builder requestBuilder = getRequest(httpType, parameter, requestBody, urlBuilder);
                if (iRequestBeforeStartListener != null) {
                    iRequestBeforeStartListener.beforeStar(requestBuilder);
                }
                request = requestBuilder.build();
            } else {
                FormBody.Builder builder = getBuilder(parameter);
                final HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

                Request.Builder requestBuilder = getRequest(httpType, parameter, builder, urlBuilder);
                if (iRequestBeforeStartListener != null) {
                    iRequestBeforeStartListener.beforeStar(requestBuilder);
                }
                request = requestBuilder.build();
            }


            if (isNeedSwitchManyDomain) {
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        clientOnFailure(e);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (updateDomainInterface != null) {
                            String domain = getUrlDomain(url);
                            if (!TextUtils.isEmpty(domain)) {
                                updateDomainInterface.updateDomain(domain);
                            }
                        }
                        callbackData(url, response, callback);
                    }

                    /**
                     * 连接失败，递归重新选择
                     * @param e
                     */
                    private void clientOnFailure(Exception e) {
                        LogUtils.e("wq 0807 url访问失败：" + url);
                        if (TextUtils.isEmpty(url) || position >= domainList.size()) {
                            for (String domain : domainList) {
                                if (originalUrl.contains(domain)) {
                                    if (updateDomainInterface != null) {
                                        updateDomainInterface.updateDomain(domain);
                                    }
                                    break;
                                }
                            }
                            LogUtils.e("wq 0807 返回访问失败old：" + url);
                            callFailure(url, e, callback);
                            return;
                        }
                        String oldDomain = getUrlDomain(url);
                        if (TextUtils.isEmpty(oldDomain)) {
                            LogUtils.e("wq 0807 返回访问失败old：" + url);
                            callFailure(url, e, callback);
                            return;
                        }
                        String newDomain = DomainManager.domainList.get(position);
                        String newUrl = url.replace(oldDomain, newDomain);//更换成新的域名
                        LogUtils.e("wq 0807 切换新的newUrl：" + newUrl);
                        postDataManyDomain(httpType, newUrl, parameter, strBody, callback, isNeedSwitchManyDomain, originalUrl, position + 1);
                    }

                });
            } else {
                client.newCall(request).enqueue(getResponseCallback(url, callback));
            }
        } catch (Exception e) {
            e.printStackTrace();
            callFailure(url, e, callback);
//            callback.failOkHttpCode(url, CLIENT_FAIL_CODE, CLIENT_FAIL, CLIENT_FAIL);
        }
    }

    private Request.Builder getRequest(HttpType httpType, Map<String, String> parameter, FormBody.Builder builder, HttpUrl.Builder urlBuilder) {
        Request.Builder requestBuilder;
        if (httpType == HttpType.GET) {
            if (parameter != null) {
                for (String key : parameter.keySet()) {
                    urlBuilder.addQueryParameter(key, parameter.get(key));
                }
            }
            requestBuilder = new Request.Builder()
                    .get()
                    .url(urlBuilder.build());
//                    .build();
        } else {
            requestBuilder = new Request.Builder()
                    .url(urlBuilder.build())
                    .post(builder.build());
//                    .build();
        }
        return requestBuilder;
    }

    private Request.Builder getRequest(HttpType httpType, Map<String, String> parameter, RequestBody builder, HttpUrl.Builder urlBuilder) {
        Request.Builder requestBuilder;
        if (httpType == HttpType.GET) {
            if (parameter != null) {
                for (String key : parameter.keySet()) {
                    urlBuilder.addQueryParameter(key, parameter.get(key));
                }
            }
            requestBuilder = new Request.Builder()
                    .get()
                    .url(urlBuilder.build());
//                    .build();
        } else {
            requestBuilder = new Request.Builder()
                    .url(urlBuilder.build())
                    .post(builder);
//                    .build();
        }
        return requestBuilder;
    }

    @NonNull
    private String getUrlDomain(String url) {
        String domain = "";
        for (String dm : domainList) {//取出url的域名
            if (url.contains(dm)) {
                domain = dm;
                break;
            }
        }
        return domain;
    }

    /**
     * get请求
     *
     * @param url      URL
     * @param callback 成功 失败callback
     */
    public void getData(String url, final Map<String, String> map, String strBody, final OkHttpCallback callback, boolean isNeedSwitchManyDomain) {
        if (isDebug) {
            isNeedSwitchManyDomain = false;
        }
        postDataManyDomain(HttpType.GET, url, map, strBody, callback, isNeedSwitchManyDomain, url, 0);
    }

    public void getData(String url, final Map<String, String> map, String strBody, final OkHttpCallback callback) {
        getData(url, map, strBody, callback, true);
    }

    /**
     * post请求
     *
     * @param url       URL
     * @param parameter
     * @param strBody
     * @param callback
     */
    public void postData(String url, Map<String, String> parameter, String strBody, final OkHttpCallback callback) {
        postData(url, parameter, strBody, callback, true);
    }

    public void postData(String url, Map<String, String> parameter, String strBody, final OkHttpCallback callback, boolean isNeedSwitchManyDomain) {
        if (isDebug) {
            isNeedSwitchManyDomain = false;
        }
        postDataManyDomain(HttpType.POST, url, parameter, strBody, callback, isNeedSwitchManyDomain, url, 0);
    }


    /**
     * 设置post参数
     *
     * @param parameter
     * @return
     */
    @NonNull
    private FormBody.Builder getBuilder(Map<String, String> parameter) {

        FormBody.Builder builder = new FormBody.Builder();
        if (parameter != null) {
            for (String key : parameter.keySet()) {
                try {
                    builder.add(key, parameter.get(key));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        return builder;
    }

    private RequestBody getBotyBuilder(String strBody) {
        if (TextUtils.isEmpty(strBody)) {
            strBody = "";
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//        MediaType JSON = MediaType.parse("text/pain; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, strBody);

        return body;
    }

    @NonNull
    private Callback getResponseCallback(final String url, final OkHttpCallback callback) {
        return new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callFailure(url, e, callback);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                callbackData(url, response, callback);
            }
        };
    }

    private void callFailure(String url, Exception e, OkHttpCallback callback) {
        if (e != null) {
            e.printStackTrace();
        }
        if (callback != null) {
            HttpResponseInfoBean httpResponseInfoBean = new HttpResponseInfoBean();
            httpResponseInfoBean.setRequestUrl(url);
            httpResponseInfoBean.setHttpHeads(null);
            httpResponseInfoBean.setHttpStatusCode(CLIENT_FAIL_CODE);
            httpResponseInfoBean.setHttpMessage(CLIENT_FAIL);
            httpResponseInfoBean.setHttpBody(CLIENT_FAIL);
            callback.onHttpResult(httpResponseInfoBean);
        }
    }

    private void callbackData(String url, Response response, OkHttpCallback callback) {
        try {
            Headers headers = response.headers();
            int code = response.code();
            String message = response.message();
            String body = response.body().string();

            HttpResponseInfoBean httpResponseInfoBean = new HttpResponseInfoBean();
            httpResponseInfoBean.setRequestUrl(url);
            httpResponseInfoBean.setHttpHeads(headers);
            httpResponseInfoBean.setHttpStatusCode(code);
            httpResponseInfoBean.setHttpMessage(message);
            httpResponseInfoBean.setHttpBody(body);

            if (callback != null) {
                callback.onHttpResult(httpResponseInfoBean);
            }
//            if (response.isSuccessful()) {
//                callback.successOkHttpCode(url, body);
//            } else {
//                callback.failOkHttpCode(url, response.code() + "", response.message(), body);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            HttpResponseInfoBean httpResponseInfoBean = new HttpResponseInfoBean();
            httpResponseInfoBean.setRequestUrl(url);
            httpResponseInfoBean.setHttpHeads(null);
            httpResponseInfoBean.setHttpStatusCode(PARSE_FAIL_CODE);
            httpResponseInfoBean.setHttpMessage(PARSE_FAIL);
            httpResponseInfoBean.setHttpBody(PARSE_FAIL);
            if (callback != null) {
                callback.onHttpResult(httpResponseInfoBean);
            }
        } finally {
            try {
                response.body().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
