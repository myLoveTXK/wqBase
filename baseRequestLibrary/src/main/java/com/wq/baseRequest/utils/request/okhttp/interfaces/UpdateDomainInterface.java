package com.wq.baseRequest.utils.request.okhttp.interfaces;

/**
 * Created by xt on 2017/8/3.
 */

public interface UpdateDomainInterface {
    void updateDomain(String domain);
}
