package com.wq.baseRequest.utils.request.upload;

import android.app.ProgressDialog;
import android.content.Context;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.util.PreferencesCookieStore;
import com.wq.baseRequest.utils.ShowProcessDialog;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;


/**
 * Created by Administrator on 2016/11/7.
 */
public class UploadUtil {


    public static void uploadFile(final Context context, String url, Map<String, String> parm, String path, final UploadCallback callback, final boolean isNeedProgress) {
        RequestParams params = new RequestParams();
//        params.addBodyParameter("msg", imgtxt.getText().toString());
//        params.addHeader("Cookie", CookieUtil.getCookie());
        if (parm != null) {
            for (String key : parm.keySet()) {
                params.addBodyParameter(key, parm.get(key));
            }
        }
        File file = new File(path);
        LogUtils.i("wq 0522 file:" + file);
        LogUtils.i("wq 0522 path:" + path);
        params.addBodyParameter("file", file);


        HttpUtils http = new HttpUtils(30 * 1000);

        PreferencesCookieStore cookieStore = new PreferencesCookieStore(context);
        cookieStore.clear();

        LogUtils.i("wq 0522 原始url:" + url);

        try {
            url = URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//        url = url.replace("_", "%5F");
        LogUtils.i("wq 0522 最后 url:" + url);

        http.send(HttpRequest.HttpMethod.POST, url, params, new RequestCallBack<String>() {
            ProgressDialog progressUploading;
            ShowProcessDialog processSave;

            @Override
            public void onStart() {
                progressUploading = new ProgressDialog(context);
                processSave = new ShowProcessDialog(context);
                processSave.setTitle("正在保存...");
//                progressUploading.setTitle("上传");
                progressUploading.setMessage("当前上传进度");
//                        progressUploading.setIcon(R.drawable.awu_service_icon);
                progressUploading.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressUploading.setMax(100);
                if (isNeedProgress) {
                    progressUploading.show();
                }
                if (callback != null) {
                    callback.onStart();
                }
            }

            @Override
            public void onLoading(long total, long current, boolean isUploading) {
                LogUtils.i("wq 1107 上传文件 current：" + current);
                LogUtils.i("wq 1107 上传文件 total：" + total);
                LogUtils.i("wq 1107 上传文件 isUploading：" + isUploading);
                LogUtils.i("wq 1107 上传文件 --------------------------------------------------");

                int percentage = (int) (((double) current / (double) total) * 100.00);

                progressUploading.setProgress(percentage);
                if (percentage == 100) {
                    dismissProgressUpload();
                    if (isNeedProgress) {
                        processSave.show();
                    }
                }
                if (callback != null) {
                    callback.onLoading(total, current, isUploading);
                }
            }

            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                LogUtils.i("wq 1107 上传文件 onSuccess " + responseInfo.result);
                dismissProgressUpload();
                if (isNeedProgress) {
                    processSave.dismiss();
                }

                if (callback != null) {
                    callback.success(responseInfo.result);
                }
            }


            @Override
            public void onFailure(HttpException error, String msg) {
                LogUtils.i("wq 1107 上传文件 onFailure " + msg);
                error.printStackTrace();
                dismissProgressUpload();
                processSave.dismiss();
                if (callback != null) {
                    callback.fail(msg);
                }
//                      msgTextview.setText(error.getExceptionCode() + ":" + msg);
            }


            private void dismissProgressUpload() {
                if (isNeedProgress) {
                    if (progressUploading != null && progressUploading.isShowing()) {
                        progressUploading.dismiss();
                    }
                }
            }
        });
    }

    public interface UploadCallback {
        void onStart();

        void onLoading(long total, long current, boolean isUploading);

        void success(String result);

        void fail(String msg);

    }


//    String getCookie(Context context) {
//        CookieManager cookieManager = CookieManager.getInstance();
//        String cookie = cookieManager.getCookie("cookie");
//        if (cookie != null) {
//            return cookie;
//        } else {
////            cookie =“XXX”;
//            cookieManager.setCookie("cookie", cookie);
//            return cookie;
//        }
//    }

}
