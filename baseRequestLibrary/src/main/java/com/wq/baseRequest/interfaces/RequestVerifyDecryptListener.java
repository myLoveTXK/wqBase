package com.wq.baseRequest.interfaces;

/**
 * Created by Administrator on 2016/9/27.
 */
public interface RequestVerifyDecryptListener {
    void verifyDecrypSuccess(String result);
    void verifyDecrypFail(String msg);
}
