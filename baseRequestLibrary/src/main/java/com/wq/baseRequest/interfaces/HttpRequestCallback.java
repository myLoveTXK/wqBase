package com.wq.baseRequest.interfaces;

import com.wq.baseRequest.bean.HttpResponseInfoBean;

/**
 * Created by Administrator on 2016/10/10.
 */
public interface HttpRequestCallback {
    void httpSuccess(HttpResponseInfoBean httpResponseInfoBean);//http成功

    void httpFail(HttpResponseInfoBean httpResponseInfoBean);//http失败
}
