package com.wq.baseRequest.interfaces;

/**
 * Created by Administrator on 2016/9/26.
 * 根据状态码返回成功失败
 */
public interface RequestStatusCodeListener {
    final int successCode = 300001;
//    final int failOkHttpCode = 300002;

    void success(String result);

    void fail(String msg);

}
