package com.wq.baseRequest.interfaces;

/**
 * Created by Administrator on 2016/9/26.
 * API请求过程状态
 */
public interface RequestProcessStateListener {
    void requestStartCallback(String url);

    void requestStopCallback(String url);

}
