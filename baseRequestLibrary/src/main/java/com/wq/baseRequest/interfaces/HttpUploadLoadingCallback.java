package com.wq.baseRequest.interfaces;

/**
 * Created by Administrator on 2016/10/10.
 */
public interface HttpUploadLoadingCallback {
    void onStart();
    void onLoading(long total, long current, boolean isUploading);
}
