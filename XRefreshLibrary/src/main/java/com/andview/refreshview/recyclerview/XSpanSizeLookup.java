package com.andview.refreshview.recyclerview;

import android.support.v7.widget.GridLayoutManager;

import com.andview.refreshview.utils.LogUtils;

/**
 * use this class to let the footerview have full width
 */
public class XSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

    public BaseRecyclerAdapter adapter;
    private int mSpanSize = 1;

    public BaseRecyclerAdapter getAdapter() {
        return adapter;
    }

    public XSpanSizeLookup(BaseRecyclerAdapter adapter, int spanSize) {
        this.adapter = adapter;
        this.mSpanSize = spanSize;
    }

    @Override
    public int getSpanSize(int position) {
        LogUtils.i("wq 1221 XRefresh 中使用getSpanSize："+position);
        boolean isHeaderOrFooter = adapter.isFooter(position) || adapter.isHeader(position);
        return isHeaderOrFooter ? mSpanSize : 1;
    }
}